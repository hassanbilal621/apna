<?php
class Pages extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('blog_model');
    $this->load->model('pages_model');
  }

  /*
     * Function: view
     * Purpose: This controller is responsible for showing static pages,
     * Params:  $page: optional parameter, default is home. This is the name of the page to view
     * Return: none
     */
  public function index()
  {


    $data['sliders'] = $this->admin_model->get_sliders();
    $data['sellers'] = $this->admin_model->get_featureshop();


    $this->load->view('templates/users/header.php');
    $this->load->view('templates/users/navbar.php', $data);
    $this->load->view('templates/home/home.php', $data);
    $this->load->view('templates/users/footer.php');
  }
}
