<?php
class admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->model('blog_model');
    }

    public function login()
    {

        if ($this->session->admindata('apna_admin_id')) {
            redirect('admin/');
        }

        $data['title'] = 'Nexco Japan';

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('templates/users/header.php');
            $this->load->view('templates/admin/login.php', $data);
            $this->load->view('templates/users/footer.php');
        } else {

            $username = $this->input->post('username');
            $password = $this->input->post('password');

            //$user_id = $this->user_model->login($username, $password);
            $apna_admin_id = $this->admin_model->login($username, $password);

            if ($apna_admin_id) {
                $admin_data = array(
                    'apna_admin_id' => $apna_admin_id,
                    'japan_email' => $username,
                    'submit_alogged_in' => true
                );
                $this->session->set_admindata($admin_data);

                redirect('admin/');
            } else {

                $this->session->set_flashdata('login_failed', 'Login is invalid. Incorrect username or password.');
                redirect('admin/login');
            }
        }
    }


    public function logout()
    {

        $this->session->unset_userdata('apna_admin_id');
        $this->session->unset_userdata('japan_email');
        $this->session->unset_userdata('submit_alogged_in');

        $this->session->set_flashdata('admin_loggedout', 'You are now logged out');
        redirect('admin/login');
    }





    public function index()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/index.php');
        $this->load->view('templates/admin/footer.php');
    }

    public function ajax_edit_categorymodal($catid)
    {

        $data['categories'] = $this->admin_model->get_categoryinfo($catid);

        $this->load->view('templates/ajax/edit_category.php', $data);
    }

    public function category()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('categoryname', 'categoryname', 'required');
        if ($this->form_validation->run() === FALSE) {

            $data['categories'] = $this->admin_model->get_cat();
            $data['brands'] = $this->admin_model->get_brand();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/category.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {

            $imgname = $this->do_upload();

            $this->admin_model->add_cat($imgname);
            redirect('admin/category');
        }
    }

    public function managecategory()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }


        if ($this->form_validation->run() === FALSE) {

            $data['categories'] = $this->admin_model->get_cat();
            $data['brands'] = $this->admin_model->get_brand();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/managecategory.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {

            $imgname = $this->do_upload();

            $this->admin_model->update_cat($imgname);
            redirect('admin/category');
        }
    }

    public function del_cat($catid)
    {

        $this->admin_model->del_cat($catid);
        redirect('admin/managecategory');
    }


    public function generatevoucher()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('voucher_code', 'voucher_code', 'required');
        if ($this->form_validation->run() === FALSE) {

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/generatevoucher.php');
            $this->load->view('templates/admin/footer.php');
        } else {


            $this->admin_model->generatevoucher();
            redirect('admin/generatevoucher');
        }
    }

    public function managevoucher()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('voucher_code', 'voucher_code', 'required');
        if ($this->form_validation->run() === FALSE) {

            $data['vouchers'] = $this->admin_model->get_vouchers();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/managevoucher.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {


            $this->admin_model->update_vouchers();
            redirect('admin/managevoucher');
        }
    }

    public function deletevoucher($voucher_id)
    {

        $this->admin_model->deletevoucher($voucher_id);
        redirect('admin/managevoucher');
    }

    public function del_subcat($subcatid)
    {

        $this->admin_model->del_subcat($subcatid);
        redirect('admin/managesubcategory');
    }


    public function slider()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $data['imges'] = $this->admin_model->img();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/slider.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    public function updateimage01()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $imgname = $this->do_upload();
        $this->admin_model->updateimage01($imgname);

        redirect('admin/slider');
    }
    public function updateimage02()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }
        $imgname = $this->do_upload();

        $this->admin_model->updateimage02($imgname);

        redirect('admin/slider');
    }
    public function updateimage03()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }
        $imgname = $this->do_upload();

        $this->admin_model->updateimage03($imgname);

        redirect('admin/slider');
    }


    public function del_brand($brandid)
    {

        $this->admin_model->del_brand($brandid);
        redirect('admin/managebrand');
    }

    public function del_seller($sellerid)
    {

        $this->admin_model->del_seller($sellerid);
        redirect('admin/seller');
    }

    public function del_staff($staffid)
    {

        $this->admin_model->del_staff($staffid);
        redirect('admin/administrator');
    }

    public function del_role($roleid)
    {

        $this->admin_model->del_role($roleid);
        redirect('admin/role');
    }

    public function update_cat()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('categoryname', 'categoryname', 'required');



        if ($this->form_validation->run() === FALSE) {


            $this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/category?fail');
        } else {

            $imgname = $this->do_upload();

            $this->admin_model->update_cat($imgname);

            $this->session->set_flashdata('user_registered', 'You are sucessfully registered');


            redirect('admin/managecategory');
        }
    }




    public function ajax_edit_subcategorymodal($subcatid)
    {

        $data['subcategories'] = $this->admin_model->get_subcategoryinfo($subcatid);
        $data['categories'] = $this->admin_model->get_cat();
        $this->load->view('templates/ajax/edit_subcategory.php', $data);
    }


    public function update_subcat()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('subcate_name', 'subcate_name', 'required');



        if ($this->form_validation->run() === FALSE) {
            $data['categories'] = $this->admin_model->get_cat();

            $this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/subcategory?fail');
        } else {

            $this->admin_model->update_subcat();

            $this->session->set_flashdata('user_registered', 'You are sucessfully registered');


            redirect('admin/managesubcategory');
        }
    }

    public function subcategory()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('subcate_name', 'subcate_name', 'required');
        if ($this->form_validation->run() === FALSE) {

            $data['categories'] = $this->admin_model->get_cat();
            $data['subcategories'] = $this->admin_model->get_subcat();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/subcategory.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {

            $this->admin_model->add_subcat();
            redirect('admin/subcategory');
        }
    }

    public function managesubcategory()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('subcate_name', 'subcate_name', 'required');
        if ($this->form_validation->run() === FALSE) {

            $data['categories'] = $this->admin_model->get_cat();
            $data['subcategories'] = $this->admin_model->get_subcat();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/managesubcategory.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {

            $this->admin_model->update_subcat();
            redirect('admin/subcategory');
        }
    }

    public function brand()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('brand_name', 'brand_name', 'required');
        if ($this->form_validation->run() === FALSE) {

            $data['brands'] = $this->admin_model->get_brand();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/brand.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {

            $imgname = $this->do_upload();

            $this->admin_model->add_brand($imgname);
            redirect('admin/brand');
        }
    }

    public function managebrand()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('brand_name', 'brand_name', 'required');
        if ($this->form_validation->run() === FALSE) {

            $data['brands'] = $this->admin_model->get_brand();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/managebrand.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {

            $imgname = $this->do_upload();

            $this->admin_model->add_brand($imgname);
            redirect('admin/brand');
        }
    }

    public function ajax_edit_brandmodal($brandid)
    {

        $data['brands'] = $this->admin_model->get_brandinfo($brandid);
        $this->load->view('templates/ajax/edit_brand.php', $data);
    }

    public function update_brand()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('brand_name', 'brand_name', 'required');



        if ($this->form_validation->run() === FALSE) {


            $this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/brand?fail');
        } else {
            $imgname = $this->do_upload();

            $this->admin_model->update_brand($imgname);

            $this->session->set_flashdata('user_registered', 'You are sucessfully registered');


            redirect('admin/managebrand');
        }
    }

    public function blogger()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('name', 'name', 'required');
        if ($this->form_validation->run() === FALSE) {


            $data['bloggers'] = $this->admin_model->get_blogger();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/blogger.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {

            $enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);

            $this->admin_model->add_blog($enc_password);
            redirect('admin/blogger');
        }
    }

    public function ajax_edit_blogmodal($blogid)
    {

        $data['bloggers'] = $this->admin_model->get_bloginfo($blogid);
        $this->load->view('templates/ajax/editblog.php', $data);
    }

    public function update_blog()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('name', 'name', 'required');



        if ($this->form_validation->run() === FALSE) {


            $this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/blogger?fail');
        } else {

            $this->admin_model->update_blog();

            $this->session->set_flashdata('user_registered', 'You are sucessfully registered');


            redirect('admin/blogger');
        }
    }



    public function del_blogger($blogid)
    {

        $this->admin_model->del_blogger($blogid);
        redirect('admin/blogger');
    }

    public function product()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('productsku', 'productsku', 'required');
        if ($this->form_validation->run() === FALSE) {
            $data['categories'] = $this->admin_model->get_cat();
            $data['products'] = $this->admin_model->get_product();
            $data['brands'] = $this->admin_model->get_brand();
            $data['productimgs'] = $this->admin_model->get_image();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/product.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {

            $this->admin_model->add_product();
            redirect('admin/product');
        }
    }


    public function manageproduct()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }


        $data['products'] = $this->admin_model->get_product();


        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/manageproduct.php', $data);
        $this->load->view('templates/admin/footer.php');
    }
    public function homepageproduct()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }


        $data['products'] = $this->admin_model->get_home_product();


        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/homepageproduct.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    public function del_product($productid)
    {

        $this->admin_model->del_product($productid);
        redirect('admin/manageproduct');
    }
    public function showhome($productid)
    {

        $this->admin_model->showhome($productid);
        redirect('admin/manageproduct');
    }
    public function hidehome($productid)
    {

        $this->admin_model->hidehome($productid);
        redirect('admin/homepageproduct');
    }

    public function edit_product($productid)
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }


        $data['categories'] = $this->admin_model->get_cat($productid);
        $data['products'] = $this->admin_model->get_editproduct($productid);
        $data['brands'] = $this->admin_model->get_brand($productid);
        $data['productimgs'] = $this->admin_model->get_product_image($productid);

        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';
        // die;

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/edit_product.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    public function update_product()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }
        $productid = $this->input->post('ProductID');

        $this->admin_model->update_product($productid);
        redirect('admin/manageproduct/' . $productid);
    }

    public function delete_product_picture($imageid)
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $this->db->where('productimgid', $imageid);
        $this->db->delete('product_img');

        $productid = $_GET['ProductID'];
        redirect('admin/edit_product/' . $productid);
    }


    public function users()
    {
        // if(!$this->session->admindata('apna_admin_id'))
        // {
        // 	redirect('admin/login');
        // }

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/users.php');
        $this->load->view('templates/admin/footer.php');
    }


    public function client()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }
        $this->form_validation->set_rules('email', 'email', 'required');
        if ($this->form_validation->run() === FALSE) {
            $data['users'] = $this->admin_model->get_users();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/client.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            $enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
            $this->admin_model->add_user($enc_password);
            redirect('admin/client');
        }
    }


    public function ajax_edit_usermodal($userid)
    {

        $data['users'] = $this->admin_model->get_userinfo($userid);
        $this->load->view('templates/ajax/edit_user.php', $data);
    }

    public function update_user()
    {

        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('email', 'email', 'required');



        if ($this->form_validation->run() === FALSE) {

            $this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/client?fail');
        } else {

            $this->admin_model->update_user();

            $this->session->set_flashdata('user_registered', 'You are sucessfully registered');


            redirect('admin/client');
        }
    }

    public function blog()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }
        $data['blogs'] = $this->blog_model->get_blog();

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/blog.php', $data);
        $this->load->view('templates/admin/footer.php');
    }

    public function del_blog($blog_id)
    {

        $this->blog_model->del_blog($blog_id);
        redirect('admin/blog');
    }

    public function order()
    {
        // if(!$this->session->admindata('apna_admin_id'))
        // {
        // 	redirect('admin/login');
        // }

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/order.php');
        $this->load->view('templates/admin/footer.php');
    }

    public function recurring()
    {
        // if(!$this->session->admindata('apna_admin_id'))
        // {
        // 	redirect('admin/login');
        // }

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/recurring.php');
        $this->load->view('templates/admin/footer.php');
    }

    public function return()
    {
        // if(!$this->session->admindata('apna_admin_id'))
        // {
        // 	redirect('admin/login');
        // }

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/return.php');
        $this->load->view('templates/admin/footer.php');
    }

    public function setting()
    {
        // if(!$this->session->admindata('apna_admin_id'))
        // {
        // 	redirect('admin/login');
        // }

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/setting.php');
        $this->load->view('templates/admin/footer.php');
    }

    public function totalsales()
    {
        // if(!$this->session->admindata('apna_admin_id'))
        // {
        // 	redirect('admin/login');
        // }

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/totalsales.php');
        $this->load->view('templates/admin/footer.php');
    }

    public function seller()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('name', 'name', 'required');
        if ($this->form_validation->run() === FALSE) {

            $data['sellers'] = $this->admin_model->get_seller();


            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/seller.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {


            $enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);

            $this->admin_model->add_seller($enc_password);
            redirect('admin/seller');
        }
    }


    public function ajax_edit_sellermodal($sellerid)
    {

        $data['sellers'] = $this->admin_model->get_sellerinfo($sellerid);
        $this->load->view('templates/ajax/edit_seller.php', $data);
    }

    public function update_seller()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('name', 'name', 'required');



        if ($this->form_validation->run() === FALSE) {

            $this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/seller?fail');
        } else {

            $this->admin_model->update_seller();

            $this->session->set_flashdata('user_registered', 'You are sucessfully registered');


            redirect('admin/seller');
        }
    }

    public function administrator()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('name', 'name', 'required');
        if ($this->form_validation->run() === FALSE) {

            $data['Staffs'] = $this->admin_model->get_staff();
            $data['roles'] = $this->admin_model->get_role();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/administrator.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            $enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);

            $this->admin_model->add_staff($enc_password);
            redirect('admin/administrator');
        }
    }



    public function ajax_edit_staffmodal($staffid)
    {

        $data['Staffs'] = $this->admin_model->get_staffinfo($staffid);
        $data['roles'] = $this->admin_model->get_role();
        $this->load->view('templates/ajax/editstaff.php', $data);
    }

    public function updatestaff()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('name', 'name', 'required');



        if ($this->form_validation->run() === FALSE) {
            $data['roles'] = $this->admin_model->get_role();

            $this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/administrator?fail');
        } else {

            $this->admin_model->updatestaff();

            $this->session->set_flashdata('user_registered', 'You are sucessfully registered');


            redirect('admin/administrator');
        }
    }


    public function role()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $this->form_validation->set_rules('role', 'role', 'required');
        if ($this->form_validation->run() === FALSE) {

            $data['roles'] = $this->admin_model->get_role();

            $this->load->view('templates/admin/header.php');
            $this->load->view('templates/admin/navbar.php');
            $this->load->view('templates/admin/aside.php');
            $this->load->view('templates/admin/role.php', $data);
            $this->load->view('templates/admin/footer.php');
        } else {
            $this->admin_model->add_role();
            redirect('admin/role');
        }
    }

    public function ajax_edit_rolemodal($roleid)
    {

        $data['roles'] = $this->admin_model->get_roleinfo($roleid);
        $this->load->view('templates/ajax/editrole.php', $data);
    }

    public function update_role()
    {
        if (!$this->session->admindata('apna_admin_id')) {
            redirect('admin/login');
        }

        $data['title'] = 'Sign Up';
        $this->form_validation->set_rules('role', 'role', 'required');



        if ($this->form_validation->run() === FALSE) {


            $this->session->set_flashdata('user_error', 'something missing');
            redirect('admin/role?fail');
        } else {

            $this->admin_model->update_role();

            $this->session->set_flashdata('user_registered', 'You are sucessfully registered');


            redirect('admin/role');
        }
    }

    public function country()
    {
        // if(!$this->session->admindata('apna_admin_id'))
        // {
        // 	redirect('admin/login');
        // }

        $this->load->view('templates/admin/header.php');
        $this->load->view('templates/admin/navbar.php');
        $this->load->view('templates/admin/aside.php');
        $this->load->view('templates/admin/country.php');
        $this->load->view('templates/admin/footer.php');
    }
    function do_upload()
    {

        $config = array(
            'upload_path' => "assets/uploads/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => false,
            'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
            'max_height' => "5000",
            'max_width' => "5000"
        );

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('userfile')) {
            $imgdata = array('upload_data' => $this->upload->data());

            $imgname = $imgdata['upload_data']['file_name'];
        } else {
            $error = array('error' => $this->upload->display_errors());
            echo '<pre>';
            print_r($error);
            echo '<pre>';
            exit;
        }

        return $imgname;
    }

    public function uploadImage()
    {

        $data = [];

        $count = count($_FILES['files']['name']);



        for ($i = 0; $i < $count; $i++) {



            if (!empty($_FILES['files']['name'][$i])) {



                $_FILES['file']['name'] = $_FILES['files']['name'][$i];

                $_FILES['file']['type'] = $_FILES['files']['type'][$i];

                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];

                $_FILES['file']['error'] = $_FILES['files']['error'][$i];

                $_FILES['file']['size'] = $_FILES['files']['size'][$i];



                $config['upload_path'] = 'assets/uploads/';

                $config['allowed_types'] = 'jpg|jpeg|png|gif';

                $config['max_size'] = '5000';

                $config['file_name'] = $_FILES['files']['name'][$i];



                $this->load->library('upload', $config);



                if ($this->upload->do_upload('file')) {

                    $uploadData = $this->upload->data();

                    $filename = $uploadData['file_name'];



                    $data['totalFiles'][] = $filename;
                }
            }
        }
        $productid = $this->input->post('productid');

        foreach ($data['totalFiles'] as $carimage) {
            $this->admin_model->add_image($productid, $carimage);
        }
        // echo '<pre>';
        // print_r($data);
        // echo '<pre>';

        redirect('admin/edit_product/' . $productid);
        // $this->load->view('imageUploadForm', $data); 

    }
}
