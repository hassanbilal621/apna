<?php
class login extends CI_Controller{
    public function __construct()
    {
      parent::__construct();
      $this->load->model('admin_model');
    }

	  
	public function index(){
    if ($this->session->userdata('apna_seller_id')) {
			redirect('seller/');
		}else{
      if ($this->session->userdata('apna_user_id')) {
        redirect('users/');
      }
    }
		$this->load->view('templates/users/header.php');
        $this->load->view('templates/login/index.php');
	}

}
?>