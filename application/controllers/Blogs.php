<?php
class Blogs extends CI_Controller{

	public function __construct()
    {
	  parent::__construct();
	  $this->load->library('cart');
	  $this->load->model('blog_model');
	}
    
    
    public function index(){


        $this->form_validation->set_rules('blog_title', 'blog_title', 'required');
        if($this->form_validation->run() === FALSE){
            $data['blogs'] = $this->blog_model->get_blog();
        

		$data['products'] = $this->admin_model->get_product();
		$data['productimgs'] = $this->admin_model->get_image();
		$data['categories'] = $this->admin_model->get_cat();
        $data['title'] = "All Blogs";
        // echo '<pre>';
		// print_r($data);
		// echo '<pre>';
		// die;

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/aside.php', $data);
        $this->load->view('templates/blogger/index.php', $data);
        $this->load->view('templates/users/footer.php');
        }
        else{
            $imgname = $this->do_upload();

            $this->blog_model->add_blog($imgname);
            redirect('blogs/');
        }

    }

        
    public function view(){


		$data['products'] = $this->admin_model->get_product();
		$data['productimgs'] = $this->admin_model->get_image();
		$data['categories'] = $this->admin_model->get_cat();
        $data['title'] = "All Blogs";

		$this->load->view('templates/users/header.php');
        $this->load->view('templates/users/navbar.php', $data);
        $this->load->view('templates/users/aside.php', $data);
        $this->load->view('templates/blogger/view.php', $data);
        $this->load->view('templates/users/footer.php');

    }

    function do_upload() {
		
		$config = array(
			'upload_path' => "assets/uploads/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => false,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "5000",
			'max_width' => "5000"
			);
	
		$this->load->library('upload', $config);

		if($this->upload->do_upload('userfile'))
		{
			$imgdata = array('upload_data' => $this->upload->data());
	
			$imgname = $imgdata['upload_data']['file_name'];
		}
		else
		{
			$error = array('error' => $this->upload->display_errors());
			echo '<pre>';
			print_r($error);
			echo '<pre>';
			exit;
		}

        return $imgname;
    }






}