<?php
class Seller extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('cart');
		$this->load->model('user_model');
		$this->load->model('seller_model');
	}


	public function login()
	{
		if ($this->session->userdata('apna_seller_id')) {
			redirect('seller/');
		}


		$this->form_validation->set_rules('username', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		// echo  $this->input->post('username');
		// echo '<br>';
		// echo $this->input->post('password');
		// die;

		if ($this->form_validation->run() === FALSE) {
			// $username = $this->input->post('username');
			// $password = $this->input->post('password');

			// echo  $this->input->post('username');
			// echo '<br>';
			// echo $this->input->post('password');
			// die;

			// 	die;
			$data['title'] = 'apna kalakar = Login';

			$this->load->view('templates/users/header.php');
			$this->load->view('templates/seller/login.php', $data);
			$this->load->view('templates/users/footer.php');
		} else {


			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$user_id = $this->seller_model->login($username, $password);



			if ($user_id) {
				$currUser = $this->admin_model->get_sellerinfo($user_id);
				$firstname = $currUser["firstname"];

				$user_data = array(
					'apna_seller_id' => $user_id,
					'username' => $username,
					'firstname' => $firstname,
					'apna_logged_in' => true
				);
				$this->session->set_userdata($user_data);


				//set cookie for 1 year
				$cookie = array(
					'name'   => 'apna_seller_id',
					'value'  => $user_id,
					'expire' => time() + 31556926
				);
				$this->input->set_cookie($cookie);


				$this->session->set_flashdata('user_loggedin', 'You are now logged in');
				redirect('seller/');
			} else {

				$this->session->set_flashdata('login_failed', 'Login is invalid. Incorrect username or password.');
				redirect('login');
			}
		}
	}


	public function logout()
	{

		$this->session->unset_userdata('apna_seller_id');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('necxo_logged_in');

		delete_cookie('jobboard_user_id');

		$this->session->set_flashdata('user_loggedout', 'You are now logged out');
		redirect('login');
	}



	function do_upload()
	{

		$config = array(
			'upload_path' => "assets/uploads/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => false,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "5000",
			'max_width' => "5000"
		);

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('userfile')) {
			$imgdata = array('upload_data' => $this->upload->data());

			$imgname = $imgdata['upload_data']['file_name'];
		} else {
			$error = array('error' => $this->upload->display_errors());
			echo '<pre>';
			print_r($error);
			echo '<pre>';
			exit;
		}

		return $imgname;
	}

	public function register()
	{
		// if(!$this->session->userdata('apna_seller_id'))
		// {
		// 	redirect('login');
		// }

		$this->form_validation->set_rules('email', 'email', 'required|valid_email|is_unique[seller.email]');
		$this->form_validation->set_rules('password', 'password', 'required');

		if ($this->form_validation->run() === FALSE) {

			$this->load->view('templates/users/header.php');
			$this->load->view('templates/users/navbar.php');
			$this->load->view('templates/seller/register.php');
			$this->load->view('templates/seller/footer.php');
		} else {
			$enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
			$this->seller_model->register($enc_password);
		}

		$this->session->set_flashdata('user_registered', 'You are now registerd');

		redirect('login');
	}


	public function index()
	{
		if (!$this->session->userdata('apna_seller_id')) {
			redirect('login');
		}


		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/seller/aside.php');
		$this->load->view('templates/seller/index.php');
		$this->load->view('templates/seller/footer.php');
	}


	public function seller()
	{
		if (!$this->session->userdata('apna_seller_id')) {
			redirect('login');
		}


		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/seller/aside.php');
		$this->load->view('templates/seller/seller.php');
		$this->load->view('templates/seller/footer.php');
	}

	public function productadd()
	{
		if (!$this->session->userdata('apna_seller_id')) {
			redirect('login');
		}
		$seller_id = $this->session->userdata('apna_seller_id');
		$this->form_validation->set_rules('productsku', 'productsku', 'required');
		if ($this->form_validation->run() === FALSE) {
			$data['categories'] = $this->admin_model->get_cat();
			$data['products'] = $this->seller_model->get_products($seller_id);
			$data['brands'] = $this->admin_model->get_brand();
			$data['sellers'] =

				$this->load->view('templates/users/header.php');
			$this->load->view('templates/users/navbar.php');
			$this->load->view('templates/seller/aside.php');
			$this->load->view('templates/seller/productadd.php', $data);
			$this->load->view('templates/seller/footer.php');
		} else {

			$sellerdata = $this->admin_model->get_sellerinfo($seller_id);


			$product_no = $sellerdata['no_of_product'];
			$add = '1';

			$total_product = $product_no + $add;
			$this->seller_model->update_stock($total_product, $seller_id);
			$this->admin_model->add_product($seller_id);
			redirect('Seller/productadd');
		}
	}

	public function  manageproduct()
	{
		if (!$this->session->userdata('apna_seller_id')) {
			redirect('login');
		}
		$this->form_validation->set_rules('productsku', 'productsku', 'required');
		if ($this->form_validation->run() === FALSE) {
			$seller_id = $this->session->userdata('apna_seller_id');
			$data['products'] = $this->seller_model->get_products($seller_id);

			$this->load->view('templates/users/header.php');
			$this->load->view('templates/users/navbar.php');
			$this->load->view('templates/seller/aside.php');
			$this->load->view('templates/seller/manageproduct.php', $data);
			$this->load->view('templates/seller/footer.php');
		} else {
			// $seller_id = $this->session->userdata('apna_seller_id');
			$this->seller_model->update_product();
			redirect('Seller/manageproduct');
		}
	}

	public function  deleteproduct($productid)
	{
		if (!$this->session->userdata('apna_seller_id')) {
			redirect('login');
		}
		$seller_id = $this->session->userdata('apna_seller_id');

		$sellerdata = $this->admin_model->get_sellerinfo($seller_id);


		$product_no = $sellerdata['no_of_product'];
		$add = '1';

		$total_product = $product_no - $add;
		$seller_id = $this->session->userdata('apna_seller_id');
		$this->seller_model->update_stock($total_product, $seller_id);
		$this->seller_model->deleteproduct($productid);
		redirect('Seller/manageproduct');
	}
	public function image()
	{
		if (!$this->session->userdata('apna_seller_id')) {
			redirect('login');
		}
		$seller_id = $this->session->userdata('apna_seller_id');
		$imgname = $this->do_upload();
		$this->seller_model->update_image($imgname, $seller_id);
		redirect('Seller/profile');
	}
	public function updateprofile(){
		if (!$this->session->userdata('apna_seller_id')) {
			redirect('login');
		}
		$seller_id = $this->session->userdata('apna_seller_id');
		$this->seller_model->update_profile($seller_id);
		redirect('Seller/profile');
	}

	public function orders()
	{
		if (!$this->session->userdata('apna_seller_id')) {
			redirect('login');
		}
		$seller_id = $this->session->userdata('apna_seller_id');
		$data['orders'] = $this->seller_model->get_orders($seller_id);

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/seller/aside.php');
		$this->load->view('templates/seller/orders.php', $data);
		$this->load->view('templates/seller/footer.php');
	}

	public function pendingorder()
	{
		if (!$this->session->userdata('apna_seller_id')) {
			redirect('login');
		}
		$seller_id = $this->session->userdata('apna_seller_id');
		$data['orders'] = $this->seller_model->get_order_pending($seller_id);


		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/seller/aside.php');
		$this->load->view('templates/seller/pendingorders.php', $data);
		$this->load->view('templates/seller/footer.php');
	}

	public function deliveryorder()
	{
		if (!$this->session->userdata('apna_seller_id')) {
			redirect('login');
		}
		$seller_id = $this->session->userdata('apna_seller_id');
		$data['orders'] = $this->seller_model->get_order_delever($seller_id);


		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/seller/aside.php');
		$this->load->view('templates/seller/deliveredorders.php', $data);
		$this->load->view('templates/seller/footer.php');
	}
	public function profile()
	{
		if (!$this->session->userdata('apna_seller_id')) {
			redirect('login');
		}
		$userid = $this->session->userdata('apna_seller_id');

		$data['user'] = $this->seller_model->get_sellerinfo($userid);
		$data['title'] = "User Account";
		$data['orders'] = $this->user_model->get_placeorder();


		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/seller/aside.php');
		$this->load->view('templates/seller/profile.php', $data);
		$this->load->view('templates/seller/footer.php');
	}


	public function ajax_edit_product($productid)
	{
		$seller_id = $this->session->userdata('apna_seller_id');

		$data['product'] = $this->seller_model->get_product($seller_id, $productid);
		$data['categories'] = $this->admin_model->get_cat();
		$data['brands'] = $this->admin_model->get_brand();
		$this->load->view('templates/ajax/edit_product.php', $data);
	}
}
