<?php
class Users extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('cart');
		$this->load->model('user_model');
	}



	public function testing()
	{

		if (!$this->session->userdata('apna_user_id')) {
			redirect('login');
		}
		$data['title'] = "My New Website";

		$userid = $this->session->userdata('apna_user_id');
		$data['userdetails'] = $this->user_model->get_userinfo($userid);
		$data['stocks'] = $this->user_model->get_stocks();

		// echo '<pre>';
		// print_r($data);
		// echo '<pre>';
		// die;

		$this->load->view('templates/users/test.php', $data);
	}

	function do_upload()
	{

		$config = array(
			'upload_path' => "assets/uploads/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => false,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "5000",
			'max_width' => "5000"
		);

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('userfile')) {
			$imgdata = array('upload_data' => $this->upload->data());

			$imgname = $imgdata['upload_data']['file_name'];
		} else {
			$error = array('error' => $this->upload->display_errors());
			echo '<pre>';
			print_r($error);
			echo '<pre>';
			exit;
		}

		return $imgname;
	}

	public function ajaxlogin()
	{



		$username = $_GET['email'];
		$password = $_GET['password'];

		$user_id = $this->user_model->login($username, $password);


		// echo $user_id;
		// die;
		//$employee_id = $this->employee_model->login($username, $password);

		if ($user_id) {
			$user_data = array(
				'apna_user_id' => $user_id,
				'username' => $username,
				'apna_logged_in' => true
			);
			$this->session->set_userdata($user_data);


			echo "success";
		} else {

			echo "incorrect";
		}
	}




	public function login()
	{
		if ($this->session->userdata('apna_user_id')) {
			redirect('users/');
		}

		$data['title'] = 'apna kalakar = Login';
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() === FALSE) {

			$this->load->view('templates/users/header.php');
			$this->load->view('templates/users/login.php', $data);
			$this->load->view('templates/users/footer.php');
		} else {
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			$user_id = $this->user_model->login($username, $password);


			// echo $user_id;
			// die;
			//$employee_id = $this->employee_model->login($username, $password);

			if ($user_id) {
				$currUser = $this->user_model->get_userinfo($user_id);
				$UserFirstName = $currUser["UserFirstName"];
				$userimage = $currUser["userimage"];

				$user_data = array(
					'apna_user_id' => $user_id,
					'username' => $username,
					'UserFirstName' => $UserFirstName,
					'userimage' => $userimage,
					'apna_logged_in' => true
				);
				$this->session->set_userdata($user_data);


				//set cookie for 1 year
				$cookie = array(
					'name'   => 'apna_user_id',
					'value'  => $user_id,
					'expire' => time() + 31556926
				);
				$this->input->set_cookie($cookie);


				$this->session->set_flashdata('user_loggedin', 'You are now logged in');
				redirect('users/');
			} else {

				$this->session->set_flashdata('login_failed', 'Login is invalid. Incorrect username or password.');
				redirect('login');
			}
		}
	}

	// public function ajax_edit_billing_login($loginid){


	//     $this->load->view('templates/ajax/billing_login.php', $data);
	// }


	public function index()
	{
		// if(!$this->session->userdata('apna_user_id'))
		// {
		// 	redirect('login');
		// }
		$userid = $this->session->userdata('apna_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";
		$data['products'] = $this->admin_model->get_home_product_09();
		$data['subcategories'] = $this->admin_model->get_subcat();
		$data['categories'] = $this->admin_model->get_cat();
		$data['sliders'] = $this->admin_model->get_sliders();
		$data['sellers'] = $this->admin_model->get_featureshop_6();
		$data['blogs'] = $this->user_model->get_blog_6();


		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php', $data);
		$this->load->view('templates/users/aside.php', $data);
		$this->load->view('templates/users/index.php', $data);
		$this->load->view('templates/users/footer.php');
	}

	public function categories()
	{

		$data['categories'] = $this->admin_model->get_cat();

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/users/aside.php');
		$this->load->view('templates/users/category.php', $data);
		$this->load->view('templates/users/footer.php');
	}


	public function catagory($catid)
	{
		// if(!$this->session->userdata('apna_user_id'))
		// {
		// 	redirect('login');
		// }
		$userid = $this->session->userdata('apna_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";
		$data['products'] = $this->admin_model->get_catproduct($catid);
		$data['subcategories'] = $this->admin_model->get_subcat();
		$data['categories'] = $this->admin_model->get_cat();
		$data['sliders'] = $this->admin_model->get_sliders();


		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php', $data);
		$this->load->view('templates/users/aside.php', $data);
		$this->load->view('templates/users/index.php', $data);
		$this->load->view('templates/users/footer.php');
	}


	public function profile()
	{
		if (!$this->session->userdata('apna_user_id')) {
			redirect('login');
		}
		$userid = $this->session->userdata('apna_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";
		$data['orders'] = $this->user_model->get_placeorder();
		$data['blogs'] = $this->user_model->get_blog($userid);

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php', $data);
		$this->load->view('templates/users/aside.php', $data);
		$this->load->view('templates/users/profile.php', $data);
		$this->load->view('templates/users/footer.php');
	}







	public function shop()
	{
		// if(!$this->session->userdata('apna_user_id'))
		// {
		// 	redirect('login');
		// }
		$userid = $this->session->userdata('apna_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";
		$data['products'] = $this->admin_model->get_product();
		$data['productimgs'] = $this->admin_model->get_image();
		$data['categories'] = $this->admin_model->get_cat();

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php', $data);
		$this->load->view('templates/users/aside.php', $data);
		$this->load->view('templates/users/shop.php', $data);
		$this->load->view('templates/users/footer.php');
	}

	// public function addtocart($productid)
	// {
	// 	$product =$this -> product->getrows($productid);

	// 	$data = array(
	// 		'id' => $product['ProductID'],
	// 		'qty' => 1,
	// 		'price' => $product['ProductPrice'],
	// 		'name' => $product['ProductName']
	// 	);
	// 	$this->cart->insert($data);
	// 	redirect('cart/');
	// }




	public function ajax_view_product($productid)
	{


		$data['product'] = $this->user_model->get_single_product($productid);

		// echo '<pre>';
		// 	print_r($data);
		// 	echo '<pre>';
		// 	die;
		$this->load->view('templates/ajax/viewproduct.php', $data);
	}

	public function invoice()
	{
		// if(!$this->session->userdata('apna_user_id'))
		// {
		// 	redirect('login');
		// }
		$userid = $this->session->userdata('apna_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php', $data);
		$this->load->view('templates/users/aside.php', $data);
		$this->load->view('templates/users/invoice.php', $data);
		$this->load->view('templates/users/footer.php');
	}

	public function register()
	{
		// if(!$this->session->userdata('apna_user_id'))
		// {
		// 	redirect('login');
		// }
		$userid = $this->session->userdata('apna_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$this->load->view('templates/users/header.php');
		// $this->load->view('templates/users/navbar.php', $data);
		$this->load->view('templates/users/aside.php', $data);
		$this->load->view('templates/users/regist.php', $data);
		$this->load->view('templates/users/footer.php');
	}

	public function forgetpassword()
	{
		// if(!$this->session->userdata('apna_user_id'))
		// {
		// 	redirect('login');
		// }
		$userid = $this->session->userdata('apna_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php', $data);
		$this->load->view('templates/users/aside.php', $data);
		$this->load->view('templates/users/forgetpassword.php', $data);
		$this->load->view('templates/users/footer.php');
	}

	public function product()
	{
		// if(!$this->session->userdata('apna_user_id'))
		// {
		// 	redirect('login');
		// }

		$data['products'] = $this->admin_model->get_product();
		$data['subcategories'] = $this->admin_model->get_subcat();
		$data['categories'] = $this->admin_model->get_cat();

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php', $data);
		$this->load->view('templates/users/aside.php', $data);
		$this->load->view('templates/users/product.php', $data);
		$this->load->view('templates/users/footer.php');
	}

	public function get_search_product(){


	

		$data['products'] = $this->user_model->search();


	
		$this->load->view('search/search.php', $data);

	}



	public function serchproduct() {
        
        $key = $this->input->post('search');
        $sat_id = $this->input->post('CategoryID');

        if(isset($key) and !empty($key)){
            $data['products'] = $this->user_model->searchRecord($key, $sat_id);
            $data['link'] = '';
            $data['message'] = 'Search Results';
			$data['subcategories'] = $this->admin_model->get_subcat();
			$data['categories'] = $this->admin_model->get_cat();
	
			$this->load->view('templates/users/header.php');
			$this->load->view('templates/users/navbar.php', $data);
			$this->load->view('templates/users/aside.php', $data);
			$this->load->view('templates/users/product.php', $data);
			$this->load->view('templates/users/footer.php');
        }
        else {
            redirect('Search') ;
        }
	}
	public function get_discount()
    {
		$data['vouchar'] = $this->user_model->search();

		$price =  $data['voucher_price'] ;
	}
	public function usercart()
	{
		// if(!$this->session->userdata('apna_user_id'))
		// {
		// 	redirect('login');
		// }
		$userid = $this->session->userdata('apna_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php', $data);
		$this->load->view('templates/users/aside.php', $data);
		$this->load->view('templates/users/usercart.php', $data);
		$this->load->view('templates/users/footer.php');
	}

	public function userpage()
	{
		// if(!$this->session->userdata('apna_user_id'))
		// {
		// 	redirect('login');
		// }
		$userid = $this->session->userdata('apna_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php', $data);
		$this->load->view('templates/users/aside.php', $data);
		$this->load->view('templates/users/userpage.php', $data);
		$this->load->view('templates/users/footer.php');
	}

	public function slider()
	{

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/slider.php');
		$this->load->view('templates/users/footer.php');
	}


	public function updatecart()
	{
		// echo '<pre>';
		// print_r($this->input->post());
		// echo '<pre>';
		// die;
		$this->cart->update($this->input->post());
		redirect('users/cart');
	}

	public function cart()
	{
		if ($this->session->userdata('apna_seller_id')) {
			redirect('seller');
		}
		$userid = $this->session->userdata('apna_user_id');
		$currUser = $this->user_model->get_userinfo($userid);

		$data['user'] = $currUser;
		$data['title'] = "User Account";


		// $product=$this ->user_model->getrows($productid);
		// $data = array(
		// 	'id'      => 'sku_123ABC',
		// 	'qty'     => 1,
		// 	'price'   => 39.95,
		// 	'name'    => 'T-Shirt',
		// 	'options' => array('Size' => 'L', 'Color' => 'Red')
		// );

		// $this->cart->insert($data);

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php', $data);
		$this->load->view('templates/users/aside.php', $data);
		$this->load->view('templates/cart/index.php', $data);
		$this->load->view('templates/users/footer.php');

		// redirect('cart/');
	}
	public function addproduct()
	{
		$data = array(
			'id'      => $this->input->post('productid'),
			'qty'     =>  $this->input->post('productqty'),
			'price'   => $this->input->post('productprice'),
			'name'    =>  $this->input->post('productname'),
			'picture'    =>  $this->input->post('picture'),
			'sellerid'    =>  $this->input->post('sellerid'),
			'ProductStock' =>  $this->input->post('ProductStock')
			
		);

		$this->cart->insert($data);
		redirect('users/cart');
	}

	public function deleterowcart($rowid)
	{
		$this->cart->remove($rowid);
		redirect('users/cart');
	}


	public function viewcart()
	{

		$this->load->view('templates/users/viewcart.php');
	}

	public function billing()
	{
		// if(!$this->session->userdata('apna_user_id'))
		// {
		// 	redirect('login');
		// }

		$data['title'] = "User Account";
		$this->form_validation->set_rules('fullname', 'fullname', 'required');
		if ($this->form_validation->run() === FALSE) {

			$userid = $this->session->userdata('apna_user_id');
			$currUser = $this->user_model->get_userinfo($userid);

			$data['user'] = $currUser;
			$this->load->view('templates/users/header.php');
			$this->load->view('templates/users/navbar.php', $data);
			$this->load->view('templates/users/aside.php', $data);
			$this->load->view('templates/users/billing.php', $data);
			$this->load->view('templates/users/footer.php');
		} else {


			$orderid = $this->user_model->placeorder();
			foreach ($this->cart->contents() as $items) :

				$this->adorderitem($items['price'], $items['qty'], $orderid, $items['id'], $items['sellerid']);

			endforeach;
			$this->cart->destroy();
			redirect('users/');
		}
	}


	public function orders()
	{
		// if(!$this->session->userdata('apna_user_id'))
		// {
		// 	redirect('login');
		// }
		$data['title'] = "User Account";
		$data['orders'] = $this->user_model->get_placeorder();
		//     echo '<pre>';
		// print_r($data);
		// echo '<pre>';
		// die;

		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php', $data);
		$this->load->view('templates/users/aside.php', $data);
		$this->load->view('templates/users/orders.php', $data);
		$this->load->view('templates/users/footer.php');
	}

	public function del_placeorder($id)
	{

		$this->user_model->del_placeorder($id);
		redirect('users/orders');
	}


	// public function cartviewitem(){

	// 	print_r($this->cart->contents());

	// }


	public function adorderitem($orderprice, $orderqty, $orderid, $productid, $sellerid)
	{

		$data = array(
			'sub_total' => $orderprice,
			'qty' => $orderqty,
			'order_id' => $orderid,
			'product_id' => $productid,
			'sellerid' => $sellerid,
			'status' => 'pending',
			'userid' => $this->session->userdata('apna_user_id')
		);


		$this->security->xss_clean($data);
		$this->db->insert('order_item', $data);
	}

	// public function placeorder(){
	// 	if(!$this->session->userdata('apna_user_id'))
	// 	{
	// 		redirect('login');
	// 	}

	// 	$this->form_validation->set_rules('fullname', 'fullname', 'required');



	//     if($this->form_validation->run() === FALSE){


	// 		redirect('users/billing'); 
	//     }
	//     else{
	// 		$enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
	//         //$enc_password = md5($this->input->post('password'));
	//         $this->user_model->register($enc_password);

	// 		$this->session->set_flashdata('user_registered', 'You are sucessfully registered');

	//         redirect('users/profile');

	//     }



	public function checkout()
	{
		if (!$this->session->userdata('apna_user_id')) {
			redirect('login');
		} else {
			$order_id = array(
				'orderuser_id'      => $this->session->userdata('username'),
				'created'     =>  date('Y-m-d H:i:s'),
				'modified'   => date('Y-m-d H:i:s'),
				'status'    =>  0,
				// 'options' => array('Size' => 'L', 'Color' => 'Red')
			);
			$orderid = $this->user_model->createorder($order_id);

			$orderdetail_id = array(
				'DetailOrderID'      => $order_id,
				'DetailProductID'     =>  $this->input->post('productid'),
				'fullname'   => $this->input->post('fullname'),
				'phone'    =>  $this->input->post('phone'),
				'email'      => $this->input->post('email'),
				'address'     =>  $this->input->post('address'),
				'city'   => $this->input->post('city'),
				'payment'    =>  $this->input->post('payment'),
				'comment'    =>  $this->input->post('comment'),
				'qty'     =>  $this->input->post('productqty'),
				'price'   => $this->input->post('productprice'),
				// 'options' => array('Size' => 'L', 'Color' => 'Red')
			);
			$orderdetailid = $this->user_model->createorderdetail($orderdetail_id);


			$this->cart->destroy();
		}
	}



	public function blog()
	{
		// if(!$this->session->userdata('apna_user_id'))
		// {
		// 	redirect('login');
		// }


		$this->load->view('templates/users/header.php');
		$this->load->view('templates/users/navbar.php');
		$this->load->view('templates/users/aside.php');
		$this->load->view('templates/users/blog.php');
		$this->load->view('templates/users/footer.php');
	}

	public function addblog()
	{
		if (!$this->session->userdata('apna_user_id')) {
			redirect('login');
		}
		$this->form_validation->set_rules('blog_title', 'blog_title', 'required');
		if ($this->form_validation->run() === FALSE) {

		} else {
			$userid = $this->session->userdata('apna_user_id');
			$imgname = $this->do_upload();

			$this->user_model->add_blog($imgname, $userid);
			redirect('users/profile');
		}
	}

	public function updateblog()
	{
		if (!$this->session->userdata('apna_user_id')) {
			redirect('login');
		}
		$this->form_validation->set_rules('blog_title', 'blog_title', 'required');
		if ($this->form_validation->run() === FALSE) {

		} else {

			if (!file_exists($_FILES["userfile"]["tmp_name"])) {
				$userid = $this->session->userdata('apna_user_id');
				$blogid = $this->input->post('blog_id');
				$blogs = $this->user_model->get_blog_user($userid, $blogid);
	
				$imgname = $blogs["blog_img"];
				
				$this->user_model->updateblog($imgname, $userid, $blogid);
			} else {
				$imgname = $this->do_upload();
				$userid = $this->session->userdata('apna_user_id');
				$blogid = $this->input->post('blog_id');
				$this->user_model->updateblog($imgname, $userid, $blogid);
			}
	
			redirect('users/profile');
		}
	}



	public function add_comment()
	{
		if (!$this->session->userdata('apna_user_id')) {
			redirect('login');
		}
		$this->form_validation->set_rules('comment', 'comment', 'required');
		if ($this->form_validation->run() === FALSE) {

		} else {
			$userid = $this->session->userdata('apna_user_id');
			$blogid = $this->input->post('blogid');

			$this->user_model->add_comment($userid, $blogid);
			redirect('blogs');
		}
	}






	public function logout()
	{

		$this->session->unset_userdata('apna_user_id');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('necxo_logged_in');

		delete_cookie('jobboard_user_id');

		$this->session->set_flashdata('user_loggedout', 'You are now logged out');
		redirect('login');
	}




	public function registers()
	{
		// if($this->session->userdata('logged_in')){
		//     redirect('users/');
		// }

		$data['title'] = 'Sign Up';
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'required');


		if ($this->form_validation->run() === FALSE) {

			redirect('users?=tryagain');
		} else {

			$enc_password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
			$this->user_model->register($enc_password);
			$this->session->set_flashdata('user_registered', 'You are now register');
			redirect('users/profile');
		}
	}

	public function updateuser()
	{
		if (!$this->session->userdata('apna_user_id')) {
			redirect('login');
		}
		if (!file_exists($_FILES["userfile"]["tmp_name"])) {
			$userid = $this->session->userdata('apna_user_id');
			$currUser = $this->admin_model->get_userinfo($userid);

			$imgname = $currUser["userimage"];
			$this->user_model->update($userid, $imgname);
		} else {
			$imgname = $this->do_upload();
			$userid = $this->session->userdata('apna_user_id');
			$this->user_model->update($userid, $imgname);
		}

		redirect('users/logout');
	}

	public function ajax_edit_blog($blogsid)
    {

        $data['blog'] = $this->user_model->get_blogs($blogsid);

        $this->load->view('templates/ajax/editblogs.php', $data);
    }

    public function ajax_view_blog($blogsid)
    {
		$data['comments'] = $this->user_model->get_blog_comments($blogsid	);
        $data['blog'] = $this->user_model->get_blogs($blogsid);

        $this->load->view('templates/ajax/viewblogs.php', $data);
    }

    public function ajax_view_blog_user($blogsid)
    {

        $data['blog'] = $this->user_model->get_blogs($blogsid);

        $this->load->view('templates/ajax/viewblog.php', $data);
    }



}
