<?php
class User_model extends CI_Model{
	
    public function register($enc_password){
        $data = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'username' => $this->input->post('username'),
            'password' => $enc_password,
            'pcode' => $this->input->post('pcode'),
			'address' => $this->input->post('addr'),
			'city' => $this->input->post('city'),
			'prov' => $this->input->post('prov'),
			'phone' => $this->input->post('phone'),
			'fax' => $this->input->post('fax')
        );

		$this->security->xss_clean($data);
        return $this->db->insert('users', $data);
	}

}

?>