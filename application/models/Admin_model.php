<?php
class Admin_model extends CI_Model
{

	public function login($username, $password)
	{

		$this->db->where('username', $username);
		$result = $this->db->get('admin');

		if ($result->num_rows() == 1) {
			$hash = $result->row(0)->password;

			if (password_verify($password, $hash)) {
				return $result->row(0)->id;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}



	public function get_admininfo($admin_id)
	{
		$this->db->where('id', $admin_id);
		$result = $this->db->get('admin');

		return $result->row_array();
	}

	public function get_bloggerinfo($blogger_id)
	{
		$this->db->where('id', $blogger_id);
		$result = $this->db->get('blogger');

		return $result->row_array();
	}

	// blogger Start Get / Add / Update / Delete
	public function add_blog($enc_password)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'phone' => $this->input->post('phone'),
			'email' => $this->input->post('email'),
			'password' => $enc_password

		);

		$this->security->xss_clean($data);
		$this->db->insert('blogger', $data);
	}

	public function update_blog()
	{
		$data = array(
			'name' => $this->input->post('name'),
			'phone' => $this->input->post('phone'),
			'email' => $this->input->post('email')

		);

		$this->security->xss_clean($data);
		$this->db->where('blogid', $this->input->post('blogid'));
		$this->db->update('blogger', $data);
	}

	public function get_blogger()
	{

		$query = $this->db->get('blogger');
		return $query->result_array();
	}

	public function get_sliders()
	{

		$query = $this->db->get('slider');
		return $query->result_array();
	}


	public function get_bloginfo($blogid)
	{
		$this->db->where('blogid', $blogid);
		$result = $this->db->get('blogger');

		return $result->row_array();
	}

	public function del_blogger($blogid)
	{
		$this->db->where('blogid', $blogid);
		$this->db->delete('blogger');
	}

	// blogger End

	// Product Start Get / Add / Update / Delete 

	public function add_product($sellerid)
	{
		$data = array(
			'ProductSKU' => $this->input->post('productsku'),
			'ProductName' => $this->input->post('productname'),
			'ProductPrice' => $this->input->post('productprice'),
			'ProductWeight' => $this->input->post('productweight'),
			'ProductCartDesc' => $this->input->post('productcartdesc'),
			'ProductShortDesc' => $this->input->post('productshortdesc'),
			'ProductLongDesc' => $this->input->post('productlongdesc'),
			'ProductThumb' => $this->input->post('productthumb'),
			// 'no_of_product' => $total_product,
			//'city' => $this->input->post('city'),
			'ProductCategoryID' => $this->input->post('categoryid'),
			'ProductUpdateDate' => date('Y-m-d H:i:s'),
			'ProductStock' => $this->input->post('productstock'),
			'ProductLive' => $this->input->post('productlive'),
			'ProductUnlimited' => $this->input->post('productunlimited'),
			'ProductLocation' => $this->input->post('productlocation'),
			'product_brand_id' => $this->input->post('productbrandid'),
			'offer'    =>  $this->input->post('offer'),
			'productimg_id' => $this->input->post('productimg_id'),
			'seller_id' => $sellerid
		);

		$this->security->xss_clean($data);
		$this->db->insert('products', $data);
	}

	public function update_product($productid)
	{
		$data = array(
			'ProductSKU' => $this->input->post('productsku'),
			'ProductName' => $this->input->post('productname'),
			'ProductPrice' => $this->input->post('productprice'),
			'ProductWeight' => $this->input->post('productweight'),
			'ProductCartDesc' => $this->input->post('productcartdesc'),
			'ProductShortDesc' => $this->input->post('productshortdesc'),
			'ProductLongDesc' => $this->input->post('productlongdesc'),
			'ProductThumb' => $this->input->post('productthumb'),
			//'country' => $this->input->post('country'),
			//'city' => $this->input->post('city'),
			'ProductCategoryID' => $this->input->post('categoryid'),
			'ProductUpdateDate' => date('Y-m-d H:i:s'),
			'ProductStock' => $this->input->post('productstock'),
			'ProductLive' => $this->input->post('productlive'),
			'ProductUnlimited' => $this->input->post('productunlimited'),
			'ProductLocation' => $this->input->post('productlocation'),
			'product_brand_id' => $this->input->post('productbrandid'),
			'offer'    =>  $this->input->post('offer'),
			'productimg_id' => $this->input->post('productimg_id')
		);

		$this->security->xss_clean($data);
		$this->db->where('ProductID', $productid);
		$this->db->update('products', $data);
	}

	public function showhome($productid)
	{
		$data = array(
			'homepage' => 'show'
		);

		$this->security->xss_clean($data);
		$this->db->where('ProductID', $productid);
		$this->db->update('products', $data);
	}
	public function hidehome($productid)
	{
		$data = array(
			'homepage' => 'hide'
		);

		$this->security->xss_clean($data);
		$this->db->where('ProductID', $productid);
		$this->db->update('products', $data);
	}

	public function get_catproduct($catid)
	{

		$this->db->join('subcategory', 'products.ProductCategoryID = subcategory.subcat_id');
		$this->db->join('brands', 'brands.brand_id = products.product_brand_id', 'left');
		$this->db->join('product_img', 'product_img.productimg_id = products.productimg_id', 'left');

		$this->db->where('ProductCategoryID', $catid);
		$this->db->order_by('products.ProductID', 'desc');
		$this->db->limit(12);
		$query = $this->db->get('products');
		return $query->result_array();
	}

	public function get_product()
	{
		$this->db->join('subcategory', 'products.ProductCategoryID = subcategory.subcat_id', 'left');
		$this->db->join('brands', 'brands.brand_id = products.product_brand_id', 'left');
		$this->db->join('seller', 'products.seller_id = seller.seller_id', 'left');
		$this->db->order_by('products.ProductID', 'desc');
		$query = $this->db->get('products');
		return $query->result_array();
	}
	public function get_home_product_09(){

		$this->db->join('subcategory', 'products.ProductCategoryID = subcategory.subcat_id', 'left');
		$this->db->join('brands', 'brands.brand_id = products.product_brand_id', 'left');
		$this->db->join('seller', 'products.seller_id = seller.seller_id', 'left');
		$this->db->where('homepage', 'show');
		$this->db->order_by('products.ProductID', 'desc');
		$this->db->limit(6);
		$query = $this->db->get('products');
		return $query->result_array();
	}
	public function get_home_product(){

		$this->db->join('subcategory', 'products.ProductCategoryID = subcategory.subcat_id', 'left');
		$this->db->join('brands', 'brands.brand_id = products.product_brand_id', 'left');
		$this->db->join('seller', 'products.seller_id = seller.seller_id', 'left');
		$this->db->where('homepage', 'show');
		$this->db->order_by('products.ProductID', 'desc');
		$query = $this->db->get('products');
		return $query->result_array();
	}

	public function get_sellerproduct($sellerid)
	{

		$this->db->join('subcategory', 'products.ProductCategoryID = subcategory.subcat_id', 'left');
		$this->db->join('brands', 'brands.brand_id = products.product_brand_id', 'left');
		$this->db->join('seller', 'products.seller_id = seller.seller_id', 'left');

		$this->db->where('products.seller_id', $sellerid);
		$query = $this->db->get('products');
		return $query->result_array();
	}

	public function get_editproduct($productid)
	{

		$this->db->join('subcategory', 'products.ProductCategoryID = subcategory.category_id');
		$this->db->join('brands', 'brands.brand_id = products.product_brand_id', 'left');
		$this->db->where('ProductID', $productid);
		$query = $this->db->get('products');
		return $query->row_array();
	}


	public function del_product($productid)
	{
		$this->db->where('ProductID', $productid);
		$this->db->delete('products');
	}

	public function add_image($productid, $carimage)
	{

		$data = array(

			'product_img' => $carimage,
			'product_id' => $productid

		);
		$this->security->xss_clean($data);
		$this->db->insert('product_img', $data);
	}




	public function get_product_image($productid)
	{

		$this->db->where('product_id', $productid);

		$query = $this->db->get('product_img');

		return $query->result_array();
	}


	public function get_image()
	{

		$this->db->join('products', 'products.ProductID = product_img.product_id', 'left');
		$query = $this->db->get('product_img');
		return $query->result_array();
	}

	// Product End 


	// Category Start Get / Add / Update / Delete 

	public function get_categoryinfo($catid)
	{
		$this->db->where('CategoryID', $catid);
		$result = $this->db->get('productcategories');

		return $result->row_array();
	}

	public function add_cat($imgname)
	{
		$data = array(

			'CategoryName' => $this->input->post('categoryname'),

		);

		$this->security->xss_clean($data);
		$this->db->insert('productcategories', $data);
	}


	public function update_cat($imgname)
	{
		$data = array(

			'CategoryName' => $this->input->post('categoryname'),

			'Categoryimg' => $imgname

		);

		$this->security->xss_clean($data);
		$this->db->where('CategoryID', $this->input->post('CategoryID'));
		$this->db->update('productcategories', $data);
	}

	public function get_cat()
	{



		$query = $this->db->get('productcategories');
		return $query->result_array();
	}
	public function get_cat_6()
	{
		$this->db->order_by('productcategories.CategoryID', 'desc');
		$this->db->limit(6);
		$query = $this->db->get('productcategories');
		return $query->result_array();
	}

	public function del_cat($catid)
	{
		$this->db->where('CategoryID', $catid);
		$this->db->delete('productcategories');
	}

	// Category End 


	// SubCategory Start Get / Add / Update / Delete 


	public function get_subcategoryinfo($subcatid)
	{
		$this->db->where('subcat_id', $subcatid);
		$result = $this->db->get('subcategory');

		return $result->row_array();
	}

	public function update_subcat()
	{
		$data = array(

			'subcate_name' => $this->input->post('subcate_name'),
			'category_id' => $this->input->post('categoryid')
		);

		$this->security->xss_clean($data);
		$this->db->where('subcat_id', $this->input->post('subcat_id'));
		$this->db->update('subcategory', $data);
	}

	public function add_subcat()
	{
		$data = array(

			'subcate_name' => $this->input->post('subcate_name'),
			'category_id' => $this->input->post('categoryid')

		);

		$this->security->xss_clean($data);
		$this->db->insert('subcategory', $data);
	}

	public function get_subcat()
	{

		$this->db->join('productcategories', 'productcategories.CategoryID = subcategory.category_id', 'left');

		$query = $this->db->get('subcategory');
		return $query->result_array();
	}

	public function del_subcat($subcatid)
	{
		$this->db->where('subcat_id', $subcatid);
		$this->db->delete('subcategory');
	}

	// SubCategory End 

	// Brand Start Get / Add / Update / Delete 
	public function add_brand($imgname)
	{
		$data = array(

			'brand_name' => $this->input->post('brand_name'),
			'brand_logo' => $imgname

		);

		$this->security->xss_clean($data);
		$this->db->insert('brands', $data);
	}

	public function get_brand()
	{

		$query = $this->db->get('brands');
		return $query->result_array();
	}

	public function update_brand($imgname)
	{
		$data = array(

			'brand_name' => $this->input->post('brand_name'),
			'brand_logo' => $imgname
		);

		$this->security->xss_clean($data);
		$this->db->where('brand_id', $this->input->post('brand_id'));
		$this->db->update('brands', $data);
	}

	public function get_brandinfo($brandid)
	{
		$this->db->where('brand_id', $brandid);
		$result = $this->db->get('brands');

		return $result->row_array();
	}

	public function del_brand($brandid)
	{
		$this->db->where('brand_id', $brandid);
		$this->db->delete('brands');
	}

	// Brand End 

	// Seller Start Get / Add / Update / Delete 

	public function add_seller($enc_password)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'password' => $enc_password,
			'date' => date('Y-m-d H:i:s')

		);

		$this->security->xss_clean($data);
		$this->db->insert('seller', $data);
	}

	public function get_seller()
	{
		$query = $this->db->get('seller');
		return $query->result_array();
	}

	public function get_featureshop()
	{

		$this->db->where('status', 2);
		$query = $this->db->get('seller');
		return $query->result_array();
	}
	public function get_featureshop_6()
	{
		$this->db->order_by('seller.seller_id', 'desc');
		$this->db->limit(6);
		$this->db->where('status', 2);
		$query = $this->db->get('seller');
		return $query->result_array();
	}


	public function del_seller($sellerid)
	{
		$this->db->where('seller_id', $sellerid);
		$this->db->delete('seller');
	}

	public function get_sellerinfo($sellerid)
	{
		$this->db->where('seller_id', $sellerid);
		$result = $this->db->get('seller');

		return $result->row_array();
	}

	public function update_seller()
	{
		$data = array(

			'name' => $this->input->post('name'),
			'email' => $this->input->post('email'),
			'date' => date('Y-m-d H:i:s')
		);

		$this->security->xss_clean($data);
		$this->db->where('seller_id', $this->input->post('seller_id'));
		$this->db->update('seller', $data);
	}

	// Seller End 

	// Role Start Get / Add / Update / Delete 

	public function add_role()
	{
		$data = array(
			'role' => $this->input->post('role')

		);

		$this->security->xss_clean($data);
		$this->db->insert('role', $data);
	}

	public function update_role()
	{
		$data = array(
			'role' => $this->input->post('role')

		);

		$this->security->xss_clean($data);
		$this->db->where('role_id', $this->input->post('role_id'));
		$this->db->update('role', $data);
	}

	public function get_roleinfo($roleid)
	{
		$this->db->where('role_id', $roleid);
		$result = $this->db->get('role');

		return $result->row_array();
	}

	public function get_role()
	{
		$query = $this->db->get('role');
		return $query->result_array();
	}

	public function del_role($roleid)
	{
		$this->db->where('role_id', $roleid);
		$this->db->delete('role');
	}

	// Role End

	// Staff Start Get / Add / Update / Delete 

	public function add_staff($enc_password)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'phone' => $this->input->post('phone'),
			'email' => $this->input->post('email'),
			'password' => $enc_password,
			'date' => date('Y-m-d H:i:s'),
			'role_id' => $this->input->post('roleid')

		);

		$this->security->xss_clean($data);
		$this->db->insert('staff', $data);
	}

	public function updatestaff()
	{
		$data = array(
			'name' => $this->input->post('name'),
			'phone' => $this->input->post('phone'),
			'email' => $this->input->post('email'),
			'date' => date('Y-m-d H:i:s'),
			'role_id' => $this->input->post('roleid')

		);

		$this->security->xss_clean($data);
		$this->db->where('staff_id', $this->input->post('staff_id'));
		$this->db->update('staff', $data);
	}

	public function get_staff()
	{

		$this->db->join('role', 'role.role_id = staff.role_id', 'left');

		$query = $this->db->get('staff');
		return $query->result_array();
	}

	public function get_staffinfo($staffid)
	{
		$this->db->join('role', 'role.role_id = staff.role_id', 'left');

		$this->db->where('staff_id', $staffid);
		$result = $this->db->get('staff');

		return $result->row_array();
	}

	public function del_staff($staffid)
	{
		$this->db->where('staff_id', $staffid);
		$this->db->delete('staff');
	}


	// staff End 

	// Users Get / Add / Update / Delete 

	public function add_user($enc_password)
	{
		$data = array(
			'username' => $this->input->post('username'),
			'email' => $this->input->post('email'),
			'password' => $enc_password,
			'UserRegistrationDate' => date('Y-m-d H:i:s')
			//'country' => $this->input->post('country'),
			//'city' => $this->input->post('city'),
			// 'picture' => $imgname
		);

		$this->security->xss_clean($data);
		$this->db->insert('users', $data);
	}

	public function update_user()
	{
		$data = array(
			'username' => $this->input->post('username'),
			'email' => $this->input->post('email')
		);

		$this->security->xss_clean($data);
		$this->db->where('id', $this->input->post('id'));
		$this->db->update('users', $data);
	}

	public function get_userinfo($userid)
	{
		$this->db->where('id', $userid);
		$result = $this->db->get('users');

		return $result->row_array();
	}

	public function del_user($userid)
	{
		$this->db->where('id', $userid);
		$this->db->delete('users');
	}

	public function get_users()
	{
		$this->db->order_by('users.id', 'DESC');
		$query = $this->db->get('users');
		return $query->result_array();
	}

	public function get_orders()
	{
		$this->db->order_by('users_orders.orderid', 'DESC');
		$this->db->join('academic_level', 'users_orders.academic_level_id = academic_level.academic_level_id', 'left');
		$this->db->join('typesofpaper', 'users_orders.typesofpaper_id = typesofpaper.typeofpaper_id', 'left');
		$this->db->join('noofdays', 'users_orders.noofday_id = noofdays.noofdays_id', 'left');
		$this->db->join('noofpages', 'users_orders.noofpage_id = noofpages.noofpages_id', 'left');

		$query = $this->db->get('users_orders');
		return $query->result_array();
	}

	public function get_invoices()
	{
		$this->db->order_by('users_orders.orderid', 'DESC');
		$this->db->join('academic_level', 'users_orders.academic_level_id = academic_level.academic_level_id', 'left');
		$this->db->join('typesofpaper', 'users_orders.typesofpaper_id = typesofpaper.typeofpaper_id', 'left');
		$this->db->join('noofdays', 'users_orders.noofday_id = noofdays.noofdays_id', 'left');
		$this->db->join('noofpages', 'users_orders.noofpage_id = noofpages.noofpages_id', 'left');

		$query = $this->db->get('users_orders');
		return $query->result_array();
	}

	public function get_countries()
	{
		$this->db->order_by('countries.country_name', 'ASC');
		$query = $this->db->get('countries');
		return $query->result_array();
	}


	public function get_cities($country_name)
	{
		$this->db->where('country_name', $country_name);
		$this->db->order_by('cities.city_name', 'ASC');
		$query = $this->db->get('cities');
		return $query->result_array();
	}

	public function get_bloggers()
	{
		$this->db->order_by('blogger.id', 'DESC');
		$query = $this->db->get('blogger');
		return $query->result_array();
	}

	public function get_admins()
	{
		$this->db->order_by('admin.id', 'DESC');
		$query = $this->db->get('admin');
		return $query->result_array();
	}

	public function add_admin($enc_password)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
			'password' => $enc_password,
			'register_date' => date('Y-m-d H:i:s')
		);

		$this->security->xss_clean($data);
		$this->db->insert('admin', $data);
	}

	public function add_city()
	{
		$data = array(
			'geoname_id' => $this->input->post('geoname_id'),
			'country_iso_code' => $this->input->post('country_iso_code'),
			'country_name' => $this->input->post('country_name'),
			'time_zone' => $this->input->post('time_zone'),
			'city_name' => $this->input->post('city_name')
		);

		$this->security->xss_clean($data);
		$this->db->insert('cities', $data);
	}



	// public function add_blogger($enc_password)
	// {
	// 	$data = array(
	//         'name' => $this->input->post('name'),
	// 		'username' => $this->input->post('username'),
	// 		'password' => $enc_password,
	// 		'register_date' => date('Y-m-d H:i:s')
	//     );

	// 	$this->security->xss_clean($data);
	//     $this->db->insert('blogger', $data);
	// }

	public function update_admin($admin_id, $enc_password)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
			'password' => $enc_password
		);

		$this->security->xss_clean($data);
		$this->db->where('id', $admin_id);
		$this->db->update('admin', $data);
	}


	public function update_blogger($blogger_id, $enc_password)
	{
		$data = array(
			'name' => $this->input->post('name'),
			'username' => $this->input->post('username'),
			'password' => $enc_password
		);
		$this->security->xss_clean($data);
		$this->db->where('id', $blogger_id);
		$this->db->update('blogger', $data);
	}


	public function del_admin($adminid)
	{
		$this->db->where('id', $adminid);
		$this->db->delete('admin');
	}
	public function del_bloggers($bloggerid)
	{
		$this->db->where('id', $bloggerid);
		$this->db->delete('blogger');
	}
	public function del_city($city_name)
	{
		$this->db->where('city_name', $city_name);
		$this->db->delete('cities');
	}

	public function get_jobs()
	{
		$this->db->order_by('jobs.jobid', 'DESC');
		$this->db->join('company', 'jobs.companyid = company.id', 'left');
		$query = $this->db->get('jobs');

		return $query->result_array();
	}

	public function get_approvejobs()
	{
		$this->db->order_by('jobs.jobid', 'DESC');
		$this->db->join('company', 'jobs.companyid = company.id', 'left');
		$query = $this->db->get('jobs');

		return $query->result_array();
	}

	public function get_pendingjobs()
	{
		$this->db->order_by('jobs.jobid', 'DESC');
		$this->db->join('company', 'jobs.companyid = company.id', 'left');
		$query = $this->db->get('jobs');

		return $query->result_array();
	}

	public function get_appliedjobs()
	{
		$this->db->order_by('applications.appid', 'DESC');
		$this->db->join('users', 'applications.userid = users.id', 'left');
		$this->db->join('jobs', 'applications.jobid = jobs.jobid', 'left');
		$query = $this->db->get('applications');

		return $query->result_array();
	}


	function do_upload()
	{

		$config = array(
			'upload_path' => "assets/uploads/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => false,
			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
			'max_height' => "5000",
			'max_width' => "5000"
		);

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('userfile')) {
			$imgdata = array('upload_data' => $this->upload->data());

			$imgname = $imgdata['upload_data']['file_name'];
		} else {
			$error = array('error' => $this->upload->display_errors());
			echo '<pre>';
			print_r($error);
			echo '<pre>';
			exit;
		}

		return $imgname;
	}


	public function generatevoucher()
	{
		$data = array(
			'voucher_code' => $this->input->post('voucher_code'),
			'voucher_price' => $this->input->post('voucher_price'),
			'user_id' => $this->input->post('user_id'),
			'vouchar_date' => date('Y-m-d H:i:s'),
			'status' => 'active'

		);

		$this->security->xss_clean($data);
		$this->db->insert('vouchers', $data);
	}

	public function update_vouchers()
	{

		$data = array(
			'voucher_code' => $this->input->post('voucher_code'),
			'voucher_price' => $this->input->post('voucher_price'),
			'user_id' => $this->input->post('user_id'),
			'vouchar_date' => date('Y-m-d H:i:s'),
			'status' => 'active'

		);

		$this->security->xss_clean($data);
		$this->db->where('voucher_id', $this->input->post('id'));
		$this->db->update('vouchers', $data);
	}

	public function get_voucher($voucher_id)
	{
		$this->db->where('voucher_id', $voucher_id);
		$result = $this->db->get('vouchers');

		return $result->row_array();
	}

	public function deletevoucher($voucher_id)
	{
		$this->db->where('voucher_id', $voucher_id);
		$this->db->delete('vouchers');
	}

	public function get_vouchers()
	{
		$this->db->order_by('vouchers.voucher_id', 'DESC');
		$query = $this->db->get('vouchers');
		return $query->result_array();
	}

	public function updateimage01($imgname)
	{
		$data = array(
			'image' => $imgname

		);

		$this->security->xss_clean($data);
		$this->db->where('slider_id', '1');
		$this->db->update('slider', $data);
	}
	public function updateimage02($imgname)
	{
		$data = array(
			'image' => $imgname

		);

		$this->security->xss_clean($data);
		$this->db->where('slider_id', '2');
		$this->db->update('slider', $data);
	}
	public function updateimage03($imgname)
	{
		$data = array(
			'image' => $imgname

		);

		$this->security->xss_clean($data);
		$this->db->where('slider_id', '3');
		$this->db->update('slider', $data);
	}

	public function img()
	{
		$query = $this->db->get('slider');
		return $query->result_array();
	}
}
