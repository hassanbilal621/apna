<?php
class Seller_model extends CI_Model
{

	// Product Start Get / Add / Update / Delete 

	public function login($username, $password)
	{


		$this->db->where('email', $username);
		$result = $this->db->get('seller');


		if ($result->num_rows() == 1) {
			$hash = $result->row(0)->password;

			if (password_verify($password, $hash)) {
				return $result->row(0)->seller_id;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function register($enc_password)
	{
		$data = array(
			'firstname' => $this->input->post('UserFirstName'),
			'lastname' => $this->input->post('UserLastName'),
			'username' => $this->input->post('username'),
			'email' => $this->input->post('email'),
			'number' => $this->input->post('UserPhone'),
			'shopname' => $this->input->post('shopname'),
			'address' => $this->input->post('UserAddress'),
			'Password' => $enc_password
		);

		$this->security->xss_clean($data);
		return $this->db->insert('seller', $data);
	}

	public function add_product()
	{
		$data = array(
			'ProductSKU' => $this->input->post('productsku'),
			'ProductName' => $this->input->post('productname'),
			'ProductPrice' => $this->input->post('productprice'),
			'ProductWeight' => $this->input->post('productweight'),
			'ProductCartDesc' => $this->input->post('productcartdesc'),
			'ProductShortDesc' => $this->input->post('productshortdesc'),
			'ProductLongDesc' => $this->input->post('productlongdesc'),
			'ProductThumb' => $this->input->post('productthumb'),
			//'country' => $this->input->post('country'),
			//'city' => $this->input->post('city'),
			'ProductCategoryID' => $this->input->post('categoryid'),
			'ProductUpdateDate' => date('Y-m-d H:i:s'),
			'ProductStock' => $this->input->post('productstock'),
			'ProductLive' => $this->input->post('productlive'),
			'ProductUnlimited' => $this->input->post('productunlimited'),
			'ProductLocation' => $this->input->post('productlocation'),
			'product_brand_id' => $this->input->post('productbrandid'),
			'productimg_id' => $this->input->post('productimg_id')
		);

		$this->security->xss_clean($data);
		$this->db->insert('products', $data);
	}

	public function update_product()
	{
		$data = array(
			'ProductSKU' => $this->input->post('productsku'),
			'ProductName' => $this->input->post('productname'),
			'ProductPrice' => $this->input->post('productprice'),
			'ProductWeight' => $this->input->post('productweight'),
			'ProductCartDesc' => $this->input->post('productcartdesc'),
			'ProductShortDesc' => $this->input->post('productshortdesc'),
			'ProductLongDesc' => $this->input->post('productlongdesc'),
			'ProductThumb' => $this->input->post('productthumb'),
			//'country' => $this->input->post('country'),
			//'city' => $this->input->post('city'),
			'ProductCategoryID' => $this->input->post('categoryid'),
			'ProductUpdateDate' => date('Y-m-d H:i:s'),
			'ProductStock' => $this->input->post('productstock'),
			'ProductLive' => $this->input->post('productlive'),
			'ProductUnlimited' => $this->input->post('productunlimited'),
			'ProductLocation' => $this->input->post('productlocation'),
			'product_brand_id' => $this->input->post('productbrandid'),
			'productimg_id' => $this->input->post('productimg_id'),
			// $productid = $this->input->post('ProductID')
		);

		$this->security->xss_clean($data);
		$this->db->where('ProductID', $this->input->post('ProductID'));
		// $this->db->where('seller_id', $seller_id);
		$this->db->update('products', $data);
	}
	public function	update_stock($total_product, $sellerid)
	{
		$data = array(
			'no_of_product' => $total_product,

		);
		$this->security->xss_clean($data);
		$this->db->where('seller_id', $sellerid);
		$this->db->update('seller', $data);
	}
	public function update_image($imgname, $sellerid)
	{
		$data = array(
			'image' => $imgname,

		);
		$this->security->xss_clean($data);
		$this->db->where('seller_id', $sellerid);
		$this->db->update('seller', $data);
	}
	public function update_profile($sellerid)
	{
		$data = array(
			'firstname' => $this->input->post('firstname'),
			'lastname' => $this->input->post('lastname'),
			'username' => $this->input->post('username'),
			'email' => $this->input->post('email'),
			'number' => $this->input->post('number'),
			'shopname' => $this->input->post('shopname'),
			'address' => $this->input->post('address'),
		);
		$this->security->xss_clean($data);
		$this->db->where('seller_id', $sellerid);
		$this->db->update('seller', $data);
	}

	public function get_products($seller_id)
	{
		// $this->db->join('subcategory','products.ProductCategoryID = subcategory.category_id','left');
		// $this->db->join('brands','brands.brand_id = products.product_brand_id','left');
		$this->db->join('product_img', 'product_img.productimg_id = products.productimg_id', 'left');
		$this->db->order_by('products.ProductID', 'desc');
		$this->db->where('seller_id', $seller_id);
		$query = $this->db->get('products');
		return $query->result_array();
	}

	public function get_product($seller_id, $productid)
	{
		$this->db->join('product_img', 'product_img.productimg_id = products.productimg_id', 'left');
		$this->db->order_by('products.ProductID', 'desc');
		$this->db->where('seller_id', $seller_id);
		$this->db->where('ProductID', $productid);
		$query = $this->db->get('products');
		return $query->row_array();
	}

	public function get_order_delever($seller_id)
	{
		$this->db->join('products', 'order_item.product_id = products.ProductID', 'left');
		$this->db->join('orders', 'order_item.order_id = orders.orderid', 'left');
		$this->db->join('users', 'order_item.userid = users.id', 'left');

		$this->db->order_by('order_item.order_id', 'desc');
		$this->db->where('order_item.status', 'delivered');
		$this->db->where('order_item.sellerid', $seller_id);
		$query = $this->db->get('order_item');
		return $query->result_array();
	}
	public function get_orders($seller_id)
	{
		$this->db->join('products', 'order_item.product_id = products.ProductID', 'left');
		$this->db->join('orders', 'order_item.order_id = orders.orderid', 'left');
		$this->db->join('users', 'order_item.userid = users.id', 'left');

		$this->db->order_by('order_item.order_id', 'desc');
		$this->db->where('order_item.sellerid', $seller_id);
		$query = $this->db->get('order_item');
		return $query->result_array();
	}

	public function get_order_pending($seller_id)
	{
		$this->db->join('products', 'order_item.product_id = products.ProductID', 'left');
		$this->db->join('orders', 'order_item.order_id = orders.orderid', 'left');
		$this->db->join('users', 'order_item.userid = users.id', 'left');

		$this->db->order_by('order_item.order_id', 'desc');
		$this->db->where('order_item.status', 'pending');
		$this->db->where('order_item.sellerid', $seller_id);
		$query = $this->db->get('order_item');
		return $query->result_array();
	}
	public function get_editproduct($productid)
	{

		$this->db->join('productcategories', 'productcategories.CategoryID = products.ProductCategoryID', 'left');
		$this->db->join('brands', 'brands.brand_id = products.product_brand_id', 'left');
		$this->db->where('ProductID', $productid);
		$query = $this->db->get('products');
		return $query->row_array();
	}


	public function deleteproduct($productid)
	{
		$this->db->where('ProductID', $productid);
		$this->db->delete('products');
	}

	public function add_image($productid, $carimage)
	{

		$data = array(

			'product_img' => $carimage,
			'product_id' => $productid

		);
		$this->security->xss_clean($data);
		$this->db->insert('product_img', $data);
	}




	public function get_product_image($productid)
	{

		$this->db->where('product_id', $productid);

		$query = $this->db->get('product_img');

		return $query->result_array();
	}


	public function get_image()
	{

		$this->db->join('products', 'products.ProductID = product_img.product_id', 'left');
		$query = $this->db->get('product_img');
		return $query->result_array();
	}

	public function get_sellerinfo($sellerid)
	{
		$this->db->where('seller_id', $sellerid);

		$query = $this->db->get('seller');

		return $query->row_array();
	}

	// Product End 
}
