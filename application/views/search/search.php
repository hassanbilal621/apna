    <div class="col s12">
        <?php if (isset($products)) { ?>

            <?php foreach ($products as $product1) : ?>
                <div class="col s12 m4 l4">
                    <div class="card animate fadeLeft">
                    <?php if ($product1['offer'] == '0') {
                                echo '<div class="card-badge"><a class="white-text"> <b>On Offer</b> </a></div>';
                            }
                            ?> 
                    <div class="card-content" style="min-height: 325px!important;">
                        <h6 class="m-0"><?php echo $product1['subcate_name']; ?></h6>
                        <span class="card-title text-ellipsis"><?php echo $product1['ProductName']; ?></span>
                        <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $product1['ProductThumb']; ?>" class="responsive-img" alt="" style="max-height: 200px;max-width: 100%;">
                        <h6 class="col s12 m12 l12 mt-2">Product By<?php echo $product1['shopname']; ?></h6>
                    </div>
                    <div class="card-content row">
                        <h5 class="col s12 m12 l">RS: <?php echo $product1['ProductPrice']; ?></h5>
                        <a class="col s12 m12 l4 mt-2 waves-effect waves-light submit btn modal-trigger" id="<?php echo $product1['ProductID']; ?>" onclick="ajax_view_product(this.id)">View</a>
                    </div>
                    </div>
                </div>
            <?php endforeach; ?>

            <?php } else { ?>
            <center>
                <p style="margin: 20px;">No product registered</p>
            </center>
            <?php } ?>
        </div>