<div class="container" style="width:1000px;">
   <div id="main">
      <div class="row">
         <div class="col s12">
            <div class="container">
               <div class="section mt-2" id="blog-list">
                  <div id="search">
                     <div class="input-field col s12">
                        <h5 class="mb-2">Search Your Blogs</h5>
                        <input placeholder="Search" id="searchblog" type="text" class="search-box validate white search-circle">
                     </div>
                  </div>
               </div>
               <div class="row">
                  <?php foreach ($blogs as $blog) : ?>
                     <div id="blogs">
                        <div id="<?php echo $blog['blog_id']; ?>" onclick="loadblogsview(this.id)" class="col s12 m4 l4">
                           <div class="card-panel border-radius-6 mt-10 card-animation-1">
                              <div style="min-height: 250px;">
                                 <a href="#"><img class="responsive-img border-radius-8 z-depth-4 image-n-margin" src="<?php echo base_url(); ?>assets/uploads/<?php echo $blog['blog_img']; ?>" alt="" style="max-height: 200px;"></a>
                                 <h6 class="deep-purple-text text-darken-3 mt-5"><?php echo $blog['blog_title']; ?></h6>
                                 <span><?php echo substr($blog['blog_desc'], 0, 50) ?>...</span>
                              </div>
                              <div class="row" style="border-top: 3px #f4dede solid;">
                                 <div class="col s6 mt-1">
                                    <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $blog['userimage']; ?>" alt="<?php echo $blog['username']; ?>" class="circle mr-3 width-30 vertical-text-middle">
                                    <span class="pt-2"><?php echo $blog['username']; ?></span>
                                 </div>
                                 <div class="col s6 mt-3 right-align social-icon"> <span class="ml-3 vertical-align-top" style="font-size: 12px;"> Date:<?php echo $blog['blog_date']; ?></span>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  <?php endforeach; ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>

<div id="modal3" class="modal">
   <div class="modal-content">
   </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquerynew.min.js" type="text/javascript"></script>
<script>
   function loadblogsedit(blogsid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>users/ajax_edit_blog/" + blogsid,
         success: function(data) {
            $(".modal-content").html(data);
            $('#modal3').modal('open');
         }
      });
   }
</script>
<div id="modal1" class="modal">
   <div class="modal-content">
   </div>
</div>
<script>
   function loadblogsview(blogsid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>users/ajax_view_blog/" + blogsid,
         success: function(data) {
            $(".modal-content").html(data);
            $('#modal1').modal('open');
         }
      });
   }
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
   $(document).ready(function() {
      $("#searchblog").on("keyup", function() {
         var value = $(this).val().toLowerCase();
         $("#blogs *").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
         });
      });
   });
</script> F