<div class="container" style="width:1000px;">
	<div id="main">
		<div class="row user-projects tab">
			<div class="col s12">
				<ul class="tabs card-border-gray mt-4">
					<li class="tab col s12 m6 l3 p-0"><a href="#profile">
							<i class="material-icons vertical-align-middle"><img src="<?php echo base_url(); ?>assets/app-assets/images/icon/eidtprofile.png" alt="avatar" style="height: 28px;margin: 10px 0px -7px 0;"></i> Edit Profile
						</a>
					</li>
					<li class="tab col s12 m6 l3 p-0"><a class="active" href="#order">
							<i class="material-icons vertical-align-middle"><img src="<?php echo base_url(); ?>assets/app-assets/images/icon/order.png" alt="avatar" style="height: 28px;margin: 10px 0px -7px 0;"></i> Orders
						</a>
					</li>
					<li class="tab col s12 m6 l3 p-0"><a href="#postblog">
							<i class="material-icons vertical-align-middle"><img src="<?php echo base_url(); ?>assets/app-assets/images/icon/blog.png" alt="avatar" style="height: 28px;margin: 10px 0px -7px 0;"></i>Post Blog
						</a>
					</li>
					<li class="tab col s12 m6 l3 p-0"><a href="#listblog">
							<i class="material-icons vertical-align-middle"><img src="<?php echo base_url(); ?>assets/app-assets/images/icon/list.png" alt="avatar" style="height: 28px;margin: 10px 0px -7px 0;"></i>Blog List
						</a>
					</li>
				</ul>
			</div>
		</div>
		<div id="profile" class=" tab card-content tab-pane fade">
			<div class="row">
				<div class="col s12">
					<div id="icon-prefixes" class="card card-tabs">
						<?php echo form_open_multipart('users/updateuser'); ?>
						<div class="card-content">
							<div class="card-title">
								<div class="row">
									<div class="input-field col s12">
										<h5 class="ml-4">Edit Your Profile</h5>
									</div>
								</div>
								<div class="row">
									<div class="input-field col s6">
										<h6>First name</h6>
										<input type="text" name="UserFirstName" value="<?php echo $user['UserFirstName'] ?>" required>
									</div>
									<div class="input-field col s6">
										<h6>Last name</h6>
										<input type="text" name="UserLastName" value="<?php echo $user['UserLastName'] ?>" required>
									</div>
								</div>
								<div class="row">
									<div class="input-field col s6">
										<h6>Username</h6>
										<input type="text" name="username" value="<?php echo $user['username'] ?>" required>
									</div>
									<div class="input-field col s6">
										<h6>Phone</h6>
										<input id="number" type="number" name="UserPhone" value="<?php echo $user['UserPhone'] ?>" required>
									</div>
								</div>
								<div class="row">
									<div class="input-field col s6">
										<h6>Email</h6>
										<input id="email" type="email" name="email" value="<?php echo $user['email'] ?>" required>
									</div>
									<div class="input-field col s6">
										<h6>User Registration Date</h6>
										<input type="text" name="UserRegistrationDate" value="<?php echo $user['UserRegistrationDate'] ?>" required>
									</div>
								</div>
								<div class="row">
									<div class="input-field col s12">
										<h6>Billing Address</h6>
										<input id="address" type="text" name="UserAddress" value="<?php echo $user['UserAddress'] ?>" required>
									</div>
								</div>
								<div class="row">
									<div class="input-field col s12">
										<h6>Profine Image</h6>
										<input type="file" id="input-file-now" class="dropify" data-default-file="" name="userfile" accept="image/*" />
									</div>
								</div>
								<div class="row">
									<div class="input-field col s12">
										<button class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12" type="submit" name="Save">Save</button>
									</div>
								</div>
							</div>
						</div>
						<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>
		<div id="order" class=" tab card-content tab-pane fade">
			<div class="row">
				<div class="col s12">
					<div class="card">
						<div class="card-content">
							<h4 class="card-title">Your Orders</h4>
							<table id="page-length-option" class="display">
								<thead>
									<tr>
										<th>Order ID</th>
										<th>Product</th>
										<th>Quantity</th>
										<th>Total</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($orders as $order) : ?>
										<tr>
											<td><?php echo $order['id']; ?></td>
											<td><?php echo $order['ProductName']; ?></td>
											<td><?php echo $order['qty']; ?></td>
											<td>$<?php echo $order['sub_total']; ?></td>
											<td><?php echo $order['status']; ?>
											</td>
										</tr>
									<?php endforeach; ?>
									</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="postblog" class=" tab card-content tab-pane fade">
			<div class="row">
				<div class="col s12">
					<div id="icon-prefixes" class="card card-tabs">
						<div class="card-content">
							<div class="card-title">
								<?php echo form_open_multipart('users/addblog'); ?>
								<div class="row">
									<div class="s12">
										<div class="row">
											<div class="input-field col s12">
												<h5 class="ml-4">Add Your Blog</h5>
											</div>
										</div>
										<div class="row">
											<div class="input-field col s12">
												<h6>Your Blog Title</h6>
												<input type="text" name="blog_title" value="<?php echo $user['UserFirstName'] ?>" required>
											</div>
										</div>
										<div class="row">
											<div class="input-field col s12">
												<h6>Your Blog Descripion</h6>
												<textarea name="blog_desc" minlength="40" class="descrip"></textarea>
											</div>
										</div>
										<div class="row">
											<div class="input-field col s12">
												<h6>Blog Image</h6>
												<input type="file" id="input-file-now" class="dropify" data-default-file="" name="userfile" accept="image/*" />
											</div>
										</div>
										<div class="row">
											<div class="input-field col s12">
												<button class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12" type="submit" name="Save">Save</button>
											</div>
										</div>
									</div>
								</div>
								<?php echo form_close(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="listblog" class=" tab card-content tab-pane fade">
			<div class="row">
				<div class="col s12">
					<div class="card">
						<div class="card-content">
							<h4 class="card-title">Your Blog</h4>
							<table id="scroll-vert-hor_wrapper" class="display">
								<thead>
									<tr>
										<th>Blog ID</th>
										<th>blogtitle</th>
										<th>Blog Descripion</th>
										<th>blog image</th>
										<th>Date</th>
										
									</tr>
								</thead>
								<tbody>
									<?php foreach ($blogs as $blog) : ?>
										<tr id="<?php echo $blog['blog_id']; ?>" onclick="loadblogsview(this.id)">
											<td><?php echo $blog['blog_id']; ?></td>
											<td><?php echo $blog['blog_title']; ?></td>
											<td><?php echo substr($blog['blog_desc'], 0, 10) ?>...</td>
											<td><img src="<?php echo base_url(); ?>assets\uploads\<?php echo $blog['blog_img']; ?>" alt="" style="max-width: 30%;max-height: 130px;"></td>
											<td><?php echo $blog['blog_date']; ?></td>
											
										</tr>
									<?php endforeach; ?>
									</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="modal1" class="modal">
   <div class="modal-content">
   </div>
</div>
<script>
   function loadblogsview(blogsid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>users/ajax_view_blog_user/" + blogsid,
         success: function(data) {
            $(".modal-content").html(data);
            $('#modal1').modal('open');
         }
      });
   }
</script>
<div id="modal3" class="modal">
   <div class="modal-content">
   </div>
</div>
<script>
   function loadblogsedit(blogsid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>users/ajax_edit_blog/" + blogsid,
         success: function(data) {
            $(".modal-content").html(data);
            $('#modal3').modal('open');
         }
      });
   }
</script>