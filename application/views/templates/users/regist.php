<div class="row">
  <div class="col s12">
    <div class="container">
      <div id="register-page" class="row">
        <div class="col s12 m6 l5  z-depth-4 card-panel border-radius-6 register-card bg-opacity-8">
          <div class="login-form">
            <div class="row user-projects tab">
              <div class="col s12">
                <ul class="tabs card-border-gray mt-4">
                  <li class="tab col s6"><a class="active" href="#user">
                      <img src="<?php echo base_url(); ?>assets/app-assets/images/icon/order.png" alt="avatar" style="height: 28px;margin: 10px 0px -7px 0;"></i> User Registration
                    </a>
                  </li>
                  <li class="tab col  s6"><a href="#seller">
                      <img src="<?php echo base_url(); ?>assets/app-assets/images/icon/eidtprofile.png" alt="avatar" style="height: 28px;margin: 10px 0px -7px 0;"></i>seller Registration
                    </a>
                  </li>
                </ul>
              </div>
              <div class="row">
                <div class="input-field ml-5 col s5">
                  <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/app-assets/images\logo/kalakaar logo.png" alt="" style="height: 150px; margin: auto;"></a>
                </div>
                <div class="input-field col s6">
                  <h5 class="ml-4 mt-10">Register</h5>
                  <p class="ml-4">Join to our community now !</p>
                </div>
              </div>

              <div id="user">
                <?php echo form_open('users/registers'); ?>
                <div class="row margin">
                  <div class="input-field col s12  m12 l6">
                    <i class="material-icons prefix pt-2">person_outline</i>
                    <input id="username" type="text" name="UserFirstName" placeholder="Type Your First Name" required>
                  </div>
                  <div class="input-field col s12  m12 l6">
                    <i class="material-icons prefix pt-2">remove</i>
                    <input id="username" type="text" name="UserLastName" placeholder="Type Your Last Name" required>
                  </div>
                </div>
                <div class="row margin">
                  <div class="input-field col s12  m12 l6">
                    <i class="material-icons prefix pt-2">person_outline</i>
                    <input id="username" type="text" name="username" placeholder="Type Your User Name" required>
                  </div>
                  <div class="input-field col s12  m12 l6">
                    <i class="material-icons prefix pt-2">lock_outline</i>
                    <input id="password" type="password" name="password" placeholder="Type Your Password" required>
                  </div>
                </div>
                <div class="row margin">
                  <div class="input-field col s12  m12 l6">
                    <i class="material-icons prefix pt-2">phone_outline</i>
                    <input id="number" type="number" name="UserPhone" placeholder="Type Your Number" required>
                  </div>

                  <div class="input-field col s12  m12 l6">
                    <i class="material-icons prefix pt-2">mail_outline</i>
                    <input id="email" type="email" name="email" placeholder="Type Your Email" required>
                  </div>
                </div>
                <div class="row margin">
                  <div class="input-field col s12">
                    <i class="material-icons prefix pt-2">pin_drop</i>
                    <input id="address" type="text" name="UserAddress" placeholder="Type Your address" required>
                  </div>
                </div>
                <div class="row margin">
                  <div class="input-field col s12">
                    <i class="material-icons prefix pt-2">photo</i>
                    <input type="file" id="input-file-now" class="dropify" data-default-file="" name="userfile" accept="image/*" />
                  </div>
                  <div class="row">
                    <div class="input-field col s12">
                      <button class="btn waves-effect waves-light border-round submit col s12">Register</button>
                    </div>
                  </div>
                  <?php echo form_close(); ?>
                  <div class="row">
                    <div class="input-field col s12">
                      <p class="margin medium-small"><a href="login">Already have an account? Login</a></p>
                    </div>
                  </div>
                </div>
              </div>
              <div id="seller">
                <?php echo form_open('Seller/register'); ?>
                <div class="row margin">
                  <div class="input-field col s12  m12 l6">
                    <i class="material-icons prefix pt-2">person_outline</i>
                    <input id="username" type="text" name="UserFirstName" placeholder="Type Your First Name" required>
                  </div>
                  <div class="input-field col s12  m12 l6">
                    <i class="material-icons prefix pt-2">remove</i>
                    <input id="username" type="text" name="UserLastName" placeholder="Type Your Last Name" required>
                  </div>
                </div>
                <div class="row margin">
                  <div class="input-field col s12  m12 l6">
                  <i class="material-icons prefix pt-2">phone_outline</i>
                    <input id="number" type="number" name="UserPhone" placeholder="Type Your Number" required>
                  </div>
                  <div class="input-field col s12  m12 l6">
                    <i class="material-icons prefix pt-2">lock_outline</i>
                    <input id="password" type="password" name="password" placeholder="Type Your Password" required>
                  </div>
                </div>
                <div class="row margin">
                  <div class="input-field col s12  m12 l6">
                  <i class="material-icons prefix pt-2">shop</i>
                    <input id="address" type="text" name="shopname" placeholder="Type Your Shop Name" required>
                  </div>

                  <div class="input-field col s12  m12 l6">
                    <i class="material-icons prefix pt-2">mail_outline</i>
                    <input id="email" type="email" name="email" placeholder="Type Your Email" required>
                  </div>
                </div>
                <div class="row margin">
                  <div class="input-field col s12">
                    <i class="material-icons prefix pt-2">pin_drop</i>
                    <input id="address" type="text" name="UserAddress" placeholder="Type Your address" required>
                  </div>
                </div>

                <!-- <div class="input-field col s12">
                    <i class="material-icons prefix pt-2">photo</i>
                    <input type="file" id="input-file-now" class="dropify" data-default-file="" name="userfile" accept="image/*" />
                  </div> -->
                <div class="row">
                  <div class="input-field col s12">
                    <button class="btn waves-effect waves-light border-round submit col s12">Register</button>
                  </div>
                </div>
                <?php echo form_close(); ?>
                <div class="row">
                  <div class="input-field col s12">
                    <p class="margin medium-small"><a href="login">Already have an account? Login</a></p>
                  </div>
                </div>
              </div>
              <?php echo form_close(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>