<div class="container" style="width:1000px;">
    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">
                        <div class="row">
                            <?php foreach ($onycategory as $oncategory) : ?>
                            <a href="<?php echo base_url(); ?>users/oncategory/<?php echo $oncategory['subcat_id']; ?>">
                                    <div class="col s12 m4 l4">
                                        <div class="card animate fadeUp">
                                            <div class="card-content">
                                                <span class="card-title text-ellipsis"><?php echo $oncategory['subcate_name']; ?></span>
                                                <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $oncategory['subcat_id']; ?>" class=" responsive-img" alt="" style="max-height: 200px;max-width: 100%;">

                                            </div>
                                        </div>
                                    </div>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>