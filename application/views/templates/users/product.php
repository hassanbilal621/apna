<div class="container" style="width:1000px;">
   <div id="main">
      <div class="row">
         <div class="col s12">
            <div class="container">
               <div class="section">
                  <div class="header-search-wrapper center mt-1">
                     <div class="row card-contant">
                        <div class="col s7">
                           <input class="header-search-input" type="text" id="searchuser" name="search" placeholder="Search Product">
                        </div>
                        <div class="col s3">
                           <div class="selected-box auto-hight">
                              <select class="form-control" id="categoryid" name="categoryid" required>
                                 <option disabled selected>Select Category</option>
                                 <?php foreach ($categories as $category) : ?>
                                    <?php if (empty($category['CategoryName'])) { } else {
                                          ?>
                                       <option value="<?php echo $category['CategoryID']; ?>"><?php echo $category['CategoryName']; ?></option>
                                    <?php } ?>
                                 <?php endforeach; ?>
                              </select>
                           </div>
                        </div>
                        <div class="col s2 p-0">
                           <!-- <a href="<?php echo base_url(); ?>users/product" class="btn waves-effect waves-light submit btn" type="sumbit">Search
                              <i class="material-icons left">search</i>
                           </a> -->

                           <button type="submit" onclick="getSearch()"; name="searchBtn" class="btn waves-effect waves-light submit btn">Search
                              <i class="material-icons left">search</i>
                           </button>

                        
                        </div>
                     </div>
                  </div>

                  <div class="row" id="ecommerce-products">
                     <div class="col s12">
                        <?php if (isset($products)) { ?>

                           <?php foreach ($products as $product1) : ?>
                              <div class="col s12 m4 l4">
                                 <div class="card animate fadeLeft">
                                    <?php if ($product1['offer'] == '0') {
                                             echo '<div class="card-badge"><a class="white-text"> <b>On Offer</b> </a></div>';
                                          }
                                          ?>
                                    <div class="card-content" style="min-height: 325px!important;">
                                       <h6 class="m-0"><?php echo $product1['subcate_name']; ?></h6>
                                       <span class="card-title text-ellipsis"><?php echo $product1['ProductName']; ?></span>
                                       <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $product1['ProductThumb']; ?>" class="responsive-img" alt="" style="max-height: 200px;max-width: 100%;">
                                       <h6 class="col s12 m12 l12 mt-2">Product By<?php echo $product1['shopname']; ?></h6>
                                    </div>
                                    <div class="card-content row">
                                       <h5 class="col s12 m12 l">RS: <?php echo $product1['ProductPrice']; ?></h5>
                                       <a class="col s12 m12 l4 mt-2 waves-effect waves-light submit btn modal-trigger" id="<?php echo $product1['ProductID']; ?>" onclick="ajax_view_product(this.id)">View</a>
                                    </div>
                                 </div>
                              </div>
                           <?php endforeach; ?>

                           <?php } else { ?>
                           <center>
                              <p style="margin: 20px;">No product registered</p>
                           </center>
                           <?php } ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>


<div id="modal1" class="modal">
   <div class="modal-content pt-2 modal-content2">
   </div>
</div>
<script type='text/javascript'>
   function ajax_view_product(productid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>users/ajax_view_product/" + productid,
         success: function(data) {
            $(".modal-content2").html(data);
            $('#modal1').modal('open');
         }
      });
   }
</script>


<script>
function getSearch() {
   var value =   document.getElementById("searchuser").value;
   var catid =   document.getElementById("categoryid").value;


    $.ajax({



    type: "GET",
    url: "<?php echo base_url(); ?>users/get_search_product",
    data:({search: value,CategoryID:catid}),
    success: function(data){
        $("#ecommerce-products").html(data);
    }
    });
}
</script>