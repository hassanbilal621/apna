<div class="container" style="width:1000px;">
   <div id="main">
      <div class="row">
         <div class="col s12">
            <div class="container">
               <div class="section">
                  <div class="header-search-wrapper center mt-1">
                     <!-- <?php echo form_open("users/get_search_product") ?>
                     <div class="row">
                        <div class="col s7">
                           <input class="header-search-input" type="text" id="searchuser" name="search" placeholder="Search Product">
                        </div>
                        <div class="col s3">
                           <div class="selected-box auto-hight">
                              <select class="form-control" name="categoryid" required>
                                 <option disabled selected>Select Category</option>
                                 <?php foreach ($categories as $category) : ?>
                                    <?php if (empty($category['CategoryName'])) { } else {
                                          ?>
                                       <option value="<?php echo $category['CategoryID']; ?>"><?php echo $category['CategoryName']; ?></option>
                                    <?php } ?>
                                 <?php endforeach; ?>
                              </select>
                           </div>
                        </div>
                        <div class="col s2 p-0">
                           <a href="<?php echo base_url(); ?>users/product" class="btn waves-effect waves-light submit btn" type="sumbit">Search
                              <i class="material-icons left">search</i>
                           </a> 

                           <button type="submit" name="searchBtn" class="btn waves-effect waves-light submit btn">Search
                              <i class="material-icons left">search</i>
                           </button>

                           <?php echo form_close(); ?> -->
                  </div>
               </div>
            </div>

            <div class="row">
               <div class="col s12">
                  <div class="slider">
                     <ul class="slides mt-2">
                        <?php foreach ($sliders as $slide) : ?>
                           <li>
                              <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $slide['image']; ?>" alt="img-1">
                              <!-- random image -->
                              <div class="caption center-align">
                                 <!-- <h3 class="black-text">
                                          <?php echo $slide['discription']; ?></h3>
                                        -->
                              </div>
                           </li>
                        <?php endforeach; ?>
                     </ul>
                  </div>
               </div>
            </div>

            <div class="row" id="ecommerce-products">
               <div class="col s12">
                  <?php if ($products) { ?>
                     <?php foreach ($products as $product) : ?>
                        <div class="col s12 m4 l4">
                           <div class="card animate fadeLeft">
                              <?php if ($product['offer'] == '0') {
                                       echo '<div class="card-badge"><a class="white-text"> <b>On Offer</b> </a></div>';
                                    }
                                    ?>
                              <div class="card-content" style="min-height: 325px!important;">
                                 <h6 class="m-0"><?php echo $product['subcate_name']; ?></h6>
                                 <span class="card-title text-ellipsis"><?php echo $product['ProductName']; ?></span>
                                 <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $product['ProductThumb']; ?>" class="responsive-img" alt="" style="max-height: 200px;max-width: 100%;">
                                 <a href="<?php echo base_url(); ?>shops/view/<?php echo $product['seller_id']; ?>">
                                    <h6 class="col s12 m12 l12 mt-2">Product By <?php echo $product['shopname']; ?></h6>
                                 </a>
                              </div>
                              <div class="card-content row">
                                 <h5 class="col s12 m12 l">RS: <?php echo $product['ProductPrice']; ?></h5>
                                 <a class="col s12 mt-2 waves-effect waves-light submit btn modal-trigger" id="<?php echo $product['ProductID']; ?>" onclick="ajax_view_product(this.id)">View</a>
                              </div>
                           </div>
                        </div>
                     <?php endforeach; ?>
                  <?php } else {
                     ?>
                     <center>
                        <img src="<?php echo base_url(); ?>assets/uploads/no-product-found.png" alt=""style="width: 80%;">
                     </center>
                  <?php
                  } ?>
               </div>
               <a href="<?php echo base_url(); ?>users/product">
                  <h6 class="normalheading center-align">More Product</h6>
               </a>
               <h5 class="normalheading center-align">Feature Shops</h5>
               <div class="row">
               <?php if ($sellers) { ?>
                  <?php foreach ($sellers as $seller) : ?>
                     <a href="<?php echo base_url(); ?>shops/view/<?php echo $seller['seller_id']; ?>">
                        <div class="col s12 m6 xl4">
                           <div id="profile-card" class="card">
                              <div class="card-image waves-effect waves-block waves-light">
                                 <img class="activator" src="<?php echo base_url(); ?>assets\uploads\<?php echo $seller['image']; ?>" alt="user bg" />
                              </div>
                              <div class="card-content">
                                 <img src="<?php echo base_url(); ?>assets\uploads\<?php echo $seller['image']; ?>" alt="" class="circle responsive-img activator card-profile-image cyan lighten-1 padding-2" />
                                 <h5 class="card-title activator grey-text text-darken-4"><?php echo $seller['shopname']; ?></h5>
                                 <p></p>
                                 <p><?php echo $seller['shopdesc']; ?>.</p>
                              </div>

                           </div>
                        </div>
                     </a>
                  <?php endforeach; ?>
                  <?php } else {
                     ?>
                     <center>
                        <img src="<?php echo base_url(); ?>assets/uploads/no-shop-found.png" alt=""style="width: 80%;">
                     </center>
                  <?php
                  } ?>
               </div>
               <a href="<?php echo base_url(); ?>shops/featureshop">
                  <h6 class="normalheading center-align">More Feature Shops</h6>
               </a>
               <h5 class="normalheading center-align">Blogs</h5>
               <div class="row">
               <?php if ($blogs) { ?>
                  <?php foreach ($blogs as $blog) : ?>
                  <div id="<?php echo $blog['blog_id']; ?>" onclick="loadblogsview(this.id)" class="col s12 m4 l4">
                        <div class="card-panel border-radius-6 mt-10 card-animation-1">
                           <div style="min-height: 250px;">
                              <a href="#"><img class="responsive-img border-radius-8 z-depth-4 image-n-margin" src="<?php echo base_url(); ?>assets/uploads/<?php echo $blog['blog_img']; ?>" alt="" style="max-height: 200px;"></a>
                              <h6 class="deep-purple-text text-darken-3 mt-5"><?php echo $blog['blog_title']; ?></h6>
                              <span><?php echo substr($blog['blog_desc'], 0, 50) ?>...</span>
                           </div>
                           <div class="row">
                              <div class="col s6 mt-1">
                                 <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $blog['userimage']; ?>" alt="<?php echo $blog['username']; ?>" class="circle mr-3 width-30 vertical-text-middle">
                                 <span class="pt-2"><?php echo $blog['username']; ?></span>
                              </div>
                              <div class="col s6 mt-3 right-align social-icon"> <span class="ml-3 vertical-align-top" style="font-size: 12px;"> Date:<?php echo $blog['blog_date']; ?></span>
                              </div>
                           </div>
                        </div>
                     </div>
                  <?php endforeach; ?>
                  <?php } else {
                     ?>
                     <center>
                        <img src="<?php echo base_url(); ?>assets/uploads/no-blogs-found.png" alt=""style="width: 80%;">
                     </center>
                  <?php
                  } ?>
               </div>
               <a href="<?php echo base_url(); ?>blogs">
                  <h6 class="normalheading center-align">More Blogs</h6>
               </a>
            </div>
         </div>
      </div>
   </div>
</div>



<div id="modal5" class="modal">
   <div class="modal-content pt-2 modal-content2">
   </div>
</div>
<script type='text/javascript'>
   function ajax_view_product(productid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>users/ajax_view_product/" + productid,
         data: 'country_name=pakistan',
         success: function(data) {
            $(".modal-content2").html(data);
            $('#modal5').modal('open');
         }
      });
   }
</script>

<div id="modal1" class="modal">
   <div class="modal-content">
   </div>
</div>
<script>
   function loadblogsview(blogsid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>users/ajax_view_blog/" + blogsid,
         success: function(data) {
            $(".modal-content").html(data);
            $('#modal1').modal('open');
         }
      });
   }
</script>