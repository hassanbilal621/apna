<div class="container" style="width:1000px;">
   <div id="main">
      <div class="row">
         <div class="col s12">
            <div class="container">
               <div id="search-page" class="section">
                  <div class="row">
                     <div class="col s12">
                        <div class="row">
                           <div class="col s12">
                              <div class="card z-depth-1">
                                 <?php echo form_open('users/billing'); ?>
                                 <div class="card-content">
                                    <div class="row">
                                       <h5>Billing Form</h5>
                                       <div class="col l8 m12" style="width:100%;">
                                          <div class="row video">

                                             <div class="col l4 m4 s12 p-3">
                                                <div class="row">
                                                   <div class="col s12">
                                                      <div class="card border-radius-6">
                                                         <form>
                                                            <div class="row">
                                                               <div class="input-field col s12">
                                                                  <h5 class="ml-4"><span class="btn-floating btn-small gradient-45deg-light-blue-cyan">1</span>
                                                                     Billing Address</h5>
                                                                  <hr class="p-0 mb-10">
                                                               </div>
                                                            </div>
                                                            <div class="row margin">
                                                               <div class="input-field col s12">
                                                                  <input id="username" type="text" name="fullname" value="<?php echo $user['UserFirstName'] ?> <?php echo $user['UserLastName'] ?>" required>
                                                                  <label for="username" class="center-align">Full
                                                                     Name</label>
                                                               </div>
                                                            </div>

                                                            <div class="row margin">
                                                               <div class="input-field col s12">
                                                                  <input id="number" type="number" name="phone" value="<?php echo $user['UserPhone'] ?>" required>
                                                                  <label for="number">Phone</label>
                                                               </div>
                                                            </div>
                                                            <div class="row margin">
                                                               <div class="input-field col s12">
                                                                  <input id="email" type="email" name="email" value="<?php echo $user['email'] ?>" required>
                                                                  <label for="email">Email</label>
                                                               </div>
                                                            </div>

                                                            <div class="row margin">
                                                               <div class="input-field col s12">
                                                                  <input id="address" type="text" name="address" value="<?php echo $user['UserAddress'] ?>" required>
                                                                  <label for="address">Address</label>
                                                               </div>
                                                            </div>
                                                            <div class="row margin">
                                                               <div class="input-field col s12">
                                                                  <input id="city" type="text" name="city" value="<?php echo $user['UserCity'] ?>" required>
                                                                  <label for="city">City</label>
                                                               </div>
                                                            </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="col l4 m4 s12">
                                                <div class="row">
                                                   <div class="col s12">
                                                      <div class="card lighten-4 border-radius-6">
                                                         <div class="row">
                                                            <div class="input-field col s12">
                                                               <h5 class="ml-4"><span class="btn-floating btn-small gradient-45deg-light-blue-cyan">2</span>
                                                                  Shipping Method</h5>
                                                               <hr class="p-0 mb-10">
                                                               <div class="card-content">
                                                                  <p class="teal-text lighten-2 truncate">
                                                                     Fixed PKR</p>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row">
                                                   <div class="col s12">
                                                      <div class="card lighten-4 border-radius-6">
                                                         <div class="row">
                                                            <div class="input-field col s12">
                                                               <h5 class="ml-4"><span class="btn-floating btn-small gradient-45deg-light-blue-cyan">3</span>
                                                                  Payment Method</h5>
                                                               <hr class="p-0 mb-10">
                                                               <div class="card-content">
                                                                  <p>
                                                                     <label>
                                                                        <input class="gradient-45deg-light-blue-cyan" name="payment" value="Paypal" type="radio" checked />
                                                                        <span>Paypal</span>
                                                                     </label>
                                                                  </p>
                                                                  <p>
                                                                     <label>
                                                                        <input name="payment" value="Credit Card Easy Pay" type="radio" />
                                                                        <span>Credit Card Easy
                                                                           Pay</span>
                                                                     </label>
                                                                  </p>
                                                                  <p>
                                                                     <label>
                                                                        <input class="" name="payment" value="Cash On Delivery" type="radio" />
                                                                        <span>Cash On
                                                                           Delivery</span>
                                                                     </label>
                                                                  </p>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>

                                                <div class="row">
                                                   <div class="col s12">
                                                      <div class="card lighten-4 border-radius-6">
                                                         <div class="row">
                                                            <div class="input-field col s12">
                                                               <h5 class="ml-4"><span class="btn-floating btn-small gradient-45deg-light-blue-cyan">4</span>
                                                                  Enter Voucher</h5>
                                                               <hr class="p-0">
                                                               <div class="card-content">
                                                                  <div class="row margin">
                                                                     <div class="input-field col s12">
                                                                        <input id="voucharcode" type="text" name="voucher">
                                                                        <label for="voucher" class="center-align">voucher</label>
                                                                     </div>
                                                                  </div>
                                                                  <button class="btn waves-effect waves-light border-round submit col s12" onclick="getdiscount()" ; type="submit">Get
                                                                     Discount</button>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>

                                             </div>
                                             <div class="col l4 m4 s12">
                                                <div class="row">
                                                   <div class="col s12">
                                                      <div class="card z-depth-0 grey lighten-4 border-radius-6 ">
                                                         <div class="row">
                                                            <div class="input-field col s12">
                                                               <h5 class="ml-4">Order Review</h5>
                                                               <hr class="p-0 mb-10">
                                                               <div class="card-content">
                                                                  <span>Sub Total :
                                                                     <?php echo $this->cart->total() ?></span><br />
                                                                  <span>Shipping : </span><br />
                                                                  <span>Total Items:
                                                                     <?php echo $this->cart->total_items(); ?></span><br />
                                                                  <span>Discount voucher:
                                                                  <h6  id="bankname" value=""></h6>
                                                                  </span>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="row">
                                                   <div class="col s12">
                                                      <form action="">
                                                         <div class="row">
                                                            <div class="input-field col s12">
                                                               <span class="helper-text" data-success="right">Comment</span>
                                                               <textarea style="height: 7rem;" name="comment" id="desc1" cols="30" rows="10"></textarea>
                                                            </div>
                                                         </div>
                                                         <div class="row">
                                                            <div class="input-field col s12" id="loginform1">
                                                               <button class="btn waves-effect waves-light border-round submit col s12" type="submit">Place
                                                                  Order</button>
                                                               <!-- <?php if (!$this->session->userdata('apna_seller_id')) { ?>
                                                                  <center>
                                                                     <?php if (!$this->session->userdata('apna_user_id')) { ?>
                                                                        <h6 class="ml-4">Click ThisLogin Button To Login Then Place Your Order</h6>
                                                                        <p><a onclick="loaduserinfo(this.id)" class="mb-6 btn waves-effect waves-light cyan modal-trigger mb-2 mr-1" href="#modal2">Sign In</a></p>

                                                                     <?php } else { ?>
                                                                        <button class="btn waves-effect waves-light border-round submit col s12" type="submit">Place
                                                                           Order</button>
                                                                     <?php } ?>
                                                                  <?php } else { ?>
                                                                     <h6 class="red-text ml-4">First you Logout Your Seller Account Then Login Your User Account to Place Your Order</h6>
                                                                  </center>

                                                               <?php } ?> -->

                                                            </div>
                                                         </div>
                                                      </form>


                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <?php echo form_close(); ?>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="modal2" class="modal" style="width:40%;">
   <div class="modal-content" style="width:40%; margin-left:30%;">
      <form method="post" action="do_login.php" onsubmit="return do_login();">
         <div class="row login-bg">
            <div class="col s12">
               <div class="container">
                  <div class="row">
                     <div class="col s12 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">
                        . <div class="login-form">
                           <div class="row">
                              <div class="input-field col s12 center">
                                 <img src="<?php echo base_url(); ?>assets/app-assets/images/logo/kalakaar logo.png" alt="" style="height: 130px;" class="responsive-img valign">

                                 <div class="row margin">
                                    <div class="input-field col s12">
                                       <i class="material-icons prefix pt-2">person_outline</i>
                                       <input id="emailid" type="email" name="username">
                                    </div>
                                 </div>
                                 <div class="row margin">
                                    <div class="input-field col s12">
                                       <i class="material-icons prefix pt-2">lock_outline</i>
                                       <input id="pass" type="password" name="password">
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="input-field col s12">
                                       <button type="submit" class="btn waves-effect waves-light border-round cyan col s12">Login</button>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </form>
   </div>
</div> 

<!-- 
<div id="modal2" class="modal" style="width:40%;">
   <div class="modal-content" style="width:40%; margin-left:30%;">
      <form method="post" action="do_login.php" onsubmit="return do_login();">
         <div class="row login-bg">
            <div class="col s12">
               <div class="container">
                  <div class="row">
                     <div class="col s12 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">
                        . <div class="login-form">
                           <div class="row">
                              <div class="input-field col s12 center">
                                 <img src="<?php echo base_url(); ?>assets/app-assets/images/logo/kalakaar logo.png" alt="" style="height: 130px;" class="responsive-img valign">
                                 <div class="row margin">
                                    <div class="input-field col s12">
                                       <i class="material-icons prefix pt-2">person_outline</i>
                                       <input id="emailid" type="email" name="username">
                                       <label for="username" class="center-align">Email</label>
                                    </div>
                                 </div>
                                 <div class="row margin">
                                    <div class="input-field col s12">
                                       <i class="material-icons prefix pt-2">lock_outline</i>
                                       <input id="pass" type="password" name="password">
                                       <label for="password">Password</label>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col s12 m12 l12 ml-2 mt-1">
                                       <p>
                                          <label>
                                             <input type="checkbox" />
                                             <span>Remember Me</span>
                                          </label>
                                       </p>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="input-field col s12">
                                       <button type="submit" class="btn waves-effect waves-light border-round cyan col s12">Login</button>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="input-field col s6 m6 l6">
                                       <p class="margin medium-small"><a href="<?php echo base_url(); ?>users/register" target="_blank">Register Now!</a></p>
                                    </div>
                                    <div class="input-field col s6 m6 l6">
                                       <p class="margin right-align medium-small"><a href="<?php echo base_url(); ?>users/forgot" target="_blank">Forgot password ?</a></p>
                                    </div>
                                 </div>
                              </div>
                              </div>
                              </div>
                              </div>
      </form>
   </div>
</div> -->

<script src="<?php echo base_url(); ?>assets/js/jquerynew.min.js" type="text/javascript"></script>

<script>
   function do_login() {
      var email = $("#emailid").val();
      var pass = $("#pass").val();

      // alert(email);
      // alert(pass);
      if (email != "" && pass != "") {
         $.ajax({
            type: 'get',
            url: '<?php echo base_url(); ?>users/ajaxlogin',
            data: {
               do_login: "do_login",
               email: email,
               password: pass
            },
            success: function(response) {
               if (response == "success") {
                  alert("Login Successfull");
                  $('#modal2').modal('close');
                  $("#setemail").val(email);
                  // $('#loginform1').css('display','none');

                  $("#loginform1").html(
                     '<button class="btn waves-effect waves-light border-round cyan col s12" type="submit" >Place Order</button>'
                  );

               } else {
                  alert("Wrong Details");
               }
            }
         });
      } else {
         alert("Please Fill All The Details");
      }

      return false;
   }
</script>

<script>
	function getdiscount{
      var value = document.getElementById("voucharcode").value;
		$.ajax({
			type: "GET",
			url: "<?php echo base_url(); ?>users/get_discount/",
			success: function(data){
				var obj = JSON.parse(data);
				
				document.getElementById("voucher_price").value = obj.voucher_price;
						
				
			}
			});
	}
</script>