<div class="container" style="width:1000px;">
   <div id="main">
      <div class="row">
         <div class="col s12">
            <div class="container">
               <div class="section">
                  <div class="header-search-wrapper center mt-1">
                     <input class="header-search-input" type="text" name="Search" placeholder="Search Product">
                  </div>
                  <div class="row" id="ecommerce-products">
                     <?php $this->load->view('templates/sidebar/sidebar.php'); ?>
                     <div class="col s12 m12 l9">
                        <?php foreach ($products as $product) : ?>
                           <div class="col s12 m4 l4">
                              <div class="card animate fadeLeft">
                                 <?php if($product['offer'] == '1' ) {
                                    echo '<div class="card-badge"><a class="white-text"> <b>On Offer</b> </a></div>';
                                 }
                                 ?>
                                 <div class="card-content" style="min-height: 290px;">
                                    <h6><?php echo $product['subcate_name']; ?></h6>
                                    <span class="card-title text-ellipsis"><?php echo $product['ProductName']; ?></span>
                                    <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $product['ProductThumb']; ?>" class="responsive-img" alt="" style="max-height: 200px;max-width: 100%;">
                                    <h6 class="col s12 m12 l12">Product By<?php echo $product['shopname']; ?></h6>
                                 </div>
                                 <div class="card-content row">
                                    <h5 class="col s12 m12 l">RS: <?php echo $product['ProductPrice']; ?></h5>
                                    <a class="col s12 m12 l4 mt-2 waves-effect waves-light submit btn modal-trigger" id="<?php echo $product['ProductID']; ?>" onclick="ajax_view_product(this.id)">View</a>
                                 </div>
                              </div>
                           </div>
                        <?php endforeach; ?>
                     </div>

                     <!-- Pagination -->
                     <div class="col s12 center">
                        <ul class="pagination">
                           <li class="disabled">
                              <a href="#!">
                                 <i class="material-icons">chevron_left</i>
                              </a>
                           </li>
                           <li class="active"><a href="#!">1</a>
                           </li>
                           <li class="waves-effect"><a href="#!">2</a>
                           </li>
                           <li class="waves-effect"><a href="#!">3</a>
                           </li>
                           <li class="waves-effect"><a href="#!">4</a>
                           </li>
                           <li class="waves-effect"><a href="#!">5</a>
                           </li>
                           <li class="waves-effect">
                              <a href="#!">
                                 <i class="material-icons">chevron_right</i>
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>



<div id="modal1" class="modal">
   <div class="modal-content pt-2 modal-content2">
   </div>
</div>
<script type='text/javascript'>
   function ajax_view_product(productid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>users/ajax_view_product/" + productid,
         data: 'country_name=pakistan',
         success: function(data) {
            $(".modal-content2").html(data);
            $('#modal1').modal('open');
         }
      });
   }
</script>