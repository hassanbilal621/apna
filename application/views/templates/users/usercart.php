<div id="main">
      <div class="row">
<div class="col s12">
                <div id="borderless-table" class="card card-tabs">
                  <div class="card-content">
                    <div class="card-title">
                      <div class="row">
                        <div class="col s12 m6 l10">
                          <h4 class="card-title">User Data</h4>
                          <p>Tables are borderless by default.</p>
                        </div>
                        <div class="col s12 m6 l2">
                          
                        </div>
                      </div>
                    </div>
                    <div id="view-borderless-table">
                      <div class="row">
                        <div class="col s12">
                          <table>
                            <thead>
                              <tr>
                                <th data-field="id">Item Name</th>
                                <th data-field="name">Invoice</th>
                                <th data-field="price">Item Price</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Alvin</td>
                                <td>Eclair</td>
                                <td>$0.87</td>
                               <td> <a class="waves-effect waves-light color red   btn">Delete</a>
                                <a class="waves-effect waves-light color green btn">Update</a></td>
                              </tr>
                              <tr>
                                <td>Alan</td>
                                <td>Jellybean</td>
                                <td>$3.76</td>
                                <td> <a class="waves-effect waves-light color red   btn">Delete</a>
                                <a class="waves-effect waves-light color green btn">Update</a></td>
                              </tr>
                              <tr>
                                <td>Jonathan</td>
                                <td>Lollipop</td>
                                <td>$7.00</td>
                                <td> <a class="waves-effect waves-light color red   btn">Delete</a>
                                <a class="waves-effect waves-light color green btn">Update</a></td>
                              </tr>
                              
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                
                </div>
              </div>
         </div>
</div>
</div>