<div id="main">
      <div class="row">

<div class="col s12">
                <div class="card animate fadeUp">
                 
                  <div class="card-content">
                    <div class="row" id="product-four">
                      <div class="col m6">
                        <img src="<?php echo base_url(); ?>assets/app-assets/images/cards/car4.png" class="responsive-img" alt="">
                      </div>
                      <div class="col m6">
                        <p>Honda</p>
                        <h5>HONDA CIVIC TYPE R</h5>
                        <span class="new badge left ml-0 mr-2" data-badge-caption="">4.2 Star</span>
                        <p>Availability: <span class="green-text">Available</span></p>
                        <hr class="mb-5">
                        <span class="vertical-align-top mr-4"><i class="material-icons mr-3">favorite_border</i>Wishlist</span>
                        <p class="mt-3">- Dual output USB interfaces</p>
                        <p>- Compatible with all smartphones</p>
                        <p>- Portable design and light weight</p>
                        <p>- Battery type: Lithium-ion</p>
                        <h5 class="red-text">$79.00 <span class="grey-text lighten-2 ml-3">$199.00</span> </h5>
                        <a class="waves-effect waves-light btn btn  mt-2" style="background: #3299cc">BUY NOW</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
</div>
</div>