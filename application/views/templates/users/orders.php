<div class="container" style="width:1000px;">
   <div id="main">
      <div class="row">

         
         <!-- Page Length Options -->
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Order</h4>
                  <div class="row">
                     <div class="col s12">
                        <table id="page-length-option" class="display">
                           <thead>
                              <tr>
                                 <th>Order ID</th>
                                 <th>Product</th>
                                 <th>Quantity</th>
                                 <th>Total</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php foreach($orders as $order):?>
                              <tr>
                                 <td><?php echo $order['id'];?></td>
                                 <td><?php echo $order['ProductName'];?></td>
                                 <td><?php echo $order['qty'];?></td>
                                 <td>$<?php echo $order['sub_total'];?></td>
                                 <!-- <td>
                                    <a href="<?php echo base_url(); ?>users/del_placeorder/<?php echo $order['id']; ?>" class="btn waves-effect waves-light red" type="submit" name="action">Delete
                                    <i class="material-icons left">delete_forever</i>
                                    </button>
                                 </td> -->
                              </tr>
                              <?php endforeach;?>
                              </tfoot>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- BEGIN VENDOR JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->