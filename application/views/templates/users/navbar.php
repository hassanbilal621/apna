<header class="page-topbar" id="header">
   <div class="navbar navbar-fixed">
      <nav class="navbar-main background-color white">
         <div class="container" style="width:1000px;">


            <?php if (!$this->session->userdata('apna_seller_id')) { ?>
               <?php if (!$this->session->userdata('apna_user_id')) { ?>
                  <div class="nav-wrapper">
                     <ul class="left">
                        <li>
                           <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="<?php echo base_url(); ?>users"><img src="<?php echo base_url(); ?>assets/app-assets/images/logo/kalakaar logo.png" alt=" logo" style="margin-bottom:-10px; height: 40px; margin-top:-10px" ;><span class="logo-text hide-on-med-and-down" style="color:black" ;>Apna Kalakaar</span></a></h1>
                        </li>
                     </ul>

                  </div>
                  <ul class="navbar-list right">
                     <li><a class="waves-effect  waves-light shopping-button" href="<?php echo base_url(); ?>users/cart" data-target="Shopping-dropdown"><span class="avatar-status avatar-online"><img src="<?php echo base_url(); ?>assets/app-assets/images/icon/cart.png" alt="avatar" style="border-radius: 0;background: no-repeat;"><small class="notification-badge orange accent-3"><?php echo $this->cart->total_items(); ?></small></span></a></li>

                     <li><a class="waves-effect waves-dark" style="color: black;" href="<?php echo base_url(); ?>login">Login</a></li>
                     <li><a class="waves-effect waves-dark" style="color: black;" href="<?php echo base_url(); ?>register">Register</a></li>
                  <?php } else { ?>
                     <div class="nav-wrapper">
                        <ul class="left">
                           <li>
                              <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="<?php echo base_url(); ?>users"><img src="<?php echo base_url(); ?>assets/app-assets/images/logo/kalakaar logo.png" alt=" logo" style="margin-bottom:-10px; height: 40px; margin-top:-10px" ;><span class="logo-text hide-on-med-and-down" style="color:black" ;>Apna Kalakaar</span></a></h1>
                           </li>
                        </ul>

                     </div>
                     <ul class="navbar-list right">
                        <li><a class="waves-effect  waves-light shopping-button" href="<?php echo base_url(); ?>users/cart" data-target="Shopping-dropdown"><span class="avatar-status avatar-online"><img src="<?php echo base_url(); ?>assets/app-assets/images/icon/cart.png" alt="avatar" style="border-radius: 0;background: no-repeat;"><small class="notification-badge orange accent-3"><?php echo $this->cart->total_items(); ?></small></span></a></li>

                        <li><a class="waves-effect waves-dark" style="color: black;" href="<?php echo base_url(); ?>users/profile"><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $this->session->userdata('userimage') ?>" alt="" class="responsive-img" style="margin: 0 10px -15% 0px;max-height: 50px;" ><?php echo $this->session->userdata('UserFirstName') ?></a></li>
                        <li><a class="waves-effect waves-dark" style="color: black;" href="<?php echo base_url(); ?>users/logout">Logout</a></li>

                        </li> <?php } ?>
                  <?php } else { ?>
                     <div class="nav-wrapper">
                        <ul class="left">
                           <li>
                              <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="<?php echo base_url(); ?>seller"><img src="<?php echo base_url(); ?>assets/app-assets/images/logo/kalakaar logo.png" alt=" logo" style="margin-bottom:-10px; height: 40px; margin-top:-10px" ;><span class="logo-text hide-on-med-and-down" style="color:black" ;>Apna Kalakaar</span></a></h1>
                           </li>
                        </ul>

                     </div>
                     <ul class="navbar-list right">
                        <li><a class=" waves-effect waves-dark" style="color: black;" href="<?php echo base_url(); ?>seller/profile"><?php echo $this->session->userdata('firstname') ?></a></li>
                        <li><a class="waves-effect waves-dark" style="color: black;" href="<?php echo base_url(); ?>seller/logout">Logout</a></li>
                        </li> <?php } ?>

                     <li><a href="#" data-target="contact-sidenav" style="color: black;" class="sidenav-trigger hide-on-large-only"><i class="material-icons">menu</i></a></li>
                     </ul>
         </div>
      </nav>

   </div>
</header>