<div class="container" style="width:1000px;">
    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">
                        <div class="row" id="ecommerce-products">
                            <?php $this->load->view('templates/sidebar/sidebar.php'); ?>
                            <!-- slider -->

                            <!-- end slider -->
                            <div class="col s12 m12 l9">
                                <div class="row">
                                    <?php foreach ($sellers as $seller):?>
                                    <div class="col s12 m4">
                                        <div class="card blue-grey darken-4 bg-image-1">
                                            <div class="card-content white-text">
                                                <span
                                                    class="card-title font-weight-400 mb-10"><?php echo $seller['shopname'];?></span>

                                                <div class="border-non mt-5">
                                                    <a href="<?php echo base_url();?>shops/view/<?php echo $seller['seller_id'];?>"
                                                        class="waves-effect waves-light btn red border-round box-shadow">View</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php endforeach;?>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>