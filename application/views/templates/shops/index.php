<div class="container" style="width:1000px;">
    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">
                        <div class="row" id="ecommerce-products">
                            <div class="row">
                                <?php foreach ($sellers as $seller) : ?>
                                    <a href="<?php echo base_url(); ?>shops/view/<?php echo $seller['seller_id']; ?>">
                                        <div class="col s12 m6 xl4">
                                            <div id="profile-card" class="card">
                                                <div class="card-image waves-effect waves-block waves-light">
                                                    <img class="activator" src="<?php echo base_url(); ?>assets\uploads\<?php echo $seller['image']; ?>" alt="user bg" />
                                                </div>
                                                <div class="card-content">
                                                    <img src="<?php echo base_url(); ?>assets\uploads\<?php echo $seller['image']; ?>" alt="" class="circle responsive-img activator card-profile-image cyan lighten-1 padding-2" />
                                                    <h5 class="card-title activator grey-text text-darken-4"><?php echo $seller['shopname']; ?></h5>
                                                    <p></p>
                                                    <p><?php echo $seller['shopdesc']; ?>.</p>
                                                </div>

                                            </div>
                                        </div>
                                    </a>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>