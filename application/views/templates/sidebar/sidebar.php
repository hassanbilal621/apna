<div class="sidebar-list-padding app-sidebar sidenav" id="contact-sidenav">
   <div class="col s12 m3 l3 contact-list display-grid">
      <div class="card br-1">
         <div class="card-content">
            <!-- <span class="card-title"><i class="material-icons mr-2">event_note</i><a href="<?php echo base_url(); ?>blogs"> Blogs</a></span>
            <hr class="p-0 mb-10">
            <span class="card-title"><i class="material-icons mr-2">shop</i><a href="<?php echo base_url(); ?>shops/"> Shop </a></span>-->
            <hr class="p-0 mb-10">
            <span class="card-title">Categories</span>
            <hr class="p-0 mb-10">
            <ul class="collapsible categories-collapsible">
               <?php foreach ($categories as $category) : ?>
                  <li>
                     <div class="collapsible-header"><?php echo $category['CategoryName']; ?><i class="material-icons"> keyboard_arrow_right </i>
                     </div>
                     <div class="collapsible-body">
                        <?php foreach ($subcategories as $subcategory) :
                              if ($category['CategoryID'] == $subcategory['category_id']) { ?>
                              <p>
                                 <a class="black-text" href="<?php echo base_url(); ?>users/catagory/<?php echo $subcategory['subcat_id']; ?>"><?php echo $subcategory['subcate_name']; ?>
                                 </a>
                              </p>
                        <?php }
                           endforeach; ?>
                     </div>
                  </li>
               <?php endforeach; ?>
            </ul>
         </div>
      </div>
   </div>
</div>