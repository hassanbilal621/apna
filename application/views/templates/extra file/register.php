<div class="container" style="width:1000px;">
  <div id="main">
    <div class="row">
      <div class="col s12">
        <div class="container">
          <div id="register-page" class="row">
            <div class="col s12 m6 l4 z-depth-4 card-panel border-radius-6 register-card bg-opacity-8" style="width:50%;">
              <?php echo form_open('Seller/register'); ?>
                <div class="row margin">
                  <div class="input-field col s12">
                    <i class="material-icons prefix pt-2">person_outline</i>
                    <input id="username" type="text" name="name" required>
                    <label for="username" class="center-align">Name</label>
                  </div>
                </div>

                <div class="row margin">
                  <div class="input-field col s12">
                    <i class="material-icons prefix pt-2">mail_outline</i>
                    <input id="email" type="email" name="email" required>
                    <label for="email">Email</label>
                  </div>
                </div>
                <div class="row margin">
                  <div class="input-field col s12">
                    <i class="material-icons prefix pt-2">address_outline</i>
                    <input id="address" type="text" name="address" required>
                    <label for="address">Address</label>
                  </div>

                </div>
                <div class="row margin">
                  <div class="input-field col s12">
                    <i class="material-icons prefix pt-2">lock_outline</i>
                    <input id="password" type="password" name="password" required>
                    <label for="password">Password</label>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s12">
                    <button class="btn waves-effect waves-light border-round cyan col s12" type="submit" name="register">Register</button>
                  </div>
                </div>
                <div class="row">
                  <div class="input-field col s12">
                    <p class="margin medium-small"><a href="seller/login">Already have an account? Login</a></p>
                  </div>
                </div>
                <?php echo form_close(); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>