<div class="container" style="width:1000px;">
   <div id="main">
      <div class="row">
         <div class="col s12">
            <div class="container">
               <div class="section">
                  <div class="row" id="ecommerce-products">
                     <div class="sidebar-list-padding app-sidebar sidenav" id="contact-sidenav">
                        <div class="col s12 m3 l3 contact-list display-grid">
                           <div class="card">
                              <div class="card-content">
                                 <span class="card-title"><i class="material-icons mr-2">event_note</i><a href="<?php echo base_url(); ?>users/blog"> Blogs</a></span>
                                 <hr class="p-0 mb-10">
                                 <span class="card-title"><i class="material-icons mr-2">shop</i><a href="<?php echo base_url(); ?>users/shop"> Shop </a></span>
                                 <hr class="p-0 mb-10">
                                 <span class="card-title">Categories</span>
                                 <hr class="p-0 mb-10">
                                 <ul class="collapsible categories-collapsible">
                                    <?php foreach ($categories as $category) : ?>
                                       <li>
                                          <div class="collapsible-header"><?php echo $category['CategoryName']; ?></div>
                                       </li>
                                    <?php endforeach; ?>
                                 </ul>
                                 <span class="card-title mt-10">Price</span>
                                 <hr class="p-0 mb-10">
                                 <div id="price-slider"></div>
                                 <span class="card-title mt-10">Size</span>
                                 <hr class="p-0 mb-10">
                                 <div class="size-filter">
                                    <ul>
                                       <li><a href="#">XL</a></li>
                                       <li><a href="#">L</a></li>
                                       <li class="active"><a href="#">M</a></li>
                                       <li><a href="#">S</a></li>
                                       <li><a href="#">XS</a></li>
                                    </ul>
                                 </div>
                                 <span class="card-title mt-10">Color</span>
                                 <hr class="p-0 mb-10">
                                 <form action="#" class="display-grid">
                                    <label class="mt-3">
                                       <input type="checkbox" />
                                       <span><i class="material-icons vertical-align-bottom blue-grey-text text-lighten-5"> lens </i> White</span>
                                    </label>
                                    <label class="mt-3">
                                       <input type="checkbox" />
                                       <span><i class="material-icons vertical-align-bottom black-text"> lens </i> Black</span>
                                    </label>
                                    <label class="mt-3">
                                       <input type="checkbox" />
                                       <span><i class="material-icons vertical-align-bottom amber-text"> lens </i> Amber</span>
                                    </label>
                                    <label class="mt-3">
                                       <input type="checkbox" />
                                       <span><i class="material-icons vertical-align-bottom blue-text"> lens </i> Blue</span>
                                    </label>
                                    <label class="mt-3">
                                       <input type="checkbox" />
                                       <span><i class="material-icons vertical-align-bottom green-text"> lens </i> Green</span>
                                    </label>
                                    <label class="mt-3">
                                       <input type="checkbox" />
                                       <span><i class="material-icons vertical-align-bottom link-text"> lens </i> Pink</span>
                                    </label>
                                    <label class="mt-3">
                                       <input type="checkbox" />
                                       <span><i class="material-icons vertical-align-bottom yellow-text"> lens </i> Yellow</span>
                                    </label>
                                 </form>
                                 <span class="card-title mt-10">Brand</span>
                                 <hr class="p-0 mb-10">
                                 <form action="#" class="display-grid">
                                    <label>
                                       <input type="checkbox" />
                                       <span>Apple</span>
                                    </label>
                                    <label>
                                       <input type="checkbox" />
                                       <span>Samsung</span>
                                    </label>
                                    <label>
                                       <input type="checkbox" />
                                       <span>Dell</span>
                                    </label>
                                    <label>
                                       <input type="checkbox" />
                                       <span>Sony</span>
                                    </label>
                                    <label>
                                       <input type="checkbox" />
                                       <span>Nokia</span>
                                    </label>
                                    <label>
                                       <input type="checkbox" />
                                       <span>JBL</span>
                                    </label>
                                    <label>
                                       <input type="checkbox" />
                                       <span>Philips</span>
                                    </label>
                                 </form>
                                 <span class="card-title mt-10">Offers</span>
                                 <hr class="p-0 mb-10">
                                 <form action="#" class="display-grid">
                                    <label>
                                       <input type="checkbox" />
                                       <span>Offer</span>
                                    </label>
                                    <label>
                                       <input type="checkbox" />
                                       <span>No Cost EMI</span>
                                    </label>
                                    <label>
                                       <input type="checkbox" />
                                       <span>Special Price</span>
                                    </label>
                                    <label>
                                       <input type="checkbox" />
                                       <span>Bank Offer</span>
                                    </label>
                                 </form>
                                 <span class="card-title mt-10">Customer Ratings</span>
                                 <hr class="p-0 mb-10">
                                 <form action="#" class="display-grid">
                                    <label>
                                       <input type="checkbox" />
                                       <span>
                                          <i class="material-icons amber-text"> star </i>
                                          <i class="material-icons amber-text"> star </i>
                                          <i class="material-icons amber-text"> star </i>
                                          <i class="material-icons amber-text"> star </i>
                                          <i class="material-icons amber-text"> star </i>
                                       </span>
                                    </label>
                                    <label>
                                       <input type="checkbox" />
                                       <span>
                                          <i class="material-icons amber-text"> star </i>
                                          <i class="material-icons amber-text"> star </i>
                                          <i class="material-icons amber-text"> star </i>
                                          <i class="material-icons amber-text"> star </i>
                                       </span>
                                    </label>
                                    <label>
                                       <input type="checkbox" />
                                       <span>
                                          <i class="material-icons amber-text"> star </i>
                                          <i class="material-icons amber-text"> star </i>
                                          <i class="material-icons amber-text"> star </i>
                                       </span>
                                    </label>
                                    <label>
                                       <input type="checkbox" />
                                       <span>
                                          <i class="material-icons amber-text"> star </i>
                                          <i class="material-icons amber-text"> star </i>
                                       </span>
                                    </label>
                                    <label>
                                       <input type="checkbox" />
                                       <span>
                                          <i class="material-icons amber-text"> star </i>
                                       </span>
                                    </label>
                                 </form>
                              </div>
                           </div>
                        </div>
                     </div>
                     <a href="#" data-target="contact-sidenav" class="sidenav-trigger hide-on-large-only"><i class="material-icons">menu</i></a>
                     <div class="col s12 m12 l9">
                        <?php foreach ($products as $product) : ?>
                           <div class="col s12 m4 l4">
                              <div class="card animate fadeLeft">
                                 <div class="card-content">
                                    <p><?php echo $product['CategoryName']; ?></p>
                                    <span class="card-title text-ellipsis"><?php echo $product['ProductName']; ?></span>
                                    <img src="<?php echo base_url(); ?>assets/app-assets/images/cards/cameras.png" class="responsive-img" alt="">
                                    <!-- <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $productimg['product_img']; ?>" alt="avatar" style="width:150px; height:150px;"> -->
                                    <div class="row">
                                       <h5 class="col s12 m12 l8 mt-3">$<?php echo $product['ProductPrice']; ?></h5>
                                       <a id="<?php echo $product['ProductID']; ?>" class="col s12 m12 l4 mt-2 waves-effect waves-light gradient-45deg-deep-purple-blue btn modal-trigger" onclick="ajax_view_product(this.id)">View</a>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        <?php endforeach; ?>
                        <!-- Pagination -->
                        <!-- <div class="col s12 center">
                        <ul class="pagination">
                          <li class="disabled">
                            <a href="#!">
                              <i class="material-icons">chevron_left</i>
                            </a>
                          </li>
                          <li class="active"><a href="#!">1</a>
                          </li>
                          <li class="waves-effect"><a href="#!">2</a>
                          </li>
                          <li class="waves-effect"><a href="#!">3</a>
                          </li>
                          <li class="waves-effect"><a href="#!">4</a>
                          </li>
                          <li class="waves-effect"><a href="#!">5</a>
                          </li>
                          <li class="waves-effect">
                            <a href="#!">
                              <i class="material-icons">chevron_right</i>
                            </a>
                          </li>
                        </ul>
                        </div> -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="modal1" class="modal">
   <div class="modal-content pt-2 modal-content2">
   </div>
</div>
<script type='text/javascript'>
   function ajax_view_product(productid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>users/ajax_view_product/" + productid,
         success: function(data) {
            $(".modal-content2").html(data);
            $('#modal1').modal('open');
         }
      });
   }
</script>