<div class="container" style="width:1000px;">
   <div id="main">
      <div class="row">
         <div class="col s12">
            <div class="container">
               <div class="section mt-2" id="blog-list">
                  <div class="card">
                     <div class="card-content">
                        <h5 class="breadcrumbs-title">Blog </h5>
                        <!-- Blog Style One -->
                        <!-- Fashion Card -->
                        <div class="col s12 m8 16" style="margin-left:18%;">
                           <div class="card-panel border-radius-6 mt-10 card-animation-1">
                              <a href="#"><img class="responsive-img border-radius-8 z-depth-4 image-n-margin" src="<?php echo base_url(); ?>assets/app-assets/images/cards/news-fashion.jpg"
                                 alt=""></a>
                              <h6 class="deep-purple-text text-darken-3 mt-5"><a href="#">Fashion</a></h6>
                              <span>Fashion is a popular style, especially in clothing, footwear, lifestyle, accessories, makeup, hairstyle
                              and body.</span>
                              <div class="row mt-4">
                                 <div class="col s5 mt-1">
                                    <img src="<?php echo base_url(); ?>assets/app-assets/images/user/9.jpg" alt="fashion" class="circle mr-3 width-30 vertical-text-middle">
                                    <span class="pt-2">Taniya</span>
                                 </div>
                                 <div class="col s7 mt-3 right-align social-icon">
                                    <span class="material-icons">favorite_border</span> <span class="ml-3 vertical-align-top">340</span>
                                    <span class="material-icons ml-10">chat_bubble_outline</span> <span class="ml-3 vertical-align-top">80</span>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- Apple News -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>