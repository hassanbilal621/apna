<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google.">
   <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template, eCommerce dashboard, analytic dashboard">
   <meta name="author" content="ThemeSelect">
   <title>User Profile Page | Materialize - Material Design Admin Template</title>
   <link rel="apple-touch-icon" href="<?php echo base_url(); ?>assets/app-assets/images/favicon/apple-touch-icon-152x152.png">
   <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/app-assets/images/favicon/kalakaar-favicon.png">
   <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

   <!-- BEGIN: VENDOR CSS-->
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/vendors/vendors.min.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/vendors/flag-icon/css/flag-icon.min.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/css/jquery.dataTables.min.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/extensions/responsive/css/responsive.dataTables.min.css">
   <!-- END: VENDOR CSS-->
   <!-- BEGIN: Page Level CSS-->
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/themes/horizontal-menu-template/materialize.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/themes/horizontal-menu-template/style.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/layouts/style-horizontal.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/pages/user-profile-page.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/pages/eCommerce-products-page.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/pages/app-contacts.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/pages/login.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/pages/register.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/pages/advance-ui-media.css">

   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/pages/data-tables.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/custom/custom.css">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/vendors/dropify/css/dropify.css">

   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/app-assets/css/mystyle.css">

   <!-- END: Custom CSS-->
</head>
<!-- END: Head-->

<body class="horizontal-layout page-header-light horizontal-menu 2-columns  " data-open="click" data-menu="horizontal-menu" data-col="2-columns">