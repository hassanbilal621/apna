<div class="container" style="width:1000px;">
    <div id="main">
        <div class="row">
            <div class="col s12">
                <div class="container">
                    <div class="section">
                        <div class="row" id="ecommerce-products">
                            <div class="row">
                                <div class="card user-card-negative-margin z-depth-0">
                                    <div class="col s12">
                                        <div>
                                            <div class="row">
                                                <div class="col s12">
                                                    <div class="slider">
                                                        <ul class="slides mt-2">
                                                            <?php foreach ($sliders as $slide) : ?>
                                                                <li>
                                                                    <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $slide['sliderimage']; ?>" alt="img-1">
                                                                    <!-- random image -->
                                                                    <div class="caption center-align">
                                                                        <h3 class="black-text">
                                                                            <?php echo $slide['slidertext_1']; ?></h3>
                                                                        <h5 class="light white-texttext-lighten-3">
                                                                            <?php echo $slide['slidertext_2']; ?>.</h5>
                                                                    </div>
                                                                </li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class=" user-card-negative-margin z-depth-0">
                                    <a href="<?php echo base_url(); ?>shops/">
                                        <div class="col s6">
                                            <div class="row">
                                                <div class="col s12">
                                                    <div class="card shopimage border-radius-3 animate fadeUp">
                                                        <div class="card-content center">
                                                            <h5 class="white-text">Shops</h5>
                                                            <h6 class="white-text">We've compiled a list of online
                                                                eCommerce stores to shop based on quality, value and
                                                                variety Click to visit stores.</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <a href="<?php echo base_url(); ?>blogs/">
                                        <div class="col s6">
                                            <div class="row">
                                                <div class="col s12">
                                                    <div class="card blogimage darken-1 border-radius-3 animate fadeUp">
                                                        <div class="card-content center">
                                                            <h5 class="black-text">Blogs</h5>
                                                            <h6 class="black-text">List of the top Product Review blogs to
                                                                follow to get best reviews on technical, beauty, experience
                                                                before buying/investing in it</h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 m12 l9">
                                    <div class="row">
                                        <?php foreach ($sellers as $seller) : ?>
                                            <div class="col s12 m4">
                                                <div class="card blue-grey darken-4 bg-image-1">
                                                    <div class="card-content white-text">
                                                        <span class="card-title font-weight-400 mb-10"><?php echo $seller['shopname']; ?></span>

                                                        <div class="border-non mt-5">
                                                            <a href="<?php echo base_url(); ?>shops/view/<?php echo $seller['seller_id']; ?>" class="waves-effect waves-light btn red border-round box-shadow">View</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>