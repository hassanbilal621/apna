<?php echo form_open('users/updatecart'); ?>
<div class="container" style="width:1000px;">
   <div id="main">
      <?php $i = 1; ?>
      <?php foreach ($this->cart->contents() as $items) : ?>

         <?php echo form_hidden($i . '[rowid]', $items['rowid']); ?>
         <div class="row">
            <div class="col s12 m12 l12">
               <div id="basic-tabs check" class="card card card-default scrollspy">
                  <div class="card-content pb-5">
                     <div class="col s12 m1 l1" style="padding: 15px 16px 16px 17px;">
                        <P><?php echo $i ?>.</P>
                     </div>
                     <div class="col s12 m1 l1 "><img src="<?php echo base_url(); ?>assets/uploads/<?php if (!isset($items['picture'])) {
                                                                                                         echo "";
                                                                                                      } else {
                                                                                                         echo $items['picture'];
                                                                                                      } ?>" class="responsive-img" alt=""></div>
                     <div class="col s12 m5 l5">
                        <p> <?php echo $items['name']; ?> </p>
                        <p> Available Stock is Only <?php echo $items['ProductStock']; ?></p>
                        <?php if ($this->cart->has_options($items['rowid']) == TRUE) : ?>
                           <p>
                              <?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value) : ?>
                                 <strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?> ,
                              <?php endforeach; ?>
                           </p>
                        <?php endif; ?>
                     </div>
                     <div class="col s12 m1 l1">
                        <!-- <p>Price: <?php echo $this->cart->format_number($items['price']); ?> </p> -->
                        <p><strong> Total: </strong> <?php echo $this->cart->format_number($items['subtotal']); ?> </p>
                        <!-- <p>24%off</p> -->
                     </div>
                     <div class="qty col s12 m2 l2">

                        <input type="number" name="<?php echo  $i . '[qty]' ?>" value="<?php echo  $items['qty'] ?>" min="1" max="<?php echo $items['ProductStock']; ?>" style="width: 0px;">

                     </div>
                     <a href="<?php echo base_url(); ?>users/deleterowcart/<?php echo $items['rowid']; ?>" class=" col s12 m2 l2 btn waves-effect waves-light delete ">delete</a>
                  </div>
               </div>
            </div>
         </div>
         <?php $i++; ?>
      <?php endforeach; ?>
      <div class="row">
         <div class="col s12 m12 l12">
            <div id="basic-tabs check" class="card card card-default center">
               <div class="card-content pb-5">
                  <h6 class="blue-text text-darken-2">Terms & Condition</h6>
                  <p>You know, being a test pilot isn't always the healthiest business
                     in the world<span id="dots">...</span><span id="more" style="display: none;">We predict too
                        much for the next year and yet far too little for the next
                        10..</span><a onclick="myFunction()" id="myBtn">Read more</a>
                  </p>
                  <center>
                     <a class="mb-6 btn waves-effect waves-light gradient-45deg-deep-purple-blue btn modal-trigger" href="<?php echo base_url(); ?>shops/" type="submit">Continue Shopping</a>
                     <button type="submit" class="mb-6 btn waves-effect waves-light submit-1">Update your Cart</button>

                     <?php if ($this->cart->contents()) { ?><a class="mb-6 btn waves-effect waves-light submit btn modal-trigger" href="<?php echo base_url(); ?>users/billing" type="submit">CheckOut</a> <?php } ?>
                  </center>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<script>
   function myFunction() {
      document.getElementById("Check").disabled = true;
   }

   function myFunction() {
      var dots = document.getElementById("dots");
      var moreText = document.getElementById("more");
      var btnText = document.getElementById("myBtn");

      if (dots.style.display === "none") {
         dots.style.display = "inline";
         btnText.innerHTML = "Read more";
         moreText.style.display = "none";
      } else {
         dots.style.display = "none";
         btnText.innerHTML = "Read less";
         moreText.style.display = "inline";
      }
   }
</script>