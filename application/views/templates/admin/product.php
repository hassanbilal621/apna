<!-- <style>
   img
   	{
   		max-width:200px;
   		margin-top: 10px;
           display: content;
   	}
      </style> -->
      <div id="main">
   <div class="row">
      <div class="col s12">
         <div class="card">
            <div class="card-content">
               <div class="row">
                  <div class="col s12">
                     <?php echo form_open_multipart('admin/uploadImage') ?>
                     <div class="row section">
                        <div class="col s12 m4 l4">
                           <p>Upload Product Image</p>
                           <input id="img2" type='file' name='files[]' multiple="" onchange="previewimg(this);" accept="image/*" >
                           <input type="hidden" name="ProductID">
                           <button class="waves-light btn button blue darken-3  z-depth-2  mt-5 " type="sumbit" >upload image</button>    
                           <img id="view" src="<?php echo base_url(); ?>assets/images/select.png" alt="your image">
                        </div>
                        <div class="col s12 m8 l8">
                           <div class="carousel">
                              <?php foreach($productimgs as $productimg): ?>
                              <a class="carousel-item modal-trigger" id="<?php echo $productimg['productimg_id']; ?>" onclick="openModal(this.id, this.getAttribute('data-imagenaame'))" data-imagenaame="<?php echo base_url(); ?>assets/uploads/<?php echo $productimg['product_img']; ?>"><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $productimg['product_img']; ?>"></a>   
                              <?php endforeach; ?>                          
                           </div>
                        </div>
                        <div id="modal1" class="modal">
                           <div class="modal-content">
                              <center> <img src="" id="modalimage"/></center>
                           </div>
                           <div class="modal-footer">
                              <a class="danger btn button" id="deleteimage" href=""> Delete Image </a>
                              <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Close</a>
                           </div>
                        </div>
                        <div class="col s12 m12 l12">
                        </div>
                     </div>
                     <?php echo form_close() ?>
                     <?php echo form_open('admin/product');?>
                     <div class="col s12">
                        <!-- Form with placeholder -->
                        <h4 class="card-title">Add Products</h4>
                        <div class="row">
                           <div class="input-field col s12">
                              <label for="name2">Product SKU </label>
                              <input id="SKU" type="text" name="productsku" >
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <label for="name2">Name</label>
                              <input id="name2" type="text" name="productname" >
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <label for="name2">Price</label>
                              <input id="Price" type="text" name="productprice" ?>
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <label for="name2">Product Weight</label>
                              <input id="Weight" type="text" name="productweight" >
                           </div>
                        </div>
                        <div class="row">
                           <div class="col s12">
                              <label for="Category">Select Category *</label>
                              <div class="selected-box auto-hight">
                                 <select class="form-control" name="categoryid" required>
                                    <option disabled selected>Select Category</option>
                                    <?php foreach ($categories as $category): ?>
                                    <?php if (empty($category['CategoryName'])) { }
                                       else{        
                                       ?>
                                    <option value="<?php echo $category['CategoryID']; ?>"><?php echo $category['CategoryName']; ?></option>
                                    <?php }?>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col s12">
                              <label for="Brand">Select Brand *</label>
                              <div class="selected-box auto-hight">
                                 <select class="form-control" name="productbrandid" required>
                                    <option disabled selected>Select Brand</option>
                                    <?php foreach ($brands as $brand): ?>
                                    <?php if (empty($brand['brand_name'])) { }
                                       else{        
                                       ?>
                                    <option value="<?php echo $brand['brand_id']; ?>"><?php echo $brand['brand_name']; ?></option>
                                    <?php }?>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <span class="helper-text"  data-success="right">Product Cart Description</span>
                              <textarea style="height: 7rem;" name="productcartdesc" id="desc1" cols="30" rows="10"></textarea>
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <span class="helper-text"  data-success="right">Product Short Description</span>
                              <textarea style="height: 7rem;" name="productshortdesc" id="desc2" cols="30" rows="10"></textarea>
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <span class="helper-text"  data-success="right">Product Long Description</span>
                              <textarea style="height: 7rem;" name="productlongdesc"  id="desc3" cols="30" rows="10"></textarea>
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <label for="name2">Product Thumb</label>
                              <input id="Thumb" type="text" name="productthumb">
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <label for="name2">Product Stock</label>
                              <input id="Stock" type="text" name="productstock">
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <label for="name2">Product Live</label>
                              <input id="Live" type="text" name="productlive">
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <label for="name2">Product Unlimited</label>
                              <input  id="Unlimited" type="text" name="productunlimited">
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <label for="name2">Product Location</label>
                              <input  id="Location" type="text" name="productlocation">
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                              <i class="material-icons right">send</i>
                              </button>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php echo form_close();?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!--                               
   <script>
       function openModal(imageid, imagelink){
   
   
           document.getElementById("modalimage").src = imagelink; 
           document.getElementById("deleteimage").href = "<?php echo base_url(); ?>admin/delete_stock_picture/"+imageid+"?stockid=<?php echo $stock['stock_id']; ?>"; 
   
           $('#modal1').modal('open');
           // alert("sadasd");
           // alert(imageid );
   
       }
   
   </script> -->
<script>
   function previewimg(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();
   
              reader.onload = function (e) {
                  $('#view')
                      .attr('src', e.target.result);
              };
   
              reader.readAsDataURL(input.files[0]);
          }
      }
</script>