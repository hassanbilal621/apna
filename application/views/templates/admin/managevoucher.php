<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Manage vouchers</h4>
                  <table id="page-length-option" class="display">
                     <thead>
                        <tr>
                           <th>Vouchar Id</th>
                           <th>Vouchar Code</th>
                           <th>Vouchar Price</th>
                           <th>User Name</th>
                           <th>Vouchar Date</th>
                           <th>Status</th>
                           <th>Delete</th>
                        </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <?php foreach ($vouchers as $voucher) : ?>
                              <td><?php echo $voucher['voucher_id']; ?></td>
                              <td><?php echo $voucher['voucher_code']; ?></td>
                              <td><?php echo $voucher['voucher_price']; ?></td>
                              <td><?php echo $voucher['user_id']; ?></td>
                              <td><?php echo $voucher['vouchar_date']; ?></td>
                              <td><?php echo $voucher['status']; ?></td>
                              <td>
                                 <!-- <button id="<?php echo $voucher['voucher_id']; ?>" onclick="loaduserinfo(this.id)" class="btn waves-effect waves-light blue btn">Edit
                                    <i class="material-icons left">edit</i>
                                 </button> -->
                                 <a href="<?php echo base_url(); ?>admin/del_cat/<?php echo $voucher['voucher_id']; ?>" class="btn waves-effect waves-light red" type="submit" name="action">Delete
                                    <i class="material-icons left">delete_forever</i>
                                 </a>
                              </td>
                        </tr>
                     <?php endforeach; ?>
                     </tfoot>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>

<!-- BEGIN VENDOR JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->


<div id="modal2" class="modal">
   <div class="modal-content">

   </div>
</div>


<script src="<?php echo base_url(); ?>assets/js/jquerynew.min.js" type="text/javascript"></script>

<script type='text/javascript'>
   function loaduserinfo(catid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>admin/ajax_edit_vouchermodal/" + catid,
         success: function(data) {
            $(".modal-content").html(data);
            $('#modal2').modal('open');
         }
      });
   }
</script>