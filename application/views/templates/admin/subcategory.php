<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <div class="row">
                     <div class="col s12">
                        <?php echo form_open('admin/subcategory');?>
                        <div class="col s12">
                           <!-- Form with placeholder -->
                           <h4 class="card-title">Create Sub Category</h4>
                           <div class="row">
                              <div class="input-field col s12">
                                 <input id="subcat2" type="text" name="subcate_name">
                                 <label for="subcat2">Sub Category</label>
                              </div>
                           </div>
                           <div class="row">
                           <div class="col s12">
                              <label for="Category">Select Category *</label>
                              <div class="selected-box auto-hight">
                                 <select class="form-control" name="categoryid" required>
                                    <option disabled selected>Select Category</option>
                                    <?php foreach ($categories as $category): ?>
                                    <?php if (empty($category['CategoryName'])) { }
                                       else{        
                                       ?>
                                    <option value="<?php echo $category['CategoryID']; ?>"><?php echo $category['CategoryName']; ?></option>
                                    <?php }?>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                           </div>
                           <div class="row">
                              <div class="input-field col s12">
                                 <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                 <i class="material-icons right">send</i>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                     <?php echo form_close();?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<!-- BEGIN VENDOR JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->