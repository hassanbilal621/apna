<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <div class="row">
                     <div class="col s12">
                        <?php echo form_open_multipart('admin/brand');?>
                        <div class="col s12">
                           <!-- Form with placeholder -->
                           <h4 class="card-title">Create Brands</h4>
                           <div class="row">
                              <div class="input-field col s12">
                                 <input  id="name2" type="text" name="brand_name">
                                 <label for="name2">Brand Name</label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="input-field col s12">
                                 <span for="img2">Brand Logo</span>
                                 <input  id="img2" type="file" name="userfile">
                              </div>
                           </div>
                           <div class="row">
                              <div class="input-field col s12">
                                 <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                 <i class="material-icons right">send</i>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                     <?php echo form_close();?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<!-- BEGIN VENDOR JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->