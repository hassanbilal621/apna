<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Products</h4>
                  <div class="row">

                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Product Name</th>
                              <th>Date</th>
                              <th>Price</th>
                              <th>Category</th>
                              <th>Brand</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach ($products as $product) : ?>
                              <tr>
                                 <td><?php echo $product['ProductID']; ?></td>
                                 <td><?php echo $product['ProductName']; ?></td>
                                 <td><?php echo $product['ProductUpdateDate']; ?></td>
                                 <td><?php echo $product['ProductPrice']; ?></td>
                                 <td><?php echo $product['subcate_name']; ?></td>
                                 <td><?php echo $product['brand_name']; ?></td>
                                 <!-- <td><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $product['ProductImage']; ?>" alt="avatar" style="width:100px; height:100px;"></td> -->
                                 <td>
                                    <a href="<?php echo base_url(); ?>admin/edit_product/<?php echo $product['ProductID']; ?>" class="btn waves-effect waves-light blue btn">Edit
                                       <i class="material-icons left">edit</i>
                                    </a>

                                    <a href="<?php echo base_url(); ?>admin/del_product/<?php echo $product['ProductID']; ?>" class="btn waves-effect waves-light red" type="submit" name="action">Delete
                                       <i class="material-icons left">delete_forever</i>
                                    </a>
                                    <?php if($product['homepage'] == 'show'){?>
                                       <a href="<?php echo base_url(); ?>admin/hidehome/<?php echo $product['ProductID']; ?>" class="btn waves-effect waves-light blue" type="submit" name="action">hide in homepage
                                       <i class="material-icons left">send</i>
                                    </a>
                                    <?php } else{?>
                                    <a href="<?php echo base_url(); ?>admin/showhome/<?php echo $product['ProductID']; ?>" class="btn waves-effect waves-light black-text white" type="submit" name="action">Show in homepage
                                       <i class="material-icons left">send</i>
                                    </a>
                                    <?php }?>
                                 </td>
                              </tr>

                           <?php endforeach; ?>
                           </tfoot>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>