
<div class="row login-bg">
   <div class="col s12">
      <div class="container">
         <div id="login-page" class="row">
            <div class="col s8 m6 l4 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">
               <?php echo form_open('admin/login'); ?>
               <div class="login-form">
                  <div class="row">
                     <div class="input-field col s12 center">
                        <img src="<?php echo base_url(); ?>assets/app-assets/images/logo/kalakaar logo.png" alt="" style="height: 130px;" class="responsive-img valign">
                        <?php if ($this->session->flashdata('login_failed')) : ?>
                           <div id="card-alert" class="card gradient-45deg-amber-amber">
                              <div class="card-content white-text">
                                 <p> <?php echo $this->session->flashdata('login_failed'); ?></p>
                              </div>
                              <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                 <span aria-hidden="true">×</span>
                              </button>
                           </div>
                        <?php endif; ?>
                        <?php if ($this->session->flashdata('user_loggedout')) : ?>
                           <div id="card-alert" class="card green">
                              <div class="card-content white-text">
                                 <p><?php echo $this->session->flashdata('user_loggedout'); ?></p>
                              </div>
                              <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                 <span aria-hidden="true">×</span>
                              </button>
                           </div>
                        <?php endif; ?>
                        <p class="center login-form-text">Login With Your Admin Account</p>
                     </div>
                  </div>
                  <div class="row margin">
                     <div class="input-field col s12">
                        <i class="material-icons prefix pt-2">person_outline</i>
                        <input id="username" name="username" type="text" placeholder="Username" style="width: 85%!important;">

                     </div>
                  </div>
                  <div class="row margin">
                     <div class="input-field col s12">
                        <i class="material-icons prefix pt-2">lock_outline</i>
                        <input id="password" name="password" type="password" placeholder="Password" style="width: 85%!important;">

                     </div>
                  </div>
                  <div class="row">
                     <div class="input-field col s12">
                        <button type="submit" name="login" class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1 col s12">Login</button>
                     </div>
                  </div>
                  <div class="row">
                     <div class="input-field col s6 m6 l6">
                        <p class="margin medium-small"><a href="<?php echo base_url(); ?>admin/register">Register Now!</a></p>
                     </div>
                  </div>
               </div>
            </div>
            <?php echo form_close(); ?>
         </div>
      </div>
   </div>
</div>