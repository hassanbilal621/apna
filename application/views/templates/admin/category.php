<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <div class="row">
                     <div class="col s12">
                        <?php echo form_open_multipart('admin/category');?>
                        <div class="col s12">
                           <!-- Form with placeholder -->
                           <h4 class="card-title">Create Categories</h4>
                           <div class="row">
                              <div class="input-field col s12">
                                 <input id="name2" type="text" name="categoryname">
                                 <label for="name2">Name </label>
                              </div>
                           </div>
                          
                           <div class="row">
                              <div class="input-field col s12">
                                 <span for="img2">Category Image</span>
                                 <input  id="img2" type="file" name="userfile">
                              </div>
                           </div>
                           <div class="row">
                              <div class="input-field col s12">
                                 <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                 <i class="material-icons right">send</i>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                     <?php echo form_close();?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>
<!-- BEGIN VENDOR JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->
<div id="modal2" class="modal">
   <div class="modal-content">
   </div>
</div>
<script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>
<script type='text/javascript'>
   function loaduserinfo(catid){
      // var userid = this.id;
         $.ajax({
            type: "GET",
            url: "<?php echo base_url();?>admin/ajax_edit_categorymodal/"+catid,
            success: function(data){
               $(".modal-content").html(data);
               $('#modal2').modal('open');
            }
         });
   }
   
   
</script>