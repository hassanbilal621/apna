<!-- BEGIN: SideNav-->
<aside class="sidenav-main nav-expanded nav-lock nav-collapsible sidenav-dark #3d4451  darken-3 sidenav-active-rounded">
   <div class="brand-sidebar">
      <a href="<?php echo base_url(); ?>admin/" class="darken-1">
         <span class="logo-text hide-on-med-and-down" style="font-size:44px; color:white;"> <img src="<?php echo base_url(); ?>assets/app-assets/images/logo/dark kalakaar logo.png" style=" width:65px !important; height:30px !important; " alt="Kalakar">kalakar</span>
      </a>
      <!-- <a class="brand-logo darken-1" ><img src="https://i.imgur.com/zYMsdbC.png" alt=""/> <span class="logo-text hide-on-med-and-down">Materialize</span> </a> -->
      <!-- <h1 class="logo-wrapper"><a class="darken-1" href="index.html"><img src="https://i.imgur.com/zYMsdbC.png" alt=""/><span class="logo-text hide-on-med-and-down">Materialize</span></a><a class="navbar-toggler" href="#"><i class="material-icons">radio_button_checked</i></a></h1> -->
   </div>
   <ul class="sidenav sidenav-collapsible leftside-navigation collapsible sidenav-fixed menu-shadow" id="slide-out" data-menu="menu-navigation" data-collapsible="menu-accordion">
      <li style="border-bottom: 1px solid #3d4451;" class="navigation-header"><a class="navigation-header-text">Dashboard</a><i class="navigation-header-icon material-icons">more_horiz</i>
      </li>
      <li style="border-bottom: 1px solid #3d4451;" class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/"><i class="material-icons">dashboard</i><span class="menu-title" data-i18n="">Home</span></a>
      </li>
      <li style="border-bottom: 1px solid #3d4451;" class="navigation-header"><a class="navigation-header-text">Main</a><i class="navigation-header-icon material-icons">more_horiz</i>
      </li>
      <!-- product -->
      <li class="bold" style="border-bottom: 1px solid #3d4451;">
         <a class="collapsible-header waves-effect waves-cyan " href="#"><i class="material-icons">local_grocery_store</i><span class="menu-title" data-i18n="">Product</span></a>
         <div class="collapsible-body">
            <ul>
               <li style="border-bottom: 1px solid #3d4451;"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/product" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Add Product</span></a></li>
               <li style="border-bottom: 1px solid #3d4451;"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/manageproduct" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Manage Product</span></a></li>
               <li style="border-bottom: 1px solid #3d4451;"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/homepageproduct" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Home Page Product</span></a></li>

            </ul>
         </div>
      </li>
      <!-- category -->
      <li style="border-bottom: 1px solid #3d4451;" class="bold">
         <a class="collapsible-header waves-effect waves-cyan " href="#"><i class="material-icons">add_shopping_cart</i><span class="menu-title" data-i18n="">Category</span></a>
         <div class="collapsible-body">
            <ul>
               <li style="border-bottom: 1px solid #3d4451;"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/category" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Add Category</span></a></li>
               <li style="border-bottom: 1px solid #3d4451;"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/managecategory" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Manage Category</span></a></li>

            </ul>
         </div>
      </li>

      <!-- sub category -->
      <li style="border-bottom: 1px solid #3d4451;" class="bold">
         <a class="collapsible-header waves-effect waves-cyan " href="#"><i class="material-icons">tab_unselected</i><span class="menu-title" data-i18n="">Sub Categoty</span></a>
         <div class="collapsible-body">
            <ul>
               <li style="border-bottom: 1px solid #3d4451;"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/subcategory" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Add Sub-Category</span></a>
               </li>
               <li style="border-bottom: 1px solid #3d4451;"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/managesubcategory" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Manage Sub-Category</span></a>
               </li>

            </ul>
         </div>
      </li>


      <!-- brands -->
      <li style="border-bottom: 1px solid #3d4451;" class="bold">
         <a class="collapsible-header waves-effect waves-cyan " href="#"><i class="material-icons">local_offer</i><span class="menu-title" data-i18n="">Brands</span></a>
         <div class="collapsible-body">
            <ul>
               <li style="border-bottom: 1px solid #3d4451;"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/brand" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Add Brand</span></a>
               </li>
               <li style="border-bottom: 1px solid #3d4451;"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/managebrand" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Manage Brand</span></a>
               </li>

            </ul>
         </div>
      </li>

      <!-- voucher -->
      <li style="border-bottom: 1px solid #3d4451;" class="bold">
         <a class="collapsible-header waves-effect waves-cyan " href="#"><i class="material-icons">local_offer</i><span class="menu-title" data-i18n="">voucher</span></a>
         <div class="collapsible-body">
            <ul>
               <li style="border-bottom: 1px solid #3d4451;"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/generatevoucher" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Generate Voucher</span></a>
               </li>
               <li style="border-bottom: 1px solid #3d4451;"><a class="collapsible-body" href="<?php echo base_url(); ?>admin/managevoucher" data-i18n=""><i class="material-icons">radio_button_unchecked</i><span>Manage voucher</span></a>
               </li>

            </ul>
         </div>
      </li>





      <li style="border-bottom: 1px solid #3d4451;" class="navigation-header"><a class="navigation-header-text">Users</a><i class="navigation-header-icon material-icons">more_horiz</i>
      </li>
      <li style="border-bottom: 1px solid #3d4451;" class="bold">
         <a class="collapsible-header waves-effect waves-cyan " href="#"><i class="material-icons">perm_identity</i><span class="menu-title" data-i18n="">Users</span></a>
         <div class="collapsible-body">
            <ul>
               <li style="border-bottom: 1px solid #3d4451;" class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/seller"><i class="material-icons">radio_button_unchecked</i><span class="menu-title" data-i18n="">Seller</span></a>
               </li>

               <li style="border-bottom: 1px solid #3d4451;" class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/client"><i class="material-icons">radio_button_unchecked</i><span class="menu-title" data-i18n="">Clients</span></a>
               </li>
      </li>
   </ul>
   </div>
   </li>

   <li style="border-bottom: 1px solid #3d4451;" class="navigation-header"><a class="navigation-header-text">Staff</a><i class="navigation-header-icon material-icons">more_horiz</i>
   </li>
   <li style="border-bottom: 1px solid #3d4451;" class="bold">
      <a class="collapsible-header waves-effect waves-cyan " href="#"><i class="material-icons">perm_identity</i><span class="menu-title" data-i18n="">Staff</span></a>
      <div class="collapsible-body">
         <ul>

            <li style="border-bottom: 1px solid #3d4451;" class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/administrator"><i class="material-icons">radio_button_unchecked</i><span class="menu-title" data-i18n="">Staff</span></a>
            </li>
            <li style="border-bottom: 1px solid #3d4451;" class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/role"><i class="material-icons">radio_button_unchecked</i><span class="menu-title" data-i18n="">Staff Permission</span></a>
            </li>
   </li>
   </ul>
   </div>
   </li>

   <li style="border-bottom: 1px solid #3d4451;" class="navigation-header"><a class="navigation-header-text">Blog</a><i class="navigation-header-icon material-icons">more_horiz</i>
   </li>
   <li style="border-bottom: 1px solid #3d4451;" class="bold">
      <a class="collapsible-header waves-effect waves-cyan " href="#"><i class="material-icons">perm_identity</i><span class="menu-title" data-i18n="">Blog</span></a>
      <div class="collapsible-body">
         <ul>

            <li style="border-bottom: 1px solid #3d4451;" class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/blogger"><i class="material-icons">radio_button_unchecked</i><span class="menu-title" data-i18n="">Blogger</span></a>
            </li>
            <li style="border-bottom: 1px solid #3d4451;" class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/blog"><i class="material-icons">radio_button_unchecked</i><span class="menu-title" data-i18n="">Blog</span></a>
            </li>
   </li>
   </ul>
   </div>
   </li>

   <li style="border-bottom: 1px solid #3d4451;" class="navigation-header"><a class="navigation-header-text">Orders</a><i class="navigation-header-icon material-icons">more_horiz</i>
   </li>
   <li style="border-bottom: 1px solid #3d4451;" class="bold">
      <a class="collapsible-header waves-effect waves-cyan " href="#"><i class="material-icons">perm_identity</i><span class="menu-title" data-i18n="">Sales</span></a>
      <div class="collapsible-body">
         <ul>
            <li style="border-bottom: 1px solid #3d4451;" class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/totalsales"><i class="material-icons">radio_button_unchecked</i><span class="menu-title" data-i18n="">Total Sales</span></a>
            </li>
            <li style="border-bottom: 1px solid #3d4451;" class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/order"><i class="material-icons">radio_button_unchecked</i><span class="menu-title" data-i18n="">Order</span></a>
            </li>

   </li>


   </ul>
   </div>
   </li>
   <li style="border-bottom: 1px solid #3d4451;" class="navigation-header"><a class="navigation-header-text">Setup</a><i class="navigation-header-icon material-icons">more_horiz</i>
   </li>
   <li style="border-bottom: 1px solid #3d4451;" class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/slider"><i class="material-icons">image</i><span class="menu-title" data-i18n="">Sliders</span></a>
   </li>
   
   <li style="border-bottom: 1px solid #3d4451;" class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/country"><i class="material-icons">location_city</i><span class="menu-title" data-i18n="">Country</span></a>
   </li>
   <li style="border-bottom: 1px solid #3d4451;" class="bold"><a class="waves-effect waves-cyan " href="<?php echo base_url(); ?>admin/setting"><i class="material-icons">settings</i><span class="menu-title" data-i18n="">Settings</span></a>
   </li>
   <li style="border-bottom: 1px solid #3d4451;" class="bold"><a class="waves-effect waves-cyan " href="app-calendar.html"><i class="material-icons">report</i><span class="menu-title" data-i18n="">Reports</span></a>
   </li>
   </ul>
   <div class="navigation-background"></div>
   <a class="sidenav-trigger btn-sidenav-toggle btn-floating btn-medium waves-effect waves-light hide-on-large-only" href="#" data-target="slide-out"><i class="material-icons">menu</i></a>
</aside>
<!-- END: SideNav-->