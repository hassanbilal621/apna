<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <div class="row">
                     <div class="col s12">
                        <?php echo form_open_multipart('admin/generatevoucher'); ?>
                        <div class="col s12">
                           <!-- Form with placeholder -->
                           <h4 class="card-title">Generate Vouchar</h4>
                           <div class="row">
                              <div class="input-field col s12">
                                 <input  type="text" name="voucher_code">
                                 <label>Vouchar Code </label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="input-field col s12">
                                 <input  type="text" name="voucher_price">
                                 <label>Vouchar Amount </label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="input-field col s12">
                                 <input  type="text" name="user_id">
                                 <label>User ID</label>
                              </div>
                           </div>
                           <div class="row">
                              <div class="input-field col s12">
                                 <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                    <i class="material-icons right">send</i>
                                 </button>
                              </div>
                           </div>
                        </div>
                     </div>
                     <?php echo form_close(); ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>