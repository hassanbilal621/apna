<div id="main">
   <div class="row">
      <div class="pt-1 pb-0" id="breadcrumbs-wrapper">
         <!-- Search for small screen-->
         <div class="container">
            <div class="row">
               <div class="col s12 m6 l6">
                  <h5 class="breadcrumbs-title">Manage Seller Data</h5>
               </div>
               <div class="col s12 m6 l6 right-align-md">
                  <ol class="breadcrumbs mb-0">

                     <li class="breadcrumb-item active">
                        <a class="waves-effect waves-light btn modal-trigger mb-2 mr-1" href="<?php echo base_url(); ?>register">Add Seller
                           <i class="material-icons right">shopping_basket</i>
                        </a>
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </div>
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <div class="row">
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Shop Name</th>
                              <th>Email Address</th>
                              <th>Status</th>
                              <th>No. Of Product</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach ($sellers as $seller) : ?>
                              <tr>
                                 <td><?php echo $seller['seller_id']; ?></td>
                                 <td><?php echo $seller['shopname']; ?></td>
                                 <td><?php echo $seller['email']; ?></td>
                                 <td><?php echo $seller['status']; ?></td>
                                 <td><?php echo $seller['no_of_product']; ?></td>
                                 </td>
                                 <td>
                                    <!-- <button id="<?php echo $seller['seller_id']; ?>" onclick="loaduserinfo(this.id)" class="btn waves-effect waves-light blue btn">Edit
                                       <i class="material-icons left">edit</i>
                                    </button> -->

                                    <a href="<?php echo base_url(); ?>admin/del_seller/<?php echo $seller['seller_id']; ?>" class="btn waves-effect waves-light red" type="submit" name="action">Delete
                                       <i class="material-icons left">delete_forever</i>
                                    </a>
                                 </td>
                              </tr>
                           <?php endforeach; ?>
                           </tfoot>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>


<div id="modal2" class="modal">
   <div class="modal-content">

   </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquerynew.min.js" type="text/javascript"></script>

<script type='text/javascript'>
   function loaduserinfo(sellerid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>admin/ajax_edit_sellermodal/" + sellerid,
         success: function(data) {
            $(".modal-content").html(data);
            $('#modal2').modal('open');
         }
      });
   }
</script>


