<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Add Categories</h4>
                  
                  
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Name</th>
                              <th>Banner</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr>
                           <?php foreach ($categories as $category): ?>

                              <td><?php echo $category['CategoryID']; ?></td>
                              <td><?php echo $category['CategoryName']; ?></td>
                              <td><img src="<?php echo base_url(); ?>assets/uploads/<?php echo $category['Categoryimg']; ?>" alt="avatar" style="width:100px; height:100px;"></td>
                              </td>
                              <td>
                                 <button id="<?php echo $category['CategoryID']; ?>"  onclick="loaduserinfo(this.id)" class="btn waves-effect waves-light blue btn">Edit
                                 <i class="material-icons left">edit</i>
                                 </button>
                                
                                 <a href="<?php echo base_url(); ?>admin/del_cat/<?php echo $category['CategoryID']; ?>" class="btn waves-effect waves-light red" type="submit" name="action">Delete
                                 <i class="material-icons left">delete_forever</i>
                                 </a>
                              </td>
                           </tr>
                       <?php endforeach; ?>
                           </tfoot>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- BEGIN VENDOR JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->


<div id="modal2" class="modal">
                                    <div class="modal-content">
                                      
                                    </div>
                                 </div>

                                 
   <script src="<?php echo base_url();?>assets/js/jquerynew.min.js" type="text/javascript"></script>

<script type='text/javascript'>


   function loaduserinfo(catid){
      // var userid = this.id;
         $.ajax({
            type: "GET",
            url: "<?php echo base_url();?>admin/ajax_edit_categorymodal/"+catid,
            success: function(data){
               $(".modal-content").html(data);
               $('#modal2').modal('open');
            }
         });
   }


</script>