<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Add Recurring</h4>
                  <div class="row">
                     <div class="col s12">
                        <button class="waves-effect waves-light btn modal-trigger mb-2 mr-1" href="#modal1">Add Recurring
                        <i class="material-icons left">shopping_basket</i>
                        </button>
                        <div id="modal1" class="modal">
                           <div class="modal-content">
                              <div class="row">
                                 <div class="col s12">
                                    <div class="card">
                                       <form class="col s12">
                                          <!-- Form with placeholder -->
                                          <h4 class="card-title">Create Recurring</h4>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <input id="name2" type="text">
                                                <label for="name2">Recurring Name </label>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <span for="img2">Recurring Image</span>
                                                <input  id="img2" type="file">
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <label for="desc2">Description</label>
                                                <textarea name="" id="desc2" cols="30" rows="10"></textarea>
                                             </div>
                                          </div>
                                          <div class="row">
                                             <div class="input-field col s12">
                                                <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Save
                                                <i class="material-icons right">send</i>
                                                </button>
                                             </div>
                                          </div>
                                    </div>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>Recurring ID</th>
                              <th>Customer</th>
                              <th>status</th>
                              <th>Total</th>
                              <th>Date Added</th>
                              <th>Date Modified</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           <tr>
                              <td>1</td>
                              <td>Tiger Nixon</td>
                              <td>Complete</td>
                              <td>$100</td>
                              <td>11/21/2019</td>
                              <td>11/21/2019</td>
                              <td>
                                 <button class="btn waves-effect waves-light blue modal-trigger mb-2 mr-1" type="submit"  href="#modal2"  name="action">Edit
                                 <i class="material-icons left">edit</i>
                                 </button>
                                 <div id="modal2" class="modal">
                                    <div class="modal-content">
                                       <div class="row">
                                          <div class="col s12">
                                             <div class="card">
                                                <form class="col s12">
                                                   <!-- Form with placeholder -->
                                                   <h4 class="card-title">Create Products</h4>
                                                   <div class="row">
                                                      <div class="input-field col s12">
                                                         <input id="name2" type="text">
                                                         <label for="name2">Recurring Name </label>
                                                      </div>
                                                   </div>
                                                   <div class="row">
                                                      <div class="input-field col s12">
                                                         <span for="img2">Recurring Image</span>
                                                         <input  id="img2" type="file">
                                                      </div>
                                                   </div>
                                                  
                                                   <div class="row">
                                                      <div class="input-field col s12">
                                                         <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Save
                                                         <i class="material-icons right">send</i>
                                                         </button>
                                                      </div>
                                                   </div>
                                             </div>
                                             </form>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <button class="btn waves-effect waves-light red" type="submit" name="action">Delete
                                 <i class="material-icons left">delete_forever</i>
                                 </button>
                              </td>
                           </tr>
                           <tr>
                           <td>1</td>
                              <td>Tiger Nixon</td>
                              <td>Complete</td>
                              <td>$100</td>
                              <td>11/21/2019</td>
                              <td>11/21/2019</td>
                             <td>  <button class="btn waves-effect waves-light blue modal-trigger mb-2 mr-1" href="#modal2" type="submit" name="action">Edit
                                 <i class="material-icons left">edit</i>
                                 </button>
                                 <button class="btn waves-effect waves-light red  " type="submit" name="action">Delete
                                 <i class="material-icons left">delete_forever</i>
                                 </button>
                              </td>
                           </tr>
                           </tfoot>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>
</div>
</div>
</div>
<!-- BEGIN VENDOR JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN THEME  JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<!-- END THEME  JS-->
<!-- BEGIN PAGE LEVEL JS-->
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
<!-- END PAGE LEVEL JS-->