<div class="row">

   <?php echo form_open('seller/manageproduct'); ?>
   <div class="col s12">

      <div id="card-stats">

      </div>
      <h4 class="card-title">Add Products</h4>
      <div class="row">
         <div class="input-field col s12">
            <h6>Product SKU </h6>
            <input id="SKU" type="text" name="productsku" value="<?php echo $product['ProductSKU']; ?>">
            <input  type="hidden" name="ProductID" value="<?php echo $product['ProductID']; ?>">
         </div>
      </div>
      <div class="row">
         <div class="input-field col s12">
            <h6>Name</h6>
            <input id="name2" type="text" name="productname" value="<?php echo $product['ProductName']; ?>">
         </div>
      </div>
      <div class="row">
         <div class="input-field col s12">
            <h6>Price</h6>
            <input id="Price" type="text" name="productprice" value="<?php echo $product['ProductPrice']; ?>">
         </div>
      </div>
      <div class="row">
         <div class="input-field col s12">
            <h6>Product Weight</h6>
            <input id="Weight" type="text" name="productweight" value="<?php echo $product['ProductWeight']; ?>">
         </div>
      </div>
      <div class="row">
         <div class="col s12">
            <h6>Select Category *</h6>
            <div class="selected-box auto-hight">
               <select class="browser-default" name="categoryid" required>
                  <?php foreach ($categories as $category) : ?>
                     <?php if ($category['CategoryID'] == $product['ProductCategoryID']) { ?>
                        <option selected value="<?php echo $category['CategoryID']; ?>"><?php echo $category['CategoryName']; ?></option>
                     <?php  } else {
                           ?>
                        <option value="<?php echo $category['CategoryID']; ?>"><?php echo $category['CategoryName']; ?></option>
                     <?php } ?>
                  <?php endforeach; ?>
               </select>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col s12">
            <h6>Select Brand *</h6>
            <div class="selected-box auto-hight">
               <select class="browser-default" name="productbrandid" required>

                  <?php foreach ($brands as $brand) : ?>
                     <?php if ($brand['brand_id'] == $product['product_brand_id']) { ?>
                        <option selected value="<?php echo $brand['brand_id']; ?>"><?php echo $brand['brand_name']; ?></option>
                     <?php  } else {
                           ?>
                        <option value="<?php echo $brand['brand_id']; ?>"><?php echo $brand['brand_name']; ?></option>
                     <?php } ?>
                  <?php endforeach; ?>
               </select>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="input-field col s12">
            <span class="helper-text" data-success="right">Product Cart Description</span>
            <textarea style="height: 7rem;" name="productcartdesc" id="desc1" cols="30" rows="10"><?php echo $product['ProductCartDesc']; ?></textarea>
         </div>
      </div>
      <div class="row">
         <div class="input-field col s12">
            <span class="helper-text" data-success="right">Product Short Description</span>
            <textarea style="height: 7rem;" name="productshortdesc" id="desc2" cols="30" rows="10"><?php echo $product['ProductShortDesc']; ?></textarea>
         </div>
      </div>
      <div class="row">
         <div class="input-field col s12">
            <span class="helper-text" data-success="right">Product Long Description</span>
            <textarea style="height: 7rem;" name="productlongdesc" id="desc3" cols="30" rows="10"><?php echo $product['ProductLongDesc']; ?></textarea>
         </div>
      </div>
      <div class="row">
         <div class="input-field col s12">
            <h6>Product Thumb</h6>
            <input id="Thumb" type="text" name="productthumb">
         </div>
      </div>
      <div class="row">
         <div class="input-field col s12">
            <h6>Product Stock</h6>
            <input id="Stock" type="text" name="productstock" value="<?php echo $product['ProductStock']; ?>">
         </div>
      </div>
      <div class="row">
         <div class="input-field col s12">
            <h6>Product Live</h6>
            <input id="Live" type="text" name="productlive" value="<?php echo $product['ProductLive']; ?>">
         </div>
      </div>
      <div class="row">
         <div class="input-field col s12">
            <h6>Product Unlimited</h6 value="<?php echo $product['ProductUnlimited']; ?>">
            <input id="Unlimited" type="text" name="productunlimited">
         </div>
      </div>
      <div class="row">
         <div class="input-field col s12">
            <h6>Product Location</h6 value="<?php echo $product['ProductLocation']; ?>">
            <input id="Location" type="text" name="productlocation">
         </div>
      </div>
      <div class="row">
         <div class="input-field col s12">
            <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
               <i class="material-icons right">send</i>
            </button>
         </div>
      </div>
   </div>
</div>