<div class="row">
	<div class="col s12">
		<div class="card br-1">
			<div class="card-content">
				<h4 class="card-title">View blogs</h4>
				<div class="row">
					<div class="card-panel border-radius-6 mt-10 card-animation-1">
						<a href="#"><img class="responsive-img border-radius-8 z-depth-4 image-n-margin" src="<?php echo base_url(); ?>assets/uploads/<?php echo $blog['blog_img']; ?>" alt="" style="max-height: 200px;"></a>
						<h6 class="deep-purple-text text-darken-3 mt-5"><?php echo $blog['blog_title']; ?></h6>
						<span><?php echo ($blog['blog_desc']) ?>.</span>
						<div class="row" style="border-top: 3px #f4dede solid;">
							<div class="col s6 mt-1">
								<img src="<?php echo base_url(); ?>assets/uploads/<?php echo $blog['userimage']; ?>" alt="<?php echo $blog['username']; ?>" class="circle mr-3 width-30 vertical-text-middle">
								<span class="pt-2"><?php echo $blog['username']; ?></span>
							</div>
							<div class="col s6 mt-3 right-align social-icon"> <span class="ml-3 vertical-align-top" style="font-size: 12px;"> Date:<?php echo $blog['blog_date']; ?></span>
							</div>
						</div>
					</div>
					<div class="app-wrapper">
						<?php echo form_open('users/add_comment'); ?>
						<textarea id="textarea2" placeholder="Write a comment..." name="comment" class="discrip"></textarea>
						<input type="hidden" name="blogid" value="<?php echo $blog['blog_id']; ?>">
						<button class="waves-effect waves-light  btn  submit box-shadow-none border-round mr-1 mb-1 right" type="submit" name="action">comment
							<i class="material-icons left">send</i>
						</button>
						<?php form_close(); ?>
					</div>
					<div class="row mt-10">
						<?php foreach ($comments as $comment) : ?>
							<div class="card-panel border-radius-6card-animation-1">
								<h6 class="padding-1" style="border-bottom: 1px solid;"><?php echo $comment['comment'] ?>.</h6>
								<div class="row">
									<div class="col s6 mt-1">
										<img src="<?php echo base_url(); ?>assets/uploads/<?php echo $comment['userimage']; ?>" alt="" width="5" class="circle mr-3 width-10 vertical-text-middle">
										<span class="pt-2"><?php echo $comment['username']; ?></span>
									</div>
									<div class="col s6 mt-3 right-align social-icon"> <span class="ml-3 vertical-align-top" style="font-size: 12px;"> Date: <?php echo $comment['date']; ?></span>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>