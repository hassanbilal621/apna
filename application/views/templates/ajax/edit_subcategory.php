<div class="row">
   <div class="col s12">
      <div class="card">
         <?php echo form_open('admin/update_subcat');?>
         <div class="col s12">
            <!-- Form with placeholder -->
            <h4 class="card-title">Update Sub Category</h4>
            <div class="row">
               <div class="input-field col s12">
                  <input  id="subcate2" type="text" name="subcate_name" value="<?php echo $subcategories['subcate_name'];?>">
                  <input type="hidden" value="<?php echo $subcategories['subcat_id']; ?>" name="subcat_id" >
               </div>
            </div>
            <div class="row">
            <div class="col s12">
                  <label for="Category">Select Category *</label>
                  <div class="selected-box auto-hight">
                     <select class="browser-default" name="categoryid" required>
                        <option disabled>Select Category</option>
                        <?php foreach ($categories as $category): ?>
                        <?php if (empty($category['CategoryName'])) { }
                           else{ if($category['CategoryID'] == $subcategories['category_id'] ){
                                 ?>
                        <option value="<?php echo $category['CategoryID']; ?>" selected><?php echo $category['CategoryName']; ?></option>
                        <?php
                           }    else{
                              ?>
                        <option value="<?php echo $category['CategoryID']; ?>"><?php echo $category['CategoryName']; ?></option>
                        <?php
                           }    
                           ?>
                        <option value="<?php echo $category['CategoryID']; ?>"><?php echo $category['CategoryName']; ?></option>
                        <?php }?>
                        <?php endforeach; ?>
                     </select>
                  </div>
               </div>
               </div>
            <div class="row">
               <div class="input-field col s12">
                  <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Save
                  <i class="material-icons right">send</i>
                  </button>
               </div>
            </div>
         </div>
      </div>
      <?php echo form_close();?>
   </div>
</div>