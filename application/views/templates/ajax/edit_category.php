<div class="row">
   <div class="col s12">
      <div class="card">
         <?php echo form_open_multipart('admin/update_cat');?>
         <div class="col s12">
            <!-- Form with placeholder -->
            <h4 class="card-title">Update Product Category</h4>
            <div class="row">
               <div class="input-field col s12">
                  <input id="name2" type="text" name="categoryname" value="<?php echo $categories['CategoryName']?>">
                  <input type="hidden" value="<?php echo $categories['CategoryID']; ?>" name="CategoryID" >
                  <label for="name2">Name </label>
               </div>
            </div>
            <div class="row">
               <div class="input-field col s12">
                  <span for="img2">Category Image</span>
                  <input  id="img2" type="file" name="userfile" >
                  <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $categories['Categoryimg']; ?>" value="<?php echo $categories['Categoryimg']?>" alt="avatar" style="width:100px; height:100px;">
               </div>
            </div>
            <div class="row">
               <div class="input-field col s12">
                  <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Save
                  <i class="material-icons right">send</i>
                  </button>
               </div>
            </div>
         </div>
      </div>
      <?php echo form_close();?>
   </div>
</div>