<div class="row">
	<div class="col s12">
		<div class="card br-1">
			<div class="card-content">
				<div class="row">
					<div class="card-panel border-radius-6 mt-10 card-animation-1">
						<a href="#"><img class="responsive-img border-radius-8 z-depth-4 image-n-margin" src="<?php echo base_url(); ?>assets/uploads/<?php echo $blog['blog_img']; ?>" alt="" style="max-height: 200px;"></a>
						<h6 class="deep-purple-text text-darken-3 mt-5"><?php echo $blog['blog_title']; ?></h6>
						<span><?php echo ($blog['blog_desc']) ?>.</span>
						<div class="row" style="border-top: 3px #f4dede solid;">
							<div class="col s6 mt-1">
								<img src="<?php echo base_url(); ?>assets/uploads/<?php echo $blog['userimage']; ?>" alt="<?php echo $blog['username']; ?>" class="circle mr-3 width-10 vertical-text-middle">
								<span class="pt-2"><?php echo $blog['username']; ?></span>
							</div>
							<div class="col s6 mt-3 right-align social-icon"> <span class="ml-3 vertical-align-top" style="font-size: 12px;"> Date:<?php echo $blog['blog_date']; ?></span>
							</div>
						</div>
					</div>
					<a class="waves-effect waves-light  btn  edit box-shadow-none border-round mr-1 mb-1" id="<?php echo $blog['blog_id']; ?>" onclick="loadblogsedit(this.id)" type="submit" name="action">Edit
						<i class="material-icons left">edit</i>
					</a>
					<a class="waves-effect waves-light  btn  delete box-shadow-none border-round mr-1 mb-1" href="<?php echo base_url(); ?>admin/deleteblogs/<?php echo $blog['blog_id']; ?>" type="submit" name="action">DELETE
						<i class="material-icons left">delete_forever</i>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>