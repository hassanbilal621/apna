<div class="row">
                                          <div class="col s12">
                                             <div class="card">
                                             <?php echo form_open_multipart('admin/update_brand');?>
                                                <div class="col s12">
                                                   <!-- Form with placeholder -->
                                                   <h4 class="card-title">Update Brands</h4>
                                                   <div class="row">
                                                      <div class="input-field col s12">
                                                         <input  id="name2" type="text" name="brand_name" value="<?php echo $brands['brand_name'];?>">
                                                         <input type="hidden" value="<?php echo $brands['brand_id']; ?>" name="brand_id" >
                                                         <label for="name2">Name</label>
                                                      </div>
                                                   </div>
                                                   <div class="row">
                                                      <div class="input-field col s12">
                                                         <span for="img2">Brand Logo</span>
                                                         <input  id="img2" type="file" name="userfile" >
                                                         <img src="<?php echo base_url();?>assets/uploads/<?php echo $brands['brand_logo'];?>" value="<?php echo $brands['brand_logo'];?>" alt="avatar" style="width:100px; height:100px;">
                                                      </div>
                                                   </div>
                                                   <div class="row">
                                                      <div class="input-field col s12">
                                                         <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Save
                                                         <i class="material-icons right">send</i>
                                                         </button>
                                                      </div>
                                                   </div>
                                             </div>
                                             </div>
                                             <?php echo form_close();?>
                                          </div>
                                       </div>