  <?php echo form_open('users/addproduct') ?>
  <div class="row" id="product-one">
    <div class="col s12">
      <a class="modal-close right"><i class="material-icons">close</i></a>
    </div>
    <div class="col m6">
      <img src="<?php echo base_url(); ?>assets/uploads/<?php echo $product['ProductThumb']; ?>" class="responsive-img" alt="">
    </div>
    <div class="col m6">
      <h6><?php echo $product['CategoryName']; ?></h6>
      <h5><?php echo $product['ProductName']; ?></h5>
      <input type="hidden" value="<?php echo $product['ProductThumb']; ?>" name="picture" />
      <input type="hidden" value="<?php echo $product['ProductID']; ?>" name="productid" />
      <input type="hidden" value="<?php echo $product['ProductName']; ?>" name="productname" />
      <input type="hidden" value="<?php echo $product['ProductPrice']; ?>" name="productprice" />
      <input type="hidden" value="<?php echo $product['ProductStock']; ?>" name="ProductStock" />
      <input type="hidden" value="<?php echo $product['seller_id']; ?>" name="sellerid" />

      <h6 class="mt-3"><?php echo $product['ProductCartDesc']; ?></h6>
      <h6><?php echo $product['ProductShortDesc']; ?></h6>
      <h6><?php echo $product['ProductLongDesc']; ?></h6>
      <?php if ($product['ProductStock'] == 0) { ?>
        <h6>Availability: <span class="red-text"><?php echo $product['ProductStock']; ?></span></h6>
        <h5>RS <?php echo $product['ProductPrice']; ?></h5>
    </div>
  </div>
  <button class="waves-effect waves-light btn submit mb-2 disabled right" type="submit">ADD TO CART</button>
  <?php } else {
    ?>
    <h6>Availability: <span class="green-text"><?php echo $product['ProductStock']; ?></span></h6>
    <h6>Qty :
      <input style="width:auto!important;" class="" type="number" value="1" min="1" max="<?php echo $product['ProductStock']; ?>" name="productqty" required /></h6>
    <h5>RS <?php echo $product['ProductPrice']; ?></h5>

    </div>

    </div>
    <button class="waves-effect waves-light btn submit mb-2 right" type="submit">ADD TO CART</button>

  <?php }
  ?>
  <?php echo form_close() ?>