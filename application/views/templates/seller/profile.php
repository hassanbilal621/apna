<style>
	.imghover {
		position: relative;
		width: 50%;
	}

	.image {
		opacity: 1;
		display: block;
		width: 100%;
		height: auto;
		transition: .5s ease;
		backface-visibility: hidden;
	}

	.middle {
		transition: .5s ease;
		opacity: 0;
		position: absolute;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
		-ms-transform: translate(-50%, -50%);
		text-align: center;
	}

	.imghover:hover .image {
		opacity: 0.3;
	}

	.imghover:hover .middle {
		opacity: 1;
	}
</style>
<div class="container" style="width:1000px;">
	<div id="main">
		<div class="row">
			<div class="col s12">
				<div class="card">

					<div id="editprofile" class=" tab card-content tab-pane fade" style="display:none;">
						<div class="row">
							<div class="col s12">
								<div id="icon-prefixes" class="card-tabs">
									<div class="card-content">
										<div class="card-title">
											<div class="row">
												<i id="close" class="material-icons right">close</i>
												<div class="input-field col s12">
													<h5 class="ml-4">Edit Your Profile</h5>

												</div>
											</div>
											<?php echo form_open('seller/updateprofile'); ?>
											<div class="row">
												<div class="input-field col s6">
													<h6>First name</h6>
													<input id="username" type="text" name="firstname" value="<?php echo $user['firstname'] ?>" required>
												</div>
												<div class="input-field col s6">
													<h6>Last name</h6>
													<input id="username" type="text" name="lastname" value="<?php echo $user['lastname'] ?>" required>
												</div>
											</div>
											<div class="row">
												<div class="input-field col s6">
													<h6>Username</h6>
													<input id="username" type="text" name="username" value="<?php echo $user['username'] ?>" required>
												</div>
												<div class="input-field col s6">
													<h6>Phone</h6>
													<input id="number" type="number" name="number" value="<?php echo $user['number'] ?>" required>
												</div>
											</div>
											<div class="row">
												<div class="input-field col s6">
													<h6>Email</h6>
													<input id="email" type="email" name="email" value="<?php echo $user['email'] ?>" required>
												</div>
												<div class="input-field col s6">
													<h6>Birthday</h6>
													<input class="datepicker" type="text" name="birthday" value="<?php echo $user['birthday'] ?>" required>
												</div>
											</div>
											<div class="row">
												<div class="input-field col s12">
													<h6>Billing Address</h6>
													<input id="address" type="text" name="address" value="<?php echo $user['address'] ?>" required>
												</div>
											</div>
											<div class="row">
												<div class="input-field col s12">
													<button class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12" type="submit" name="Save">Save</button>
												</div>
											</div>
											<?php echo form_close(); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="profile" class="col s12">
					<div class="col s12 m12 l8">
						<div id="profile-card" class="card animate fadeRight">
							<div class="card-image  imghover image waves-effect waves-block waves-light" style="height: auto!important;">
								<img class="activator" src="<?php echo base_url(); ?>assets/uploads/<?php echo $user['image'] ?>" alt="" />
								<div class="middle">
									<div class="black-text">
										<img src="<?php echo base_url(); ?>assets\app-assets\images\download.png" alt="" />
										<h6>click Here To Upload Your New Image</h6>
									</div>
								</div>
							</div>
							<div class="card-content">
								<a class="btn-floating activator btn-move-up waves-effect waves-light red accent-2 z-depth-4 right">
									<i id="edit" class="material-icons">edit</i>
								</a>
								<h6><i class="material-icons profile-card-i">perm_identity</i><?php echo $user['firstname'] ?> <?php echo $user['lastname'] ?></h6>
								<h6><i class="material-icons profile-card-i">perm_phone_msg</i><?php echo $user['number'] ?></h6>
								<h6><i class="material-icons profile-card-i">email</i><?php echo $user['email'] ?></h6>
								<h6><i class="material-icons profile-card-i">cake</i><?php echo $user['birthday'] ?></h6>
								<h6><i class="material-icons profile-card-i">pin</i><?php echo $user['address'] ?></h6>
							</div>
							<div class="card-reveal">
								<span class="card-title grey-text text-darken-4"> <i class="material-icons right">close</i>
								</span>
								<?php echo form_open_multipart('seller/image'); ?>
								<div class="row">
									<div class="input-field col s12">
										<h6>Update your Profile Image</h6>
										<input type="file" id="input-file-now" class="dropify" data-default-file="" name="userfile" accept="image/*" />
									</div>
								</div>
								<div class="row">
									<div class="input-field col s12">
										<button class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12" type="submit" name="Save">Save</button>
									</div>
								</div>
								<?php echo form_close(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
	$(document).ready(function() {
		$("#edit").click(function() {
			$("#editprofile").show();
			$("#profile").hide();
		});
	});
</script>
<script>
	$(document).ready(function() {
		$("#close").click(function() {
			$("#profile").show();
			$("#editprofile").hide();
		});
	});
</script>

<script>
	$(document).ready(function() {
		$('.datepicker').datepicker();
	});
</script>