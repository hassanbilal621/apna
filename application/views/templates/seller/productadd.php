<div class="container" style="width:1000px;">
   <div id="main">
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <div class="row">

                     <?php echo form_open('seller/productadd'); ?>
                     <div class="col s12">

                        <div id="card-stats">

                        </div>
                        <h4 class="card-title">Add Products</h4>
                        <div class="row">
                           <div class="input-field col s12">
                              <label for="name2">Product SKU </label>
                              <input id="SKU" type="text" name="productsku">
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <label for="name2">Name</label>
                              <input id="name2" type="text" name="productname">
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <label for="name2">Price</label>
                              <input id="Price" type="text" name="productprice" ?>
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <label for="name2">Product Weight</label>
                              <input id="Weight" type="text" name="productweight">
                           </div>
                        </div>
                        <div class="row">
                           <div class="col s12">
                              <label for="Category">Select Category *</label>
                              <div class="selected-box auto-hight">
                                 <select class="form-control" name="categoryid" required>
                                    <option disabled selected>Select Category</option>
                                    <?php foreach ($categories as $category) : ?>
                                       <?php if (empty($category['CategoryName'])) { } else {
                                             ?>
                                          <option value="<?php echo $category['CategoryID']; ?>"><?php echo $category['CategoryName']; ?></option>
                                       <?php } ?>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col s12">
                              <label for="Brand">Select Brand *</label>
                              <div class="selected-box auto-hight">
                                 <select class="form-control" name="productbrandid" required>
                                    <option disabled selected>Select Brand</option>
                                    <?php foreach ($brands as $brand) : ?>
                                       <?php if (empty($brand['brand_name'])) { } else {
                                             ?>
                                          <option value="<?php echo $brand['brand_id']; ?>"><?php echo $brand['brand_name']; ?></option>
                                       <?php } ?>
                                    <?php endforeach; ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <span class="helper-text" data-success="right">Product Cart Description</span>
                              <textarea style="height: 7rem;" name="productcartdesc" id="desc1" cols="30" rows="10"></textarea>
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <span class="helper-text" data-success="right">Product Short Description</span>
                              <textarea style="height: 7rem;" name="productshortdesc" id="desc2" cols="30" rows="10"></textarea>
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <span class="helper-text" data-success="right">Product Long Description</span>
                              <textarea style="height: 7rem;" name="productlongdesc" id="desc3" cols="30" rows="10"></textarea>
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <label for="name2">Product Thumb</label>
                              <input id="Thumb" type="text" name="productthumb">
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <label for="name2">Product Stock</label>
                              <input id="Stock" type="text" name="productstock">
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <label for="name2">Product Live</label>
                              <input id="Live" type="text" name="productlive">
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <label for="name2">Product Unlimited</label>
                              <input id="Unlimited" type="text" name="productunlimited">
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <label for="name2">Product Location</label>
                              <input id="Location" type="text" name="productlocation">
                           </div>
                        </div>
                        <div class="row">
                           <div class="input-field col s12">
                              <button class="btn cyan waves-effect waves-light right" type="submit" name="action">Submit
                                 <i class="material-icons right">send</i>
                              </button>
                           </div>
                        </div>
                     </div>
                  </div>
                  <?php echo form_close(); ?>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>

<script>
   function previewimg(input) {
      if (input.files && input.files[0]) {
         var reader = new FileReader();

         reader.onload = function(e) {
            $('#view')
               .attr('src', e.target.result);
         };

         reader.readAsDataURL(input.files[0]);
      }
   }
</script>