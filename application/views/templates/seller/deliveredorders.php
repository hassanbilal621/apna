<div class="container" style="width:1000px;">
   <div class="container" style="width:1000px;">
      <div id="main">
         <div class="row">

            <!-- Page Length Options -->
            <div class="col s12">
               <div class="card">
                  <div class="card-content">
                     <h4 class="card-title">Delivered Orders</h4>
                     <div class="row">
                        <div class="col s12">
                           <table id="page-length-option" class="display">
                              <thead>
                                 <tr>
                                    <th>Order ID</th>
                                    <th>Product</th>
                                    <th>Customer</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                    <!-- <th>Order Date</th> -->
                                    <th>Status</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php foreach ($orders as $order) : ?>
                                    <tr>
                                       <td><?php echo $order['orderid']; ?></td>
                                       <td><?php echo $order['ProductName']; ?></td>
                                       <td><?php echo $order['UserFirstName']; ?></td>
                                       <td><?php echo $order['qty']; ?></td>
                                       <td><?php echo $order['sub_total']; ?></td>
                                       <td><?php echo $order['status']; ?></td>
                                    </tr>
                                 <?php endforeach; ?>
                                 </tfoot>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- BEGIN VENDOR JS-->
   <script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
   <!-- BEGIN VENDOR JS-->
   <!-- BEGIN PAGE VENDOR JS-->
   <!-- END PAGE VENDOR JS-->
   <!-- BEGIN THEME  JS-->
   <script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
   <script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
   <!-- END THEME  JS-->
   <!-- BEGIN PAGE LEVEL JS-->
   <script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/advance-ui-modals.js" type="text/javascript"></script>
   <!-- END PAGE LEVEL JS-->
</div>