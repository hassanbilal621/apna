   <div id="main">
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <h4 class="card-title">Order</h4>
                  <div class="row">
                     <div class="col s12">
                     <table id="page-length-option" class="display">
                              <thead>
                                 <tr>
                                    <th>Order ID</th>
                                    <th>Product</th>
                                    <th>Customer</th>
                                    <th>Quantity</th>
                                    <th>Total</th>
                                    <!-- <th>Order Date</th> -->
                                    <th>Status</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php foreach ($orders as $order) : ?>
                                    <tr>
                                       <td><?php echo $order['orderid']; ?></td>
                                       <td><?php echo $order['ProductName']; ?></td>
                                       <td><?php echo $order['UserFirstName']; ?></td>
                                       <td><?php echo $order['qty']; ?></td>
                                       <td><?php echo $order['sub_total']; ?></td>
                                       <td><?php echo $order['status']; ?></td>
                                    </tr>
                                 <?php endforeach; ?>
                                 </tfoot>
                           </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>