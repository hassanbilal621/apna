<div id="main">
   <div class="row">
      <!-- Page Length Options -->
      <div class="row">
         <div class="col s12">
            <div class="card">
               <div class="card-content">
                  <div class="row card-titles">
                     <div class="col s6">
                        <h4 class="card-title">Manage Product
                     </div>
                     <div class="col s6 right">
                        <a class="btn waves-effect waves-light submit right" href="<?php echo base_url(); ?>seller/productadd" type="submit" href="#modal3" name="action">Add Product
                           <i class="material-icons left">add</i>
                        </a>
                        </h4>
                     </div>
                  </div>
                  <div class="row">
                     <table id="page-length-option" class="display">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Product Name</th>
                              <th>Product Price</th>
                              <th>Product Image</th>
                              <th>Short Discount</th>
                              <th>Product Stock</th>
                              <th>Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach ($products as $product) : ?>
                              <tr>
                                 <td><?php echo $product['ProductID']; ?></td>
                                 <td><?php echo $product['ProductName']; ?></td>
                                 <td><?php echo $product['ProductPrice']; ?></td>
                                 <td><img src="<?php echo base_url(); ?>assets\uploads\<?php echo $product['ProductThumb']; ?>" alt="" style="max-width: 30%;"></td>
                                 <td><?php echo $product['ProductShortDesc']; ?></td>
                                 <td><?php echo $product['ProductStock']; ?></td>
                                 <td>
                                    <button class="btn waves-effect waves-light blue modal-trigger" onclick="loadpurordinfo(this.id)" id="<?php echo $product['ProductID']; ?>" type="submit" href="#modal3" name="action">EDIT
                                       <i class="material-icons left">edit</i>
                                    </button>
                                    <a class="btn waves-effect waves-light red" id="<?php echo $product['ProductID']; ?>" onclick="del(this.id)" type="submit" name="action">DELETE
                                       <i class="material-icons left">delete_forever</i>
                                 </td>
                              </tr>
                           <?php endforeach; ?>
                           </tfoot>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<div id="modal3" class="modal">
   <div class="modal-content">
   </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquerynew.min.js" type="text/javascript"></script>
<script>
   function loadpurordinfo(productid) {
      // var userid = this.id;
      $.ajax({
         type: "GET",
         url: "<?php echo base_url(); ?>seller/ajax_edit_product/" + productid,
         success: function(data) {
            $(".modal-content").html(data);
            $('#modal3').modal('open');
         }
      });
   }
</script>
<script>
   function del(id) {

      swal({
         title: "Are you sure?",
         text: "You will not be able to recover this imaginary file!",
         icon: 'warning',
         dangerMode: true,
         buttons: {
            cancel: 'No, Please!',
            delete: 'Yes, Delete It'
         }
      }).then(function(willDelete) {
         if (willDelete) {
            var html = $.ajax({
               type: "GET",
               url: "<?php echo base_url(); ?>seller/deleteproduct/" + id,
               // data: info,
               async: false
            }).responseText;

            if (html == "success") {
               $("#delete").html("delete success.");
               return true;

            }
            swal("Poof! Your record has been deleted!", {
               icon: "success",
            });
            setTimeout(location.reload.bind(location), 1000);
         } else {
            swal("Your imaginary file is safe", {
               title: 'Cancelled',
               icon: "error",
            });
         }
      });


   }
</script>