<div class="container" style="width:1000px;">
   <!--card stats start-->
   <div id="card-stats">
      <div class="row">
         <a href="<?php echo base_url(); ?>seller/orders">
            <div class="col s12 m6 l3 xl3">
               <div class="card  gradient-shadow min-height-100  animate fadeLeft" style="color: black!important;">
                  <div class="padding-4">
                     <div class="col s8 m8">
                        <i class="material-icons mt-5">add_shopping_cart</i>
                        <p>Orders</p>
                     </div>
                     <div class="col s4 m4 right-align">
                        <h5 class="mb-0 ml-1">690</h5>
                        <p class="no-margin">Orders</p>
                     </div>
                  </div>
               </div>
            </div>
         </a>
         <a href="<?php echo base_url(); ?>seller/manageproduct">
            <div class="col s12 m6 l3 xl3">
               <div class="card gradient-shadow min-height-100  animate fadeLeft" style="color: black!important;">
                  <div class="padding-4">
                     <div class="col s8 m8">
                        <i class="material-icons mt-5">layers</i>
                        <p>Products</p>
                     </div>
                     <div class="col s4 m4 right-align">
                        <h5 class="mb-0 ml-1">1885</h5>
                        <p class="no-margin">Products</p>
                     </div>
                  </div>
               </div>
            </div>
         </a>
         <a href="<?php echo base_url(); ?>seller/pendingorder">
            <div class="col s12 m6 l3 xl3">
               <div class="card gradient-shadow min-height-100  animate fadeRight" style="color: black!important;">
                  <div class="padding-4">
                     <div class="col s8 m8">
                        <i class="material-icons mt-5">access_time</i>
                        <p>Pending Order</p>
                     </div>
                     <div class="col s4 m4 right-align">
                        <h5 class="mb-0 ml-1">01</h5>
                        <p class="no-margin">New</p>
                        <p></p>
                     </div>
                  </div>
               </div>
            </div>
         </a>
          <a href="<?php echo base_url(); ?>seller/deliveryorder">
            <div class="col s12 m6 l3 xl3">
               <div class="card gradient-shadow min-height-100  animate fadeRight" style="color: black!important;">
                  <div class="padding-4">
                     <div class="col s8 m8">
                        <i class="material-icons mt-5">done</i>
                        <p>Deliver Order</p>
                     </div>
                     <div class="col s4 m4 right-align">
                        <h5 class="mb-0 ml-1">80</h5>
                        <p class="no-margin">delivered</p>
                     </div>
                  </div>
               </div>
            </div>
         </a>
      </div>
   </div>