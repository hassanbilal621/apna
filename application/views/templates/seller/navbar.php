<header class="page-topbar" id="header">
   <div class="navbar navbar-fixed">
      <nav class="navbar-main background-color white">
         <div class="container" style="width:1000px;">
            <div class="nav-wrapper">
               <ul class="left">
                  <li>
                     <h1 class="logo-wrapper"><a class="brand-logo darken-1" href="<?php echo base_url(); ?>seller"><img src="<?php echo base_url(); ?>assets/app-assets/images/logo/kalakaar logo.png" alt=" logo" style="margin-bottom:-10px; height: 40px; margin-top:-10px" ;><span class="logo-text hide-on-med-and-down" style="color:black" ;>Apna Kalakaar</span></a></h1>
                  </li>
               </ul>

            </div>
            <ul class="navbar-list right">
               <?php if (!$this->session->userdata('apna_seller_id')) { ?>
                  <li><a class="waves-effect waves-dark" style="color: black;" href="<?php echo base_url(); ?>login">Login</a></li>
               <?php } else { ?>
                  <li><a class=" waves-effect waves-dark" style="color: black;" href="<?php echo base_url(); ?>seller/profile"><?php echo $this->session->userdata('firstname') ?></a></li>
                  <li><a class="waves-effect waves-dark" style="color: black;" href="<?php echo base_url(); ?>seller/logout">Logout</a></li>
                  </li> <?php } ?>
            </ul>
         </div>
      </nav>

   </div>
</header>