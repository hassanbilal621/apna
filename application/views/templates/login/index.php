<div class="row login-bg">
    <div class="col s12">
        <div class="container">
            <div id="login-page" class="row">
                <div class="col s12 m4 14 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">
                    <div class="login-form">
                        <div class="row">
                            <div class="input-field col s12 center">
                                <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/app-assets/images/logo/kalakaar logo.png" alt="" style="height: 130px;" class="responsive-img valign"></a>

                                <div class="row user-projects tab">
                                    <div class="col s12">
                                        <ul class="tabs card-border-gray mt-4">
                                            <li class="tab col s6"><a class="active" href="#user">
                                                    <img src="<?php echo base_url(); ?>assets/app-assets/images/icon/user.png" alt="avatar" style="height: 28px;margin: 0px 18px -7px 0;"></i> User Login
                                                </a>
                                            </li>
                                            <li class="tab col  s6"><a href="#seller">
                                                    <img src="<?php echo base_url(); ?>assets/app-assets/images/icon/seller.png" alt="avatar" style="height: 28px;margin: 0px 18px -7px 0;"></i>seller Login
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div id="user">
                                    <?php echo form_open('users/login'); ?>
                                    <?php if ($this->session->flashdata('user_registered')) : ?>
                                        <div id="card-alert" class="card green">
                                            <div class="card-content white-text">
                                                <p> <?php echo $this->session->flashdata('user_registered'); ?></p>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if ($this->session->flashdata('login_failed')) : ?>
                                        <div id="card-alert" class="card gradient-45deg-amber-amber">
                                            <div class="card-content white-text">
                                                <p> <?php echo $this->session->flashdata('login_failed'); ?></p>
                                            </div>
                                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                    <?php endif; ?>
                                    <?php if ($this->session->flashdata('user_loggedout')) : ?>
                                        <div id="card-alert" class="card green">
                                            <div class="card-content white-text">
                                                <p><?php echo $this->session->flashdata('user_loggedout'); ?></p>
                                            </div>
                                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                    <?php endif; ?>
                                    <p class="center login-form-text">Login With Your Users Account</p>
                                    <div class="row margin">
                                        <div class="input-field col s12">
                                            <i class="material-icons prefix pt-2">person_outline</i>
                                            <input id="username" name="username" type="text" placeholder="Username" style="width: 85%!important;">

                                        </div>
                                    </div>
                                    <div class="row margin">
                                        <div class="input-field col s12">
                                            <i class="material-icons prefix pt-2">lock_outline</i>
                                            <input id="password" name="password" type="password" placeholder="Password" style="width: 85%!important;">

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button type="submit" name="login" class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1 col s12">Login</button>
                                        </div>
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>
                                <div id="seller">
                                    <?php echo form_open('seller/login'); ?>
                                    <?php if ($this->session->flashdata('seller_registered')) : ?>
                                        <div id="card-alert" class="card green">
                                            <div class="card-content white-text">
                                                <p> <?php echo $this->session->flashdata('seller_registered'); ?></p>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if ($this->session->flashdata('login_failed')) : ?>
                                        <div id="card-alert" class="card gradient-45deg-amber-amber">
                                            <div class="card-content white-text">
                                                <p> <?php echo $this->session->flashdata('login_failed'); ?></p>
                                            </div>
                                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                    <?php endif; ?>
                                    <?php if ($this->session->flashdata('user_loggedout')) : ?>
                                        <div id="card-alert" class="card green">
                                            <div class="card-content white-text">
                                                <p><?php echo $this->session->flashdata('user_loggedout'); ?></p>
                                            </div>
                                            <button type="button" class="close white-text" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                    <?php endif; ?>
                                    <p class="center login-form-text">Login With Your Seller Account</p>

                                    <div class="row margin">
                                        <div class="input-field col s12">
                                            <i class="material-icons prefix pt-2">person_outline</i>
                                            <input id="username" name="username" type="text" placeholder="Username" style="width: 85%!important;">

                                        </div>
                                    </div>
                                    <div class="row margin">
                                        <div class="input-field col s12">
                                            <i class="material-icons prefix pt-2">lock_outline</i>
                                            <input id="password" name="password" type="password" placeholder="Password" style="width: 85%!important;">

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <button type="submit" name="login" class="waves-effect waves-light  btn submit box-shadow-none border-round mr-1 mb-1 col s12">Login</button>
                                        </div>
                                    </div>
                                    <?php echo form_close(); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col s6 m6 l6">
                                <p class="margin medium-small"><a href="<?php echo base_url(); ?>register">Register Now!</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/noUiSlider/nouislider.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/form-file-uploads.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/dropify/js/dropify.min.js"></script>
</body>

</html>