<div class="row login-bg">
  <div class="col s12">
    <div class="container">
      <div id="login-page" class="row">
        <div class="col s12 m4 l5 z-depth-4 card-panel border-radius-6 login-card bg-opacity-8">
          <div class="login-form">
            <div class="row">
              <div class="input-field col s12 center">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/app-assets/images/logo/kalakaar logo.png" alt="" style="height: 130px;" class="responsive-img valign"></a>

                <div class="row user-projects tab">
                  <div class="col s12">
                    <ul class="tabs card-border-gray mt-4">
                      <li class="tab col s6"><a class="active" href="#user">
                          <img src="<?php echo base_url(); ?>assets/app-assets/images/icon/user.png" alt="avatar" style="height: 28px;margin: 0px 18px -7px 0;"></i> User Register
                        </a>
                      </li>
                      <li class="tab col  s6"><a href="#seller">
                          <img src="<?php echo base_url(); ?>assets/app-assets/images/icon/seller.png" alt="avatar" style="height: 28px;margin: 0px 18px -7px 0;"></i>seller Register
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <div id="user">

                  <?php echo form_open('users/registers'); ?>
                  <div class="row">
                    <div class="col s12">
                      <div class="row margin">
                        <div class="input-field col s12  m12 l6">
                          <i class="material-icons prefix pt-2">person_outline</i>
                          <input type="text" name="UserFirstName" placeholder="Type Your First Name" required style="width: 85%!important;">
                        </div>
                        <div class="input-field col s12  m12 l6">
                          <i class="material-icons prefix pt-2">remove</i>
                          <input type="text" name="UserLastName" placeholder="Type Your Last Name" required style="width: 85%!important;">
                        </div>
                      </div>
                      <div class="row margin">
                        <div class="input-field col s12  m12 l6">
                          <i class="material-icons prefix pt-2">phone_outline</i>
                          <input type="number" name="UserPhone" placeholder="Type Your Number" required style="width: 85%!important;">
                        </div>
                        <div class="input-field col s12  m12 l6">
                          <i class="material-icons prefix pt-2">lock_outline</i>
                          <input type="password" name="password" placeholder="Type Your Password" required style="width: 85%!important;">
                        </div>
                      </div>
                      <div class="row margin">
                        <div class="input-field col s12  m12 l12">
                          <i class="material-icons prefix pt-2">mail_outline</i>
                          <input type="email" name="email" placeholder="Type Your Email" required style="width: 85%!important;">
                        </div>
                      </div>
                      <div class="row margin">
                        <div class="input-field col s12">
                          <i class="material-icons prefix pt-2">pin_drop</i>
                          <input type="text" name="UserAddress" placeholder="Type Your address" required style="width: 85%!important;">
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field col s12">
                          <button class="btn waves-effect waves-light border-round submit col s12">Register</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php echo form_close(); ?>

                </div>
                <div id="seller">

                  <?php echo form_open('Seller/register'); ?>
                  <div class="row">
                    <div class="col s12">
                      <div class="row margin">
                        <div class="input-field col s12  m12 l6">
                          <i class="material-icons prefix pt-2">person_outline</i>
                          <input type="text" name="UserFirstName" placeholder="Type Your First Name" required style="width: 85%!important;">
                        </div>
                        <div class="input-field col s12  m12 l6">
                          <i class="material-icons prefix pt-2">remove</i>
                          <input type="text" name="UserLastName" placeholder="Type Your Last Name" required style="width: 85%!important;">
                        </div>
                      </div>
                      <div class="row margin">
                        <div class="input-field col s12  m12 l6">
                          <i class="material-icons prefix pt-2">phone_outline</i>
                          <input type="number" name="UserPhone" placeholder="Type Your Number" required style="width: 85%!important;">
                        </div>
                        <div class="input-field col s12  m12 l6">
                          <i class="material-icons prefix pt-2">lock_outline</i>
                          <input type="password" name="password" placeholder="Type Your Password" required style="width: 85%!important;">
                        </div>
                      </div>
                      <div class="row margin">
                        <div class="input-field col s12  m12 l6">
                          <i class="material-icons prefix pt-2">shop</i>
                          <input type="text" name="shopname" placeholder="Type Your Shop Name" required style="width: 85%!important;">
                        </div>
                        <div class="input-field col s12  m12 l6">
                          <i class="material-icons prefix pt-2">mail_outline</i>
                          <input type="email" name="email" placeholder="Type Your Email" required style="width: 85%!important;">
                        </div>
                      </div>
                      <div class="row margin">
                        <div class="input-field col s12  m12 l12">
                          <i class="material-icons prefix pt-2">pin_drop</i>
                          <input type="text" name="UserAddress" placeholder="Type Your address" required style="width: 85%!important;">
                        </div>
                      </div>
                      <div class="row">
                        <div class="input-field col s12">
                          <button class="btn waves-effect waves-light border-round submit col s12">Register</button>
                        </div>
                      </div>
                    </div>
                  </div>

                  <?php echo form_close(); ?>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <p class="margin medium-small"><a href="<?php echo base_url(); ?>login">Already have an account? Login</a></p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>

<script src="<?php echo base_url(); ?>assets/app-assets/js/vendors.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/noUiSlider/nouislider.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/extensions/responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/data-tables/js/dataTables.select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/plugins.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/custom/custom-script.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/js/scripts/form-file-uploads.js"></script>
<script src="<?php echo base_url(); ?>assets/app-assets/vendors/dropify/js/dropify.min.js"></script>
</body>

</html>